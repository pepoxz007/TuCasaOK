<div id="sidebar-primary" class="sidebar">
    <?php if ( is_active_sidebar( 'primary' ) ) : ?>
        <?php dynamic_sidebar( 'primary' ); ?>
    <?php else : ?>
        <!-- Time to add some widgets! -->
        <div id="primary" class="sidebar">
		    <?php do_action( 'before_sidebar' ); ?>
		    <?php if ( ! dynamic_sidebar( 'sidebar-primary' ) ) : ?>
		       
		        
		   <?php endif; ?>
		</div>
    <?php endif; ?>
</div>