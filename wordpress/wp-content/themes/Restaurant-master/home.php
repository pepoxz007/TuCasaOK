<?php
get_header();
get_template_part( 'template-parts/navigation/navigation-top', null );
?>


  

<div class="texture-header" style="">
  <img src="<?php echo get_template_directory_uri();?>/assets/images/benefits/img-header.png">
</div>

<div id="content" class="blogs">
  <section class="section-blogs" style="">
    <?php get_sidebar('primary'); ?> 

    <div id="works-list" class="filter-list no-spaces grid" style="border: 1px solid blue;" >
  
      <?php 
      //PORTAFOLIO
      $args = array(
       'numberposts' => 9,
       'offset' => 0,
       'orderby' => 'post_date',
       'order' => 'DESC',
       'include' => '',
       'exclude' => '',
       'meta_key' => '',
       'meta_value' =>'',
       'post_type' => 'post',
       'post_status' => 'publish',
       'suppress_filters' => true,
     );

       
      
      if (have_posts()) : while (have_posts()) : the_post();

      ?>
      <!-- Image Box -->
      <?php 
      $category = get_the_category();      
      ?>
      


    <!-- <div class="masonry-item col-md-4 col-sm-6 col-xs-12 col-lg-4" >    -->
    <div class="<?php echo $category[0]->cat_name; ?> masonry-item " style="border: 1px solid green" >   
      <div class="image-box " >
          <a class="" href="<?php the_permalink();?>">
             <div class="posts " >
               <div class="image ">
                 <img class="b-lazy" data-src="<?php echo wp_get_attachment_url( get_post_thumbnail_id()); ?>"  >
               </div>
               <div class="work">
                  <p class="fecha">
                    <time datetime="<?php the_time('Y-m-d'); ?>">
                    <?php the_time('F j, Y'); ?>
                    </time>
                  </p>
                 <h4 class="mb-0 p-0-1"><?php the_title(); ?></h4>
                 <div class="extracto "><?php the_excerpt(); ?></div>
               </div>
             </div>
           </a>
        </div>
      </div>   

<!--
    <div class="<?php echo $category[0]->cat_name; ?>col-md-4 col-sm-6 col-xs-12 col-lg-4" >   


      <div class="image-box-2">
      <div class="image">
        <img src="<?php echo wp_get_attachment_url(get_post_thumbnail_id()); ?>" style="background-size:100% 100%;" >
      </div>
      <div class="hover">        
        <a class="modal-link" >

          <div class="circular--portrait-2" style="margin-bottom: 10%;">
            <div class="bg"></div>
            <h4 class="mb-0 p-0-1"><?php the_title(); ?></h4>
            <a class="btn btn-primary" href="<?php the_permalink();?>">Leer Mas</a>
          </div>
        </a>
      </div>
    </div>


  </div>    -->



  <?php endwhile; else : ?>

  <div class="jumbotron">
    <div class="container">
      <h1>Lo sentimos</h1>
      <p><?php _e( 'Pero en estos momentos no hay blogs iniciados.' ); ?></p>
      <p>
        <a class="btn btn-primary btn-lg" href="<?php echo get_bloginfo("url").'/'; ?>">Volver</a>
      </p>
    </div>
  </div>
  <?php endif; ?>      
  </div>
  </section>
  
  <?php
   global $wp_query;
   
  $total_pages = $wp_query->max_num_pages;
   
  if ($total_pages > 1){
   
    $current_page = max(1, get_query_var('paged'));
     
    echo '<div class="page_nav">';
     
    echo paginate_links(array(
        'base' => get_pagenum_link(1) . '%_%',
        'format' => '/page/%#%',
        'current' => $current_page,
        'total' => $total_pages,
        'prev_text' => '<i class="fa fa-arrow-left"></i>',
        'next_text' => '<span class="mas">Más</span><i class="fa fa-arrow-right"></i>'
      ));
   
    echo '</div>';
     
  }
  ?>
</div>


<?php get_footer();?>

<!-- ========== START CONTROL CONTACT POPUP ========== -->
<!-- <div class="contact-popup-ctrl">
  <a href="" class="contact-popup-toggle" data-toggle="modal" data-target="#contact-popup">
    <span class="typing" data-text="[&quot;¿Tienes una Pregunta?&quot;,&quot;¡Escribenos!&quot;]">¡Escribenos!</span><span class="typed-cursor"></span>
  </a>
</div> -->
<!-- ========== END CONTROL CONTACT POPUP ========== -->

<!-- ========== START MODAL CONTACT POPUP ========== -->
<!-- <div id="contact-popup" class="modal fade">
  <div class="modal-dialog" style="font-family: Poppins, Helvetica, Arial, sans-serif;">
    <div class="modal-content" style="border-radius: 8px; height: auto;">
      <div class="modal-header" style="font-weight: bold; color: #333; letter-spacing: 2px; background-color: #ffcc00; border-radius: 8px 8px 0px 0px;">
        CONTACTANOS
      </div>
      <div class="modal-body margin-contact" style="padding-bottom: 0px;">
        <?php echo do_shortcode('[contact-form-7 id="70" title="Formulario de contacto 1"]')?>
      </div>
    </div>
  </div>
</div> -->
<!-- ========== END MODAL CONTACT POPUP ========== -->