
    <footer class="sub_footer">
        <div class="container" style="border-bottom: 1px #ffffff solid;">
            <div class="col-md-3">

                <ul class="footer-list footer-ul">
                    <li>QUIENES SOMOS</li>
                    <li>BENEFICIOS</li>
                    <li>PLANES</li>
                    <li>PRENSA</li>
                    <li>COTIZA</li>
                    <li>BLOG</li>
                    <li>BENEFICIOS</li>
                    <li>CONTACTO</li>
                </ul>

            </div>
            <div class="col-md-3 symbols-footer">
                    <div style="display: inline-flex;">
                    <div class="symbol-item-footer" style="margin: 0 15% 0 1%;padding: 1%;">
                        <img class="process__img-2 b-lazy" style="size: 2em;" data-src="<?php echo get_template_directory_uri();?>/assets/images/footer/instagram-logo.png" alt="Revisar Medidas">
                    </div>
                    <div class="symbol-item-footer" style="margin: 0 15% 0 1%;padding: 1%;">
                        <img class="process__img-2 b-lazy" data-src="<?php echo get_template_directory_uri();?>/assets/images/footer/instagram-logo.png" alt="Revisar Medidas">
                    </div>
                    </div>
                    <div class="symbols-footer2" style="margin-top: 3%;">
                        <h2>Con el apoyo de:</h2>
                    <div class="symbols-sponsor" style="margin-top: 2%;">
                        <div style="padding-left: 11%;">
                            <img class="x" style="width: 100%;" src="<?php echo get_template_directory_uri(); ?>/assets/images/LogosFooter/startup-chile-side-banner-2016-06-23-B.png">
                        </div>
                        <div style="padding-bottom: 3%;">
                            <img class="x" style="width: 48%; top: 1%;" src="<?php echo get_template_directory_uri(); ?>/assets/images/LogosFooter/Logo_Corfo-B.png">
                        </div>
                    </div>
                </div>    
            </div> 
            <div class="col-md-3 symbols-footer" style="padding-top: 1%; width:28%;" >
                <div>
                    <h2 style="color: white; font-family: 'CooperHewitt-Bold'; font-size: 0.8em;">Inscribete a nuestro newsletter:</h2>
                </div>
                <div class="inputWithIcon inputIconBg">
                  <input type="text" placeholder="Email">

              </div>
                <div style="align-self: left;">
                    <button class="btn btn-primary2">ENVIAR</button>
                </div>
            </div>   
            <div class="col-md-3 " style="width: 22%; font-family: 'CooperHewitt-Light'; padding:0; margin-top: 2%;">
                <p style="color: #666666;">&#9400; 2015 TuCasa<em style="color: #ff6600;">Ok</em></p>
                <p style="color: #666666;">Todos los derechos reservados.</p>
            </div>        
        </div><div></div>

        <div class="container symbols-footer-sub">
            <p class="footer-p ">Terminos y Condiciones</p><span class="footer-p ">|</span><p class="footer-p ">Politica de Privacidad</p><span class="footer-p ">|</span><p class="footer-p ">Manual de Tolerancia</p><span class="footer-p ">|</span><p class="footer-p">Tus Derechos</p>
        </div>
    </footer>
    
    <?php wp_footer(); ?>

    <script type="text/javascript" src="<?php echo get_template_directory_uri();?>/assets/js/jquery-1.10.2.min.js"> </script>
    <script type="text/javascript" src="<?php echo get_template_directory_uri();?>/assets/js/bootstrap.min.js" ></script>
    <script type="text/javascript" src="<?php echo get_template_directory_uri();?>/assets/js/owl.carousel.min.js"> </script>
    <script type="text/javascript" src="<?php echo get_template_directory_uri();?>/assets/js/blazy.min.js" ></script>
    <script type="text/javascript" src="<?php echo get_template_directory_uri();?>/assets/js/jquery.mixitup.min.js" ></script>
    <script type="text/javascript" src="<?php echo get_template_directory_uri();?>/assets/js/main.js" ></script>
