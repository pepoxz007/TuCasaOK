var gulp = require('gulp'),
concat = require('gulp-concat'),
minify = require('gulp-minifier'),
uglify = require('gulp-uglify'),
imagemin = require('gulp-imagemin'),
sourcemaps = require('gulp-sourcemaps'),
del = require('del'),
gutil = require('gulp-util'),
js="assets/js/";
css="assets/css/";


var paths = {
  scripts: [
  js+'libs/**/*.js',
  ],
  styles:[
  css+'**/*.css',
  ],
  images: 'assets/images/**/*'
};

var minifiedOptions={
  minify: true,
  collapseWhitespace: true,
  conservativeCollapse: true,
  minifyJS: true,
  minifyCSS: true,
  getKeptComment: function (content, filePath) {
    var m = content.match(/\/\*![\s\S]*?\*\//img);
    return m && m.join('\n') + '\n' || '';
  }
}


// Not all tasks need to use streams
// A gulpfile is just another node program and you can use any package available on npm
gulp.task('clean', function() {
  return del(['build']);
});

gulp.task('scripts', ['clean'], function() {

  gulp.src(paths.scripts)
  .pipe(sourcemaps.init())
  .pipe(minify(minifiedOptions))
  .pipe(sourcemaps.write())
  .pipe(gulp.dest('build/js'))

});

gulp.task('styles', ['clean'], function() {

  gulp.src(paths.styles)
  .pipe(sourcemaps.init())
  .pipe(minify(minifiedOptions))
  .pipe(sourcemaps.write())
  .pipe(gulp.dest('build/css'))

});

// Copy all static images
gulp.task('images', ['clean'], function() {
  return gulp.src(paths.images)
    // Pass in options to the task
    .pipe(imagemin({optimizationLevel: 5}))
    .pipe(gulp.dest('build/img'));
  });

// Rerun the task when a file changes
gulp.task('watch', function() {
  gulp.watch(paths.scripts, ['build']);
  gulp.watch(paths.styles, ['build']);
  gulp.watch(paths.images, ['images']);
});

// The default task (called when you run `gulp` from cli)
gulp.task('default', ['watch', 'scripts', 'styles', 'images']);
gulp.task('build', ['scripts', 'styles']);