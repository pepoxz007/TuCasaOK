<?php
/**
 * Displays top navigation
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.2
 */

$baseUrl=get_site_url();
$baseUrlTheme=get_template_directory_uri();
$type=is_front_page()?1:0;
$url=(!$type?$baseUrl.'/':'');
$activeClass=(!$type?'active':'');
?>


    <nav class="navbar navbar-default navbar-fixed-top" role="navigation">
            <div class="container">
                <div class="row">
                <!-- Brand and toggle get grouped for better mobile display -->
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <a class="navbar-brand" href="#">
                            <img src="<?php echo $baseUrlTheme; ?>/assets/images/logo.png" alt="">
                        </a>
                    </div>

                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                        <ul class="nav navbar-nav main-nav  clear navbar-right ">
                            <li><a class="navactive color_animation" href="<?php echo $url; ?>#quienes-somos">QUIENES SOMOS</a></li>
                            <li><a class="color_animation" href="<?php echo $url; ?>#plans">PLANES</a></li>
                            <li><a class="color_animation" href="<?php echo $url; ?>#budget">COTIZA</a></li>
                            <li><a class="color_animation" href="<?php echo $url; ?>#team">BENEFICIOS</a></li>
                            <li><a class="color_animation" href="<?php echo $url; ?>#blog">PRENSA</a></li>
                            <li><a class="color_animation" href="<?php echo get_bloginfo("url").'/blog/'; ?>">BLOG</a></li>
                            <li><a class="color_animation" href="<?php echo $url; ?>#testimony">CONTACTANOS</a></li>
                            <li><a><button class="btn btn-primary">RESERVA HOY <i class="far fa-user-circle"></i></button></a></li>                        </ul>
                    </div><!-- /.navbar-collapse -->
                </div>
            </div><!-- /.container-fluid -->
        </nav>

