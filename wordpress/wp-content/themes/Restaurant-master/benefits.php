<?php /* Template Name: Benefits */ ?>
<?php get_header(); ?>
<?php get_template_part( 'template-parts/navigation/navigation-top', null ); ?>

<section id="benefits" class="container-fluid unfilled">
	<div class="row sections-row unfilled">
		<div class="col-xs-12 unfilled">
			<img class="img-responsive" src="<?php echo get_template_directory_uri();?>/assets/images/benefits/img-header.png">
		</div>
		<div class="col-xs-12 unfilled">
			<h2 class="sections-title">Beneficios</h2>
		</div>
		<div class="col-md-offset-1 col-sm-offset-2 col-xs-offset-1 col-md-5 col-sm-8 col-xs-10">
			<p class="sections-subtitle" style="font-family: 'Helvetica-Bold';">
				DESCUENTOS EXCLUSIVOS SÓLO PARA TI
			</p>
		</div>
		<div class="col-md-offset-0 col-sm-offset-2 col-xs-offset-1 col-md-5 col-sm-8 col-xs-10">
			<p class="sections-subtitle" style="font-family: 'Helvetica-Thin';">
				Creamos alianzas estratégicas para que TÚ, nuestro cliente, puedas gozar de beneficios exclusivos al usar <strong style="font-family: Helvetica-Bold;">TUCASA<span style="color: #ff6600;">OK</span></strong>
			</p>
		</div>
	</div>
	
	<!-- #################### B·E·N·E·F·I·T·S #################### -->

	<div class="row sections-row">
		<div class="st-item all-item col-sm-offset-2 col-xs-offset-1 col-md-3 col-sm-4 col-xs-10">
			<div class="santander">
				<img class="img-responsive" src="<?php echo get_template_directory_uri();?>/assets/images/benefits/santander.png"/>
				<p class="text-item">Recibe el 10% de descuento usando tu tarjeta del banco Santander</p>
			</div>
		</div>
		<div class="all-item col-md-offset-0 col-sm-offset-0 col-xs-offset-1 col-md-3 col-sm-4 col-xs-10">
			<div class="capitalizarme">
				<img class="img-responsive" src="<?php echo get_template_directory_uri();?>/assets/images/benefits/capitalizarme.png"/>
				<p class="text-item">Recibe el 10% de descuento en Capitalizarme.com</p>
			</div>
		</div>
		<div class="all-item col-md-offset-0 col-sm-offset-2 col-xs-offset-1 col-md-3 col-sm-4 col-xs-10">
			<div class="mibodega">
				<img class="img-responsive" src="<?php echo get_template_directory_uri();?>/assets/images/benefits/mibodega.png"/>
				<p class="text-item">Recibe el 10% de descuento en Mi Bodega</p>
			</div>
		</div>
		<div class="st-item all-item col-md-offset-0 col-sm-offset-0 col-xs-offset-1 col-md-3 col-sm-4 col-xs-10">
			<div class="moviglass">
				<img class="img-responsive" src="<?php echo get_template_directory_uri();?>/assets/images/benefits/moviglass.png"/>
				<p class="text-item">Recibe el 10% de descuento en Moviglass para cierre de terrazas en Santiago</p>
			</div>
		</div>
		<div class="all-item col-md-offset-0 col-sm-offset-2 col-xs-offset-1 col-md-3 col-sm-4 col-xs-10">
			<div class="lumos">
				<img class="img-responsive" src="<?php echo get_template_directory_uri();?>/assets/images/benefits/lumos.png"/>
				<p class="text-item">Recibe el 25% de descuento en cortinas Lumos en Santiago</p>
			</div>
		</div>
		<div class="all-item col-md-offset-0 col-sm-offset-0 col-xs-offset-1 col-md-3 col-sm-4 col-xs-10">
			<div class="wenuwork">
				<img class="img-responsive" src="<?php echo get_template_directory_uri();?>/assets/images/benefits/wenuwork.png"/>
				<p class="text-item">Recibe hasta un 15% de descuento en Wenu Work</p>
			</div>
		</div>
	</div>
</section>

<?php get_footer(); ?>
</body>
</html>