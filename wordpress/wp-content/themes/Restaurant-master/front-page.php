<?php
/**
* The front page template file
*
* If the user has selected a static page for their homepage, this is what will
* appear.
* Learn more: https://codex.wordpress.org/Template_Hierarchy
* @package WordPress
* @subpackage Twenty_Seventeen
* @since 1.0
* @version 1.0
*/
define( 'WP_USE_THEMES', true );
get_header(); 
get_template_part( 'template-parts/navigation/navigation-top', null );

?>





<div id="start" class="starter_container bg">
    <div class="follow_container center-all direction-c">

        
        <div class="work">
        <h2 class="top-title stroke">Comprobamos las garantias de tus compras inmobiliarias</h2>
        <h2 class="second-title" style="color: white;">Revisamos tus inmuebles para que estes satisfecho con tu compra</h2>
        </div>
        
        <button class="btn btn-primary uppercase text-center no-borderR" style="margin-top: 1%;">Comprueba tu inmueble </button>
    </div>
</div>

<div class="light-gray" style="padding: 2%;">
    <h2 align="center" style="font-family: 'Helvetica-Regular'">Antes de firmar el acta de entrega, contáctanos para asesorarte. <a href="<?php ?>" style="font-family: 'Helvetica-Oblique'">Conócenos hoy</a></h2>
</div>



<!-- ============ Descansa y deja la inspeccion... ============= -->

<section id="description" class="description_content">
    <div class="process container-fluid">
        <h2 style="font-family: 'Helvetica-Bold'; font-size: 3em;">Descansa y deja la inspección en nuestras manos</h1>
        <section>
            <div class="col-lg-6 col-sm-6 capsule" style="margin-top: 2%;">
            <h3 class="process__title" align="justify" style="font-family: 'CooperHewitt-Light'; color: black;"><em style="font-family: 'CooperHewitt-Bold'; color: black;">TUCASA</em><em style="font-family: 'CooperHewitt-Bold' ; color: #ff6600;">OK</em> inspecciona propiedades nuevas, con el fin de que la recibas en perfectas condiciones y hagas valer las garantías asociadas a tu compra. Resguardando la seguridad de tu familia y el valor de TÚ INVERSIÓN.</h3>
            </div>
            <div class="col-lg-6 col-sm-6 capsule" style="margin-top: 2%;">
            <h3 class="process__title" align="justify" style="font-family: 'CooperHewitt-Light'; color: black;">Nuestros profesionales utilizan la mejor tecnología y están en constantes capacitaciones para entregarte el mejor servicio al revisar tu propiedad. Verificamos instalaciones, terminaciones y medimos para indicar la cantidad exacta de metros cuadrados de tu vivienda.</h3>
            </div>
            <div class="symbols-2" >
            <div class="symbol-item" >
                <img class="process__img b-lazy" data-src="<?php echo get_template_directory_uri();?>/assets/images/Iconos_Primeros/iconos-01.png" alt="Revisar Medidas">
                <h3 class="process__title"  style="text-align: center;  font-family: 'CooperHewitt-Medium' ;margin-bottom: 4%;">Dimensiones</h3>
            </div>
            <div class="symbol-item ">
                <img class="process__img b-lazy" data-src="<?php echo get_template_directory_uri();?>/assets/images/Iconos_Primeros/iconos-02.png" alt="Fugas Gas">
                <h3 class="process__title" style=" font-family: 'CooperHewitt-Medium' ;margin-bottom: 4%;">Fuga de Gas</h3>
            </div>
            <div class="symbol-item ">
                <img class="process__img b-lazy" data-src="<?php echo get_template_directory_uri();?>/assets/images/Iconos_Primeros/iconos-03.png" alt="Electricidad">
                <h3 class="process__title" style=" font-family: 'CooperHewitt-Medium' ;margin-bottom: 4%;">Electricidad</h3>
            </div>
            <div class="symbol-item ">
                <img class="process__img b-lazy" data-src="<?php echo get_template_directory_uri();?>/assets/images/Iconos_Primeros/iconos-04.png" alt="Filtraciones de Agua">
                <h3 class="process__title" style=" font-family: 'CooperHewitt-Medium' ;margin-bottom: 4%;">Filtraciones de Agua</h3>
            </div>
            <div class="symbol-item ">
                <img class="process__img b-lazy" data-src="<?php echo get_template_directory_uri();?>/assets/images/Iconos_Primeros/iconos-05.png" alt="Termografía">
                <h3 class="process__title" style=" font-family: 'CooperHewitt-Medium' ;margin-bottom: 4%;">Termografía</h3>
            </div>
            <div class="symbol-item ">
                <img class="process__img b-lazy" data-src="<?php echo get_template_directory_uri();?>/assets/images/Iconos_Primeros/iconos-06.png" alt="Terminaciones">
                <h3 class="process__title" style=" font-family: 'CooperHewitt-Medium' ;margin-bottom: 4%;">Terminaciones</h3>
            </div>
            </div>
        </section>
    </div>
</section>

<!-- ============ Como Funciona  ============= -->

<section id ="how" class="description_content gray">
    <div class=""> 
        <div>
            <h1 class="section__subtitle" style="font-family:'Helvetica-Bold'; font-size: 3em; color: #ffffff;">¿Como Funciona?</h1>
        </div>
        <div class="symbols">
            <div class="symbol-item ">
                <img class="process__img b-lazy" data-src="<?php echo get_template_directory_uri();?>/assets/images/Iconos_ComoFunciona/ComoFunciona-01.png" alt="Fugas Gas">
                <h3 class="process__title" style=" font-family: 'Arial-Narrow' ;margin-bottom: 4%; color: #ffffff;">Contacto y Cotizacion</h3>
            </div>
            <div class="symbol-item ">
                <img class="process__img b-lazy" data-src="<?php echo get_template_directory_uri();?>/assets/images/Iconos_ComoFunciona/ComoFunciona-02.png" alt="Fugas Gas">
                <h3 class="process__title" style=" font-family: 'Arial-Narrow' ;margin-bottom: 4%; color: #ffffff;">Agendar inspección, idealmente en la pre-entrega de la propiedad</h3>
            </div>
            <div class="symbol-item ">
                <img class="process__img b-lazy" data-src="<?php echo get_template_directory_uri();?>/assets/images/Iconos_ComoFunciona/ComoFunciona-03.png" alt="Fugas Gas">
                <h3 class="process__title" style=" font-family: 'Arial-Narrow' ;margin-bottom: 4%; color: #ffffff;">Se inspecciona de acuerdo al Manual de Tolerancia de la CChC</h3>
            </div>
            <div class="symbol-item ">
                <img class="process__img b-lazy" data-src="<?php echo get_template_directory_uri();?>/assets/images/Iconos_ComoFunciona/ComoFunciona-04.png" alt="Fugas Gas">
                <h3 class="process__title" style=" font-family: 'Arial-Narrow' ;margin-bottom: 4%; color: #ffffff;">Entregamos el informe, el cual debes enviar a lainmobiliaria</h3>
            </div>
            <div class="symbol-item ">
                <img class="process__img b-lazy" data-src="<?php echo get_template_directory_uri();?>/assets/images/Iconos_ComoFunciona/ComoFunciona-05.png" alt="Fugas Gas">
                <h3 class="process__title" style=" font-family: 'Arial-Narrow' ;margin-bottom: 4%; color: #ffffff;">La constructura repara</h3>
            </div>
            <div class="symbol-item ">
                <img class="process__img b-lazy" data-src="<?php echo get_template_directory_uri();?>/assets/images/Iconos_ComoFunciona/ComoFunciona-06.png" alt="Fugas Gas">
                <h3 class="process__title" style=" font-family: 'Arial-Narrow' ;margin-bottom: 4%; color: #ffffff;">Se chequea la correcta ejecución de los trabajos</h3>
            </div>
            <div class="symbol-item ">
                <img class="process__img b-lazy" data-src="<?php echo get_template_directory_uri();?>/assets/images/Iconos_ComoFunciona/ComoFunciona-07.png" alt="Fugas Gas">
                <h3 class="process__title" style=" font-family: 'Arial-Narrow' ;margin-bottom: 4%; color: #ffffff;">Firmas el acta de entrega y disfrutas tú hogar</h3>
            </div>
        </div>
        <div>
            <a><button class="btn btn-secundary" style="margin-top: 2%;">VER VIDEO</button></a>
        </div>
    </div>
</section>

<!-- ======== LIDERES DEL MERCADO.... ==== -->
<section id="description" class="description_content_h">
    <div class="symbols-h">
        <div class="symbol-item-h">
            <div class="">
                <img class="process__img_h b-lazy" data-src="<?php echo get_template_directory_uri();?>/assets/images/Iconos_arribadeNuestroEquipo/iconos-01.png" alt="Proteger">
           
                <h3 class="process__title_h">Líderes en el mercado</h3>
            </div>
        </div>
        <div class="symbol-item-h">
            <div class="">
                <img class="process__img_h b-lazy" data-src="<?php echo get_template_directory_uri();?>/assets/images/Iconos_arribadeNuestroEquipo/iconos-02.png" alt="Profesionales">
           
                <h3 class="process__title_h">Sólo Profesionales</h3>
            </div>
        </div>
        <div class="symbol-item-h">
            <div class="">
                <img class="process__img_h b-lazy" data-src="<?php echo get_template_directory_uri();?>/assets/images/Iconos_arribadeNuestroEquipo/iconos-03.png" alt="Mejor Metodologia">
           
                <h3 class="process__title_h">La Mejor Tecnología</h3>
            </div>
        </div>
        <div class="symbol-item-h">
            <div class="">
                <img class="process__img_h b-lazy" data-src="<?php echo get_template_directory_uri();?>/assets/images/Iconos_arribadeNuestroEquipo/iconos-04.png" alt="Respuesta Inmediata">
           
                <h3 class="process__title_h">Respuesta Inmediata</h3>
            </div>
        </div>
        <div class="symbol-item-h">
            <div class="">
                <img class="process__img_h b-lazy" data-src="<?php echo get_template_directory_uri();?>/assets/images/Iconos_arribadeNuestroEquipo/iconos-05.png" alt="Nos Comprometemos">
           
                <h3 class="process__title_h">Nos Comprometemos</h3>
            </div>
        </div>
        <div class="symbol-item-h">
            <div class="">
                <img class="process__img_h b-lazy" data-src="<?php echo get_template_directory_uri();?>/assets/images/Iconos_arribadeNuestroEquipo/iconos-06.png" alt="Innovadores">
           
                <h3 class="process__title_h">Innovadores</h3>
            </div>
        </div>
        <div class="symbol-item-h">
            <div class="">
                <img class="process__img_h b-lazy" data-src="<?php echo get_template_directory_uri();?>/assets/images/Iconos_arribadeNuestroEquipo/iconos-07.png" alt="Vanguardistas">
           
                <h3 class="process__title_h">Vanguardistas</h3>
            </div>
        </div>
    </div>
</section>

<!-- ===================================================================== -->
<!-- =#=#=#=#=#=#=#=#=#=#=  S·E·C·T·I·O·N - T·E·A·M  =#=#=#=#=#=#=#=#=#=#= -->
<!-- ===================================================================== -->

<section id="section-team" class="container-fluid unfilled">
	<div class="row sections-row unfilled back-team">
		<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 unfilled">
			<div>
				<img class="img-responsive" src="<?php echo get_template_directory_uri();?>/assets/images/team/img_team_one.jpg"/>
			</div>
		</div>
		<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 unfilled">
			<div>
				<h2 class="padding-top-one">Nuestro Equipo</h2>
				<p class="margin-p">
					<strong style="font-family: Helvetica-Bold;">TUCASA<span style="color: #ff6600;">OK</span></strong> fue fundada en Enero del 2015 con una visión clara: Ser la empresa <strong>#1</strong> en inspección de propiedades en Chile y Latam. Estableciendo nuestro servicio como un “must” en el proceso de cambio de casa.
				</p>
				<p class="margin-p">
					Nuestra misión es entregar tranquilidad a nuestros clientes, informando sobre el estado de su propiedad haciendo valer sus garantías y derechos. Permitiéndoles obtener una, propiedad en perfectas condiciones.
				</p>
				<div class="margin-button" style="text-align: center;">
					<a href="<?php echo get_bloginfo('wurl'); ?>/team/"><button class="btn btn-default">CONÓCENOS</button></a>
				</div>
			</div>
		</div>
	</div>
	<div class="row sections-row unfilled back-team">
		<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 unfilled">
			<div>
				<p class="padding-top-two">
					“El corazón de lo que hacemos está motivado por ser las mejores, velando por que las personas habiten felices y tranquilos. Entregando un servicio eficiente, rápido, profesional, de alta calidad y a un valor accesible, logrando que nuestro servicio sea incorporado con naturalidad al proceso de compra de la propiedad, primero en Chile y luego en Latinoamérica.
				</p>
				<p class="margin-p">
					Llevamos más de 1000 inspecciones, creciendo a un ritmo emocionante...”
				</p>
			</div>
		</div>
		<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 unfilled">
			<div>
				<img class="img-responsive" src="<?php echo get_template_directory_uri(); ?>/assets/images/team/img_team_two.jpg"/>
			</div>
		</div>
	</div>
</section>

<!-- ===================================================================== -->
<!-- =#=#=#=#=#=#=#=#=#=#=  S·E·C·T·I·O·N - P·L·A·N·E·S  =#=#=#=#=#=#=#=#=#=#= -->
<!-- ===================================================================== -->

<section id="planes" class="description_content">
    <div class="process container-fluid">
        <h2 style="font-family: 'Helvetica-Bold'; font-size: 2em;" class="t">Planes</h2>
    </div>
    <div class="container">
        <div class="box-external-1">
            <div style="padding: 0.5% 0%; margin-bottom: 1%; background: #ff6600;">
                <h2>Inspeccion de Propiedades Nuevas</h2>
            </div>
            <div class="box-internal-1">
                <div class="" style="padding: 5% 0%; border-bottom: 1px #666666 solid; background: #ff6600;">
                   <h2 align="center" style="margin: 1% 5%; color: #ffffff;">Basico</h2>
                   <h2>$1400 m2</h2>
                </div>
                <hr>
                <div>
                    <p>QLQ BECERRO</p>
                </div>
            </div>
            <div class="box-internal-1">
                <div class="" style="padding: 5% 0%; border-bottom: 1px #666666 solid; background: #ff6600;">
                   <h2 align="center" style="margin: 1% 5%; color: #ffffff;">Basico</h2>
                   <h2>$1400 m2</h2>
                </div>
                <hr>
                <div>
                    <p>QLQ BECERRO</p>
                </div>
            </div>
        </div>
            <div class="box-internal-2"></div>
            <div class="box-internal-2"></div>
    </div>
</section>

<!-- ============================================================================= -->
<!-- =#=#=#=#=#=#=#=#=#=#=  S·E·C·T·I·O·N - RESERVA  =#=#=#=#=#=#=#=#=#=#= -->
<!-- ============================================================================= -->

<section id="planes" class="description_content" style="background-color: rgb(244,244,244);">
    <div class="process container-fluid">
        <h2 style="font-family: 'Helvetica-Bold'; font-size: 2em;">Reserva Hoy</h2>
    </div>
    <div class="container">
        <div class="col-lg-4" style="border: 2px red solid; padding: 0 0;">
            <div class="" style="padding: 5% 0%; border-bottom: 1px #666666 solid; background: #ff6600;">
               <h2 align="left" style="margin: 1% 5%; color: #ffffff;"><em style="border: 1px #ffffff solid ;border-radius: 100px; margin-right: 5%; padding: 4% 5%;">1</em>¿Qué necesitas?</h2>
            </div>
            <div>
                <select name="Categorias" style="border-radius: 20px; background: #ffffff; padding: 1% 25%; margin: 5% 2%;">
                    <option>Escoge tu plan</option>
                    <option>Básico</option>
                    <option>Clásico</option>
                    <option>Premium</option>
                    <option>Garantias</option>
                    <option>Chequeo</option>
                    <option>Chequeo</option>
                </select>
            </div>
            <div style="padding: 2% 8%;">
                <input type="text1" placeholder="Indica m2 útiles(sólo números)" style="padding: 2% 2%;">
            </div>
        </div>
        <div class="col-lg-4" style="border: 2px red solid;">
            <div>
                Que necesitas
            </div>
            <div>
                <input type="" name="Indica m2 útiles(sólo números)">
            </div>
        </div>
        <div class="col-lg-4" style="border: 2px red solid;">
            <div>
                Que necesitas
            </div>
            <div>
                <input type="" name="">
            </div>
        </div>
    </div>
</section>


<!-- ============================================================================= -->
<!-- =#=#=#=#=#=#=#=#=#=#=  S·E·C·T·I·O·N - B·E·N·E·F·I·T·S  =#=#=#=#=#=#=#=#=#=#= -->
<!-- ============================================================================= -->

<section id="section-benefits" class="container-fluid unfilled">
	<div class="row sections-row unfilled">
		<div class="col-xs-12 unfilled">
			<h2 class="sections-title">Beneficios</h2>
		</div>
		<div class="sm-margin sm-padding col-md-offset-1 col-sm-offset-0 col-xs-offset-1 col-md-4 col-sm-4 col-xs-10">
			<p class="sections-subtitle">
				DESCUENTOS EXCLUSIVOS SÓLO PARA TI
			</p>
		</div>
		<div class="sm-padding col-md-offset-0 col-sm-offset-0 col-xs-offset-1 col-md-5 col-sm-5 col-xs-10">
			<p class="sections-subtitle" style="font-family: 'Helvetica-Thin';">
				Creamos alianzas estratégicas para que TÚ, nuestro cliente, puedas gozar de beneficios exclusivos al usar <strong style="font-family: Helvetica-Bold;">TUCASA<span style="color: #ff6600;">OK</span></strong>
			</p>
		</div>
		<div class="link-container col-md-offset-0 col-sm-offset-0 col-md-2 col-sm-2 col-xs-12">
			<a id="link-benefits" href="<?php echo get_bloginfo('wurl'); ?>/benefits/">VER BENEFICIOS</a>
		</div>
	</div>
	<div class="row sections-row unfilled">
		<div class="col-md-offset-2 col-sm-offset-2 col-xs-offset-1 col-md-4 col-sm-4 col-xs-10">
			<img class="img-responsive img-item" src="<?php echo get_template_directory_uri();?>/assets/images/benefits/santander_section.png"/>
		</div>
		<div class="col-md-offset-0 col-sm-offset-0 col-xs-offset-1 col-md-4 col-sm-4 col-xs-10">
			<img class="img-responsive img-item" src="<?php echo get_template_directory_uri();?>/assets/images/benefits/capitalizarme_section.png"/>
		</div>
	</div>
</section>

<!-- ============================================================================= -->
<!-- =#=#=#=#=#=#=#=#=#=#=  S·E·C·T·I·O·N - PRENSA =#=#=#=#=#=#=#=#=#=#= -->
<!-- ============================================================================= -->


<section id="bread" class=" description_content light-gray capsule">
    <h1 class="section__subtitle">Prensa</h1>
    <div class="container-fluid">
        <div class="owl-carousel">

            <div class="post-box item">
                <img data-src="<?php echo get_template_directory_uri();?>/assets/images/posts/post-1.jpg" class="img-responsive b-lazy " alt="Image">
                <label class="post-title">Titulo</label>
                <button class="btn btn-primary text-center">Ver Más</button>
            </div>
            <div class="post-box item" >
                <img data-src="<?php echo get_template_directory_uri();?>/assets/images/posts/post-2.jpg" class="img-responsive b-lazy " alt="Image">
                <label class="post-title">Titulo</label>
                <button class="btn btn-primary text-center">Ver Más</button>
            </div>
            <div class="post-box item" >
                <img data-src="<?php echo get_template_directory_uri();?>/assets/images/posts/post-3.jpg" class="img-responsive b-lazy " alt="Image">
                <label class="post-title">Titulo</label>
               <button class="btn btn-primary text-center">Ver Más</button>
            </div>
            <div class="post-box item" >
                <img data-src="<?php echo get_template_directory_uri();?>/assets/images/posts/post-4.jpg" class="img-responsive b-lazy " alt="Image">
                <label class="post-title">Titulo</label>
                <button class="btn btn-primary text-center">Ver Más</button>
            </div>
            <div class="post-box item" >
                <img data-src="<?php echo get_template_directory_uri();?>/assets/images/posts/post-5.jpg" class="img-responsive b-lazy " alt="Image">
                <label class="post-title">Titulo</label>
                <button class="btn btn-primary text-center">Ver Más</button>
            </div>
            <div class="post-box item" >
                <img data-src="<?php echo get_template_directory_uri();?>/assets/images/posts/post-6.jpg" class="img-responsive b-lazy " alt="Image">
                <label class="post-title">Titulo</label>
               <button class="btn btn-primary text-center">Ver Más</button>
            </div>
            <div class="post-box item" >
                <img data-src="<?php echo get_template_directory_uri();?>/assets/images/posts/post-7.jpg" class="img-responsive b-lazy " alt="Image">
                <label class="post-title">Titulo</label>
                <button class="btn btn-primary text-center">Ver Más</button>
            </div>

        </div>
    </div>
    <div class="center-all">
        <a class="view-more">
            <span>Ver mas</span>
            <i class="fas fa-angle-down"></i>
        </a>
    </div>
</section>



<!-- ============ Featured Dish  ============= -->

<section id="featured" class="description_content">
    <div  class="featured background_content">
        <h1>Excelente Comida, Precios inigualables!</h1>
    </div>
    <div class="text-content container">
        <h1 class="section__subtitle">Opiniones</h1>
        <div class="row">
            <div class="opinions-box direction-c col-md-4 capsule">
                <i class="fa fa-quote-left"></i>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Animi veritatis, dicta consequuntur aspernatur rem iusto blanditiis! Maxime ipsam consequatur eveniet, fugiat fugit cumque ipsa, assumenda ea! Ducimus, nemo quae debitis.</p>
                <div class="direction-r">
                    <img data-src="<?php echo get_template_directory_uri(); ?>/assets/images/cheft.png" class="img-responsive b-lazy  img-circle" alt="Image">
                    <div class="direction-c">
                        <label for="">Thomas L.</label>
                        <span for="">Clases de Cocina</span>
                    </div>
                </div>
            </div>
            <div class="opinions-box direction-c col-md-4 capsule">
                <i class="fa fa-quote-left"></i>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Odit rerum laborum, voluptates, veniam itaque ea nemo voluptas, eos laudantium laboriosam repellat eum. Voluptatibus, velit, expedita. Ratione neque eos sint dolorum?</p>
                <div class="direction-r">
                    <img data-src="<?php echo get_template_directory_uri(); ?>/assets/images/cheft.png" class="img-responsive b-lazy  img-circle" alt="Image">
                    <div class="direction-c">
                        <label for="">Thomas L.</label>
                        <span for="">Clases de Cocina</span>
                    </div>
                </div>
            </div>
            <div class="opinions-box direction-c col-md-4 capsule">
                <i class="fa fa-quote-left"></i>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Iusto quia est, mollitia minima eum asperiores vel consequatur qui quas praesentium, voluptates possimus officia beatae totam. At facere rerum officia, aperiam.</p>
                <div class="direction-r">
                    <img data-src="<?php echo get_template_directory_uri(); ?>/assets/images/cheft.png" class="img-responsive b-lazy  img-circle" alt="Image">
                    <div class="direction-c">
                        <label for="">Thomas L.</label>
                        <span for="">Clases de Cocina</span>
                    </div>
                </div>
            </div>

        </div>
    </div>
</section>

<!-- ============ Reservation  ============= -->

<section  id="contactUs" data-src="<?php echo get_template_directory_uri();?>/assets/images/newsletter.jpg"  class="b-lazy description_content gray">
    <div class="container">
        <h1 class="section__subtitle">Enterate de primero</h1>
        <p class="sub-title">Inscribete a nuestro newsletter y enterate de nuevas cenas, cursos y toures de primero</p>
        <div class="center-all">
            <section class="form-box">
                <form>
                    <div class="form-group">
                        <input type="text" class="input-style" placeholder="nombre">
                    </div>
                    <div class="form-group">
                        <input type="text" class="input-style" placeholder="e-mail">
                    </div>
                    <div class="form-group">
                        <button class="btn btn-primary no-borderR" class="uppercase">Inscribete</button>
                    </div>
                </form>
            </section>
        </div>
    </div>
</section>
<?php get_footer(); ?>
</body>
</html>
