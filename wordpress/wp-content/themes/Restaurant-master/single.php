<?php get_header(); get_template_part( 'template-parts/navigation/navigation-top', null );
?>
<div id="content" class="bg-light">

    <!-- Post / Item -->
    <article class="post single">
        <nav class="post-nav" >
            <ul class="nav-icons">
                <li><a href="#"><i class="ti-angle-left"></i></a></li>
                <li><a href="<?php echo get_bloginfo("url").'/blog/'; ?>">Volver a Notas</a></li>
                <li><a href="#"><i class="ti-angle-right"></i></a></li>
            </ul>
        </nav>

        <div class="post-image">
            <?php if (has_post_thumbnail( get_the_ID() ) ): ?>
                <?php $image = wp_get_attachment_image_src( get_post_thumbnail_id( get_the_ID() )); 
                endif; ?>
                <img src="<?php echo ($image[0]?$image[0]:get_template_directory_uri().'/static/assets/img/default.jpg') ?>" alt=""></div>
                
                
                <div class="post-content">
                   <?php get_template_part( 'template-parts/post/content-front', get_post_format() );?>
                   
               </div>
               <div class="post-comments post-module">
                <?php get_template_part( 'template-parts/comments/comment-box', get_post_format() );?>
            </div>
            
        </article>

    </div>
   <!-- ========== START CONTROL CONTACT POPUP ========== -->
<div class="contact-popup-ctrl">
    <a href="" class="contact-popup-toggle" data-toggle="modal" data-target="#contact-popup">
        <span class="typing" data-text="[&quot;¿Tienes una Pregunta?&quot;,&quot;¡Escribenos!&quot;]">¡Escribenos!</span><span class="typed-cursor"></span>
    </a>
</div>
<!-- ========== END CONTROL CONTACT POPUP ========== -->

<!-- ========== START MODAL CONTACT POPUP ========== -->
<div id="contact-popup" class="modal fade">
    <div class="modal-dialog" style="font-family: Poppins, Helvetica, Arial, sans-serif;">
        <div class="modal-content" style="border-radius: 8px; height: auto;">
            <div class="modal-header" style="font-weight: bold; color: #333; letter-spacing: 2px; background-color: #ffcc00; border-radius: 8px 8px 0px 0px;">
                CONTACTANOS
            </div>
            <div class="modal-body margin-contact" style="padding-bottom: 0px;">
                <?php echo do_shortcode('[contact-form-7 id="70" title="Formulario de contacto 1"]')?>
            </div>
        </div>
    </div>
</div>
<!-- ========== END MODAL CONTACT POPUP ========== -->
    

    <!-- Ajax Modal -->
    <div id="ajax-modal"></div>
    <!-- Ajax Close -->
    <a href="#" class="ajax-close" data-dismiss="ajax-modal"><i class="ti-close"></i></a>
    <!-- Ajax Loader -->
    <svg id="ajax-loader" class="loader" width="32px" height="32px" viewBox="0 0 66 66" xmlns="http://www.w3.org/2000/svg"><circle class="circle" fill="none" stroke-width="5" stroke-linecap="round" cx="32" cy="32" r="32"></circle></svg>

    <!-- JS Libraries -->
    <?php get_footer();?>


