<?php /* Template Name: Team */ ?>
<?php get_header(); ?>
<?php get_template_part( 'template-parts/navigation/navigation-top', null ); ?>

<section id="team" class="container-fluid unfilled">
	<div class="row sections-row unfilled">
		<div class="col-xs-12 unfilled bg-team">
			<img class="img-responsive" src="<?php echo get_template_directory_uri();?>/assets/images/team/principal-img.jpg">
		</div>
	</div>
	<div class="row sections-row unfilled">
		<div class="col-xs-12 unfilled">
			<h2 class="sections-title">Nuestro Equipo</h2>
		</div>
		<div class="col-md-offset-1 col-sm-offset-1 0 col-md-10 col-sm-10 col-xs-12">
			<p class="sections-subtitle">
				En <strong style="font-family: Helvetica-Bold;">TUCASA<span style="color: #ff6600;">OK</span></strong> creemos en la libertad de las personas, en el compromiso y en el profesionalismo de cada uno de nuestros integrantes. Cuidamos nuestro equipo y velamos por la felicidad de las personas, trabajando de manera autónoma, con confianza, orgullo y compromiso.
			</p>
		</div>
	</div>

	<!-- #################### P·R·O·F·I·L·E - F·O·U·N·D·E·R·S #################### -->
	
	<div class="row sections-row">
		<div class="margin-sm"></div> <!-- margin for a viewport width minimum 768px -->
		<div class="margin-md"></div> <!-- margin for a viewport width minimum 992px -->
		<div class="all-team col-lg-offset-0 col-md-offset-0 col-sm-offset-0 col-xs-offset-1 col-lg-3 col-md-3 col-sm-4 col-xs-10">
			<div>
				<img class="img-circle img-clip img-responsive" src="<?php echo get_template_directory_uri();?>/assets/images/team/paulina_salas.jpg"/>
			</div>
			<h3 class="name fm">Paulina Salas</h3>
			<h4 class="charge fm">Socia Fundadora, Arquitecto</h4>
			<p class="description fm">
				Establezco las pautas de crecimiento en Chile y Latinoamérica. Mi misión es posicionar a <strong>TUCASA<span style="color: #ff6600;">OK</span></strong> en el mercado, manejando las comunicaciones y marketing.
			</p>
		</div>
		<div class="all-team col-lg-offset-1 col-md-offset-1 col-sm-offset-1 col-xs-offset-1 col-lg-3 col-md-3 col-sm-4 col-xs-10">
			<div>
				<img class="img-circle img-clip img-responsive" src="<?php echo get_template_directory_uri();?>/assets/images/team/manuela_maturana.jpg"/>
			</div>
			<h3 class="name fm">Manuela Maturana</h3>
			<h4 class="charge fm">Socia Fundadora, Arquitecto</h4>
			<p class="description fm">
				Dirijo las estrategias de la empresa para que alcance sus objetivos. Mi misión es implementar tecnologías que automaticen y hagan más eficientes nuestros sistemas.
			</p>
		</div>
	</div>
	
	<!-- #################### P·R·O·F·I·L·E - T·E·A·M #################### -->

	<div class="row sections-row unfilled">
		<div class="margin-sm"></div>
		<div class="all-team col-md-offset-1 col-sm-offset-0 col-xs-offset-1 col-lg-2 col-md-2 col-sm-4 col-xs-10">
			<div>
				<img class="img-responsive" src="<?php echo get_template_directory_uri();?>/assets/images/team/carolina_guzman.jpg"/>
			</div>
			<h3 class="name rm">Carolina Guzmán</h3>
			<h4 class="charge rm">Construcción Civil</h4>
			<p class="description rm">
				Mi misión es tener al mejor equipo de inspectores y velar por la calidad de los informes emitidos. Fomentar el aprendizaje y apoyar al equipo. Guio, remuevo obstáculos y proveo de recursos.
			</p>
		</div>
		<div class="all-team col-md-offset-0 col-sm-offset-1 col-xs-offset-1 col-lg-2 col-md-2 col-sm-4 col-xs-10">
			<div>
				<img class="img-responsive" src="<?php echo get_template_directory_uri();?>/assets/images/team/catalina_salas.jpg"/>
			</div>
			<h3 class="name rm">Catalina Salas</h3>
			<h4 class="charge rm">Ingeniero Comercial</h4>
			<p class="description rm">
				Mi misión es alcanzar las metas de venta, además de manejar las relaciones comerciales entre las empresas y conseguir los mejores beneficios para ti.
			</p>
			<br class="m-filling"> <!-- filling to fix the height of the column -->
			<br class="m-filling"> <!-- filling to fix the height of the column -->
			<br class="m-filling"> <!-- filling to fix the height of the column -->
		</div>
		<div class="margin-sm"></div>
		<div class="all-team col-md-offset-0 col-sm-offset-0 col-xs-offset-1 col-lg-2 col-md-2 col-sm-4 col-xs-10">
			<div>
				<img class="img-responsive" src="<?php echo get_template_directory_uri();?>/assets/images/team/carolina_salas.jpg"/>
			</div>
			<h3 class="name rm">Carolina Salas</h3>
			<h4 class="charge rm">Diseño Gráfico</h4>
			<p class="description rm">
				Mi misión es ser el vínculo entre <strong>TUCASA<span style="color: #ff6600;">OK</span></strong> y el cliente, entregando información clara y de calidad. Coordino las inspecciones, asegurándome de que sean atendidas por arquitectos o constructores aptos para cada tarea.
			</p>
		</div>
		<div class="all-team col-md-offset-0 col-sm-offset-1 col-xs-offset-1 col-lg-2 col-md-2 col-sm-4 col-xs-10">
			<div>
				<img class="img-responsive" src="<?php echo get_template_directory_uri();?>/assets/images/team/cynthia_castillo.jpg"/>
			</div>
			<h3 class="name rm">Cynthia Castillo</h3>
			<h4 class="charge rm">Constructor</h4>
			<p class="description rm">
				Mi misión es procurar que las inspecciones de empresa a empresa sean realizadas de manera homogénea, identificar fallas y controlar a los inspectores.
			</p>
			<br class="m-filling"> <!-- filling to fix the height of the column -->
			<br class="m-filling"> <!-- filling to fix the height of the column -->
			<br class="m-filling"> <!-- filling to fix the height of the column -->
		</div>
		<div class="margin-sm"></div> <!-- margin for a viewport width minimum 768px -->
		<div class="all-team col-md-offset-0 col-sm-offset-0 col-xs-offset-1 col-lg-2 col-md-2 col-sm-4 col-xs-10">
			<div>
				<img class="img-responsive" src="<?php echo get_template_directory_uri();?>/assets/images/team/maria_concha.jpg"/>
			</div>
			<h3 class="name rm">María Concha</h3>
			<h4 class="charge rm">Constructor</h4>
			<p class="description rm">
				Mi misión es tener el mejor equipo de freelance, entrenarlos y enseñarles para que realicen las inspecciones de acuerdo a los estándares de la empresa.
			</p>
		</div>
	</div>
</section>

<?php get_footer(); ?>
</body>
</html>