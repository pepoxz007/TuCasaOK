<div class="portfolio-content" style="overflow-y: auto;">
	<div class="container-fluid p-0">

		<article class="post single-modal">      

			<div class="close-modal"><button class="ti-close" style="color: white;" ng-click="mv.closePortfolio()"></button></div>
			<div class="post-image" style="background-image: url('{{mv.active_post.image}}') "></div>
			<div class="post-content">
				<div class="contenido">
					<h1 ng-bind="mv.active_post.post_title" style="text-align: center;"></h1>
					<p align="justify" ng-bind-html="mv.skipValidation(mv.active_post.post_content)"></p>
				</div>
				<hr>
				<div class="text-center">
					<a mp-mode="dftl" ng-click="mv.closePortfolio()" href="https://www.mercadopago.com/mla/checkout/start?pref_id=91605271-4ddc2f27-a0c1-4814-ab9b-8064c5dd6e11" name="MP-payButton" class='blue-ar-l-rn-none'>Pagar</a>

					<script type="text/javascript">

						(function(){function $MPC_load(){window.$MPC_loaded !== true && (function(){var s = document.createElement("script");s.type = "text/javascript";s.async = true;s.src = document.location.protocol+"//secure.mlstatic.com/mptools/render.js";var x = document.getElementsByTagName('script')[0];x.parentNode.insertBefore(s, x);window.$MPC_loaded = true;})();}window.$MPC_loaded !== true ? (window.attachEvent ?window.attachEvent('onload', $MPC_load) : window.addEventListener('load', $MPC_load, false)) : null;})();

					</script>
				</div>
			</div>
		</article>
		
	</div>         
</div>