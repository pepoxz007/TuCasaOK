 
<h1 align="center">
    <a href="" title="<?php the_title(); ?>"><?php the_title(); ?></a>
</h1> 

<p>
<?php echo get_post_field('post_content'); ?>
</p>

<ul class="post-meta">
    <li align="center"><?php echo get_the_date(); ?></li>
</ul>
