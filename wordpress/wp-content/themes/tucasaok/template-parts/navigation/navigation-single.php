<?php
/**
 * Displays top navigation
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.2
 */
get_header();
?>



           <header id="header" class="header-horizontal transparent ">

    <div class="row">
        <div class="col-md-2">
            <!-- Logo -->
            <a href="" class="logo">
                <img width="200"  style="padding-bottom: 65%;" class="qodef-normal-logo" src="<?php echo get_template_directory_uri()?>/assets/img/FINDE.png" alt="logo">
                 <span><img width="200"  style="margin: 10%;" class="qodef-normal-logo" src="<?php echo get_template_directory_uri()?>/assets/img/FINDE.png" alt="logo"></span>
            </a>
        </div>
        <div class="col-md-8">
            <!-- Navigation -->
            <nav id="main-menu">
                <ul class="nav nav-horizontal">
                    <li><a href="#start" class="">Inicio</a></li>
                    <li><a href="#portfolio" class="">Cursos y Workshops</a></li>
                    <li><a href="#clients" class="">Instructores</a></li>
                    <li><a href="#latest-posts" class="">Notas</a></li>
                    <li><a href="#contact" class="">Contáctanos</a></li>
                </ul>
                <span class="selector" style="width: 70px; left: 263.766px;"></span>
            </nav>
        </div>
    </div>
    <!-- Mobile Nav Toggle -->
    <a href="#" id="horizontal-nav-toggle" class="nav-toggle" data-toggle="mobile-nav"><span><span></span></span></a>

</header>
