<?php
/**
 * Displays top navigation
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.2
 */

$baseUrl=get_site_url();
$type=is_front_page()?1:0;
$url=(!$type?$baseUrl.'/':'');
$activeClass=(!$type?'active':'');
?>

<header id="header" class="header-horizontal transparent" style="background: rgb(127, 127, 127);">

    <div class="row coolMenu">
        <!-- Logo -->
        <a href="" class="logo">
            <img width="300" class="qodef-normal-logo" src="<?php echo get_template_directory_uri()?>/assets/build/img/FINDE.png" alt="logo">
            <img width="300" class="qodef-normal-logo-mobile" src="<?php echo get_template_directory_uri()?>/assets/build/img/finderoomwhite.png" alt="logo">
        </a>
        <!-- Navigation -->
        <nav id="main-menu">
            <ul class="nav nav-horizontal">
                <li><a href="<?php echo $url; ?>#start" class="">Inicio</a></li>
                <li><a href="<?php echo $url; ?>#portfolio" class="">Cursos y Workshops</a></li>
                <li><a href="<?php echo $url; ?>#latest-posts" class="<?php echo $activeClass; ?>">Notas</a></li>
                <li><a href="<?php echo $url; ?>#clients" class="">Fundadores</a></li>
                <li><a href="<?php echo $baseUrl.'/contact'; ?>" class="">Contáctanos</a></li>
            </ul>
            <span class="selector"></span>
        </nav>
    </div>

    <!-- Mobile Nav Toggle -->
    <a href="#" id="horizontal-nav-toggle" class="nav-toggle" data-toggle="mobile-nav" ><span><span></span></span></a>

</header>
<div id="contact-popup" class="modal fade">
    <div class="modal-dialog" style="font-family: Poppins, Helvetica, Arial, sans-serif;">
        <div class="modal-content" style="border-radius: 8px; height: auto;">
            <div class="modal-header" style="font-weight: bold; color: #333; letter-spacing: 2px; background-color: #ffcc00; border-radius: 8px 8px 0px 0px;">CONTACTANOS
            </div>
            <div class="modal-body margin-contact" style="padding-bottom: 0px;">
                <?php echo do_shortcode('[contact-form-7 id="70" title="Formulario de contacto 1"]')?>
            </div>
        </div>
    </div>
</div>
