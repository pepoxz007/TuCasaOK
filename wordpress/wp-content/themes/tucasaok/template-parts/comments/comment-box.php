
	
<div class="title">
	<h4>Comentarios<span class="badge bg-primary" align="center"></span></h4>
</div>
<br>
 <div class="content">
 	<ul class="comments">

		<?php 
		if ( comments_open(get_the_ID()) || get_comments_number(get_the_ID()) ) :
			comments_template();
		endif;

		?>
	<!-- #respond -->
	</ul>
	</div>
