<?php get_header(); ?>

<div>
    <style type="text/css" data-styleid="theme_fonts">
        .font_0 {
            font: normal normal normal 40px/1.4em spinnaker, sans-serif;
            color: #FFFFFF;
        }
        
        .font_1 {
            font: normal normal normal 14px/1.4em spinnaker, sans-serif;
            color: #616161;
        }
        
        .font_2 {
            font: normal normal normal 18px/1.4em spinnaker, sans-serif;
            color: #616161;
        }
        
        .font_3 {
            font: normal normal normal 60px/1.4em anton, sans-serif;
            color: #303030;
        }
        
        .font_4 {
            font: normal normal normal 40px/1.4em anton, sans-serif;
            color: #303030;
        }
        
        .font_5 {
            font: normal normal normal 25px/1.4em anton, sans-serif;
            color: #303030;
        }
        
        .font_6 {
            font: normal normal normal 22px/1.4em spinnaker, sans-serif;
            color: #616161;
        }
        
        .font_7 {
            font: normal normal normal 16px/1.4em spinnaker, sans-serif;
            color: #616161;
        }
        
        .font_8 {
            font: normal normal normal 14px/1.4em spinnaker, sans-serif;
            color: #616161;
        }
        
        .font_9 {
            font: normal normal normal 13px/1.4em spinnaker, sans-serif;
            color: #616161;
        }
        
        .font_10 {
            font: normal normal normal 12px/1.4em spinnaker, sans-serif;
            color: #616161;
        }
    </style>
    <style type="text/css" data-styleid="theme_colors">
        .color_0 {
            color: #FFFFFF;
        }
        
        .backcolor_0 {
            background-color: #FFFFFF;
        }
        
        .color_1 {
            color: #FFFFFF;
        }
        
        .backcolor_1 {
            background-color: #FFFFFF;
        }
        
        .color_2 {
            color: #000000;
        }
        
        .backcolor_2 {
            background-color: #000000;
        }
        
        .color_3 {
            color: #ED1C24;
        }
        
        .backcolor_3 {
            background-color: #ED1C24;
        }
        
        .color_4 {
            color: #0088CB;
        }
        
        .backcolor_4 {
            background-color: #0088CB;
        }
        
        .color_5 {
            color: #FFCB05;
        }
        
        .backcolor_5 {
            background-color: #FFCB05;
        }
        
        .color_6 {
            color: #727272;
        }
        
        .backcolor_6 {
            background-color: #727272;
        }
        
        .color_7 {
            color: #B0B0B0;
        }
        
        .backcolor_7 {
            background-color: #B0B0B0;
        }
        
        .color_8 {
            color: #FFFFFF;
        }
        
        .backcolor_8 {
            background-color: #FFFFFF;
        }
        
        .color_9 {
            color: #727272;
        }
        
        .backcolor_9 {
            background-color: #727272;
        }
        
        .color_10 {
            color: #B0B0B0;
        }
        
        .backcolor_10 {
            background-color: #B0B0B0;
        }
        
        .color_11 {
            color: #FFFFFF;
        }
        
        .backcolor_11 {
            background-color: #FFFFFF;
        }
        
        .color_12 {
            color: #CCCCCC;
        }
        
        .backcolor_12 {
            background-color: #CCCCCC;
        }
        
        .color_13 {
            color: #919191;
        }
        
        .backcolor_13 {
            background-color: #919191;
        }
        
        .color_14 {
            color: #616161;
        }
        
        .backcolor_14 {
            background-color: #616161;
        }
        
        .color_15 {
            color: #303030;
        }
        
        .backcolor_15 {
            background-color: #303030;
        }
        
        .color_16 {
            color: #FDD0C1;
        }
        
        .backcolor_16 {
            background-color: #FDD0C1;
        }
        
        .color_17 {
            color: #FCB9A2;
        }
        
        .backcolor_17 {
            background-color: #FCB9A2;
        }
        
        .color_18 {
            color: #FA7548;
        }
        
        .backcolor_18 {
            background-color: #FA7548;
        }
        
        .color_19 {
            color: #A74E30;
        }
        
        .backcolor_19 {
            background-color: #A74E30;
        }
        
        .color_20 {
            color: #532718;
        }
        
        .backcolor_20 {
            background-color: #532718;
        }
        
        .color_21 {
            color: #F2F35B;
        }
        
        .backcolor_21 {
            background-color: #F2F35B;
        }
        
        .color_22 {
            color: #E6E72E;
        }
        
        .backcolor_22 {
            background-color: #E6E72E;
        }
        
        .color_23 {
            color: #DBDB00;
        }
        
        .backcolor_23 {
            background-color: #DBDB00;
        }
        
        .color_24 {
            color: #929200;
        }
        
        .backcolor_24 {
            background-color: #929200;
        }
        
        .color_25 {
            color: #494900;
        }
        
        .backcolor_25 {
            background-color: #494900;
        }
        
        .color_26 {
            color: #ACF1E7;
        }
        
        .backcolor_26 {
            background-color: #ACF1E7;
        }
        
        .color_27 {
            color: #82E4D5;
        }
        
        .backcolor_27 {
            background-color: #82E4D5;
        }
        
        .color_28 {
            color: #1ED6BB;
        }
        
        .backcolor_28 {
            background-color: #1ED6BB;
        }
        
        .color_29 {
            color: #148F7C;
        }
        
        .backcolor_29 {
            background-color: #148F7C;
        }
        
        .color_30 {
            color: #0A473E;
        }
        
        .backcolor_30 {
            background-color: #0A473E;
        }
        
        .color_31 {
            color: #DCC4F7;
        }
        
        .backcolor_31 {
            background-color: #DCC4F7;
        }
        
        .color_32 {
            color: #C7A5EE;
        }
        
        .backcolor_32 {
            background-color: #C7A5EE;
        }
        
        .color_33 {
            color: #9B5AE6;
        }
        
        .backcolor_33 {
            background-color: #9B5AE6;
        }
        
        .color_34 {
            color: #673C99;
        }
        
        .backcolor_34 {
            background-color: #673C99;
        }
        
        .color_35 {
            color: #341E4D;
        }
        
        .backcolor_35 {
            background-color: #341E4D;
        }
    </style>
    <style type="text/css" data-styleid="style-iuoiszjc">
        .style-iuoiszjcscreenWidthBackground {
            position: absolute;
            top: 0;
            right: 0;
            bottom: 0;
            left: 0;
        }
        
        .style-iuoiszjc[data-state~="fixedPosition"] {
            position: fixed !important;
            left: auto !important;
            z-index: 50;
        }
        
        .style-iuoiszjc[data-state~="fixedPosition"].style-iuoiszjc_footer {
            top: auto;
            bottom: 0;
        }
        
        .style-iuoiszjc_bg {
            position: absolute;
            top: 0;
            right: 0;
            bottom: 0;
            left: 0;
            background-color: rgba(255, 255, 255, 1);
            border-top: 0px solid rgba(255, 255, 255, 1);
            border-bottom: 0px solid rgba(255, 255, 255, 1);
        }
        
        .style-iuoiszjc[data-state~="mobileView"] .style-iuoiszjcbg {
            left: 10px;
            right: 10px;
        }
        
        .style-iuoiszjcbg {
            position: absolute;
            top: 0px;
            right: 0;
            bottom: 0px;
            left: 0;
            background-color: rgba(255, 255, 255, 1);
            border-radius: 0;
        }
        
        .style-iuoiszjcinlineContent {
            position: absolute;
            top: 0;
            right: 0;
            bottom: 0;
            left: 0;
        }
        
        .style-iuoiszjccenteredContent {
            position: absolute;
            top: 0;
            right: 0;
            bottom: 0;
            left: 0;
        }
    </style>
    <style type="text/css" data-styleid="style-iurapags">
        .style-iurapagsitemsContainer {
            width: calc(100% - 0px);
            height: calc(100% - 0px);
            white-space: nowrap;
            display: inline-block;
            overflow: visible;
            position: relative;
        }
        
        .style-iurapagsmoreContainer {
            overflow: visible;
            display: inherit;
            white-space: nowrap;
            width: auto;
            background-color: rgba(255, 255, 255, 1);
            border-radius: 0;
        }
        
        .style-iurapagsdropWrapper {
            z-index: 99999;
            display: block;
            opacity: 1;
            visibility: hidden;
            position: absolute;
            margin-top: 7px;
        }
        
        .style-iurapagsdropWrapper[data-dropMode="dropUp"] {
            margin-top: 0;
            margin-bottom: 7px;
        }
        
        .style-iurapagsrepeaterButton {
            height: 100%;
            position: relative;
            box-sizing: border-box;
            display: inline-block;
            cursor: pointer;
            font: normal normal normal 11px/1.4em wfont_9012a2_eab04d8add6b4f4eb1026227d1089542, wf_eab04d8add6b4f4eb1026227d, orig_montserratlight;
        }
        
        .style-iurapagsrepeaterButton[data-state~="header"] a,
        .style-iurapagsrepeaterButton[data-state~="header"] div {
            cursor: default !important;
        }
        
        .style-iurapagsrepeaterButtonlinkElement {
            display: inline-block;
            height: 100%;
            width: 100%;
        }
        
        .style-iurapagsrepeaterButton_gapper {
            padding: 0 5px;
        }
        
        .style-iurapagsrepeaterButtonlabel {
            display: inline-block;
            padding: 0 10px;
            color: #616161;
            transition: color 0.4s ease 0s;
        }
        
        .style-iurapagsrepeaterButton[data-state~="drop"] {
            width: 100%;
            display: block;
        }
        
        .style-iurapagsrepeaterButton[data-state~="drop"] .style-iurapagsrepeaterButtonlabel {
            padding: 0 .5em;
        }
        
        .style-iurapagsrepeaterButton[data-state~="over"] .style-iurapagsrepeaterButtonlabel {
            color: #FF6600;
            transition: color 0.4s ease 0s;
        }
        
        .style-iurapagsrepeaterButton[data-state~="selected"] .style-iurapagsrepeaterButtonlabel {
            color: #616161;
            transition: color 0.4s ease 0s;
        }
    </style>
    <style type="text/css" data-styleid="wp2">
        .wp2_zoomedin {
            cursor: url(https://static.parastorage.com/services/skins/2.1229.79/images/wysiwyg/core/themes/base/cursor_zoom_out.png), url(https://static.parastorage.com/services/skins/2.1229.79/images/wysiwyg/core/themes/base/cursor_zoom_out.cur), auto;
            overflow: hidden;
            display: block;
        }
        
        .wp2_zoomedout {
            cursor: url(https://static.parastorage.com/services/skins/2.1229.79/images/wysiwyg/core/themes/base/cursor_zoom_in.png), url(https://static.parastorage.com/services/skins/2.1229.79/images/wysiwyg/core/themes/base/cursor_zoom_in.cur), auto;
        }
        
        .wp2link {
            display: block;
            overflow: hidden;
        }
        
        .wp2img {
            overflow: hidden;
        }
        
        .wp2imgimage {
            position: static;
            box-shadow: #000 0 0 0;
            -webkit-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            user-select: none;
        }
    </style>
    <style type="text/css" data-styleid="style-iuonybvz">
        .style-iuonybvzscreenWidthBackground {
            position: absolute;
            top: 0;
            right: 0;
            bottom: 0;
            left: 0;
        }
        
        .style-iuonybvz[data-state~="fixedPosition"] {
            position: fixed !important;
            left: auto !important;
            z-index: 50;
        }
        
        .style-iuonybvz[data-state~="fixedPosition"].style-iuonybvz_footer {
            top: auto;
            bottom: 0;
        }
        
        .style-iuonybvz_bg {
            position: absolute;
            top: 0;
            right: 0;
            bottom: 0;
            left: 0;
            background-color: rgba(255, 102, 0, 1);
            border-top: 0px solid rgba(97, 97, 97, 1);
            border-bottom: 0px solid rgba(97, 97, 97, 1);
        }
        
        .style-iuonybvz[data-state~="mobileView"] .style-iuonybvzbg {
            left: 10px;
            right: 10px;
        }
        
        .style-iuonybvzbg {
            position: absolute;
            top: 0px;
            right: 0;
            bottom: 0px;
            left: 0;
            background-color: rgba(255, 102, 0, 1);
            border-radius: 0;
        }
        
        .style-iuonybvzinlineContent {
            position: absolute;
            top: 0;
            right: 0;
            bottom: 0;
            left: 0;
        }
        
        .style-iuonybvzcenteredContent {
            position: absolute;
            top: 0;
            right: 0;
            bottom: 0;
            left: 0;
        }
    </style>
    <style type="text/css" data-styleid="ca1">
        .ca1_zoomedin {
            cursor: url(https://static.parastorage.com/services/skins/2.1229.79/images/wysiwyg/core/themes/base/cursor_zoom_out.png), url(https://static.parastorage.com/services/skins/2.1229.79/images/wysiwyg/core/themes/base/cursor_zoom_out.cur), auto;
            overflow: hidden;
            display: block;
        }
        
        .ca1_zoomedout {
            cursor: url(https://static.parastorage.com/services/skins/2.1229.79/images/wysiwyg/core/themes/base/cursor_zoom_in.png), url(https://static.parastorage.com/services/skins/2.1229.79/images/wysiwyg/core/themes/base/cursor_zoom_in.cur), auto;
        }
        
        .ca1link {
            display: block;
            overflow: hidden;
        }
        
        .ca1img {
            overflow: hidden;
        }
        
        .ca1imgimage {
            position: static;
            box-shadow: #000 0 0 0;
            -webkit-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            user-select: none;
        }
    </style>
    <style type="text/css" data-styleid="txtNew">
        .txtNew {
            word-wrap: break-word;
        }
        
        .txtNew_override-left * {
            text-align: left !important;
        }
        
        .txtNew_override-right * {
            text-align: right !important;
        }
        
        .txtNew_override-center * {
            text-align: center !important;
        }
        
        .txtNew_override-justify * {
            text-align: justify !important;
        }
        
        .txtNew li {
            font-style: inherit;
            font-weight: inherit;
            line-height: inherit;
            letter-spacing: normal;
        }
        
        .txtNew ol,
        .txtNew ul {
            padding-left: 1.3em;
            padding-right: 0;
            margin-left: 0.5em;
            margin-right: 0;
            line-height: normal;
            letter-spacing: normal;
        }
        
        .txtNew ul {
            list-style-type: disc;
        }
        
        .txtNew ol {
            list-style-type: decimal;
        }
        
        .txtNew ul ul,
        .txtNew ol ul {
            list-style-type: circle;
        }
        
        .txtNew ul ul ul,
        .txtNew ol ul ul {
            list-style-type: square;
        }
        
        .txtNew ul ol ul,
        .txtNew ol ol ul {
            list-style-type: square;
        }
        
        .txtNew ul[dir="rtl"],
        .txtNew ol[dir="rtl"] {
            padding-left: 0;
            padding-right: 1.3em;
            margin-left: 0;
            margin-right: 0.5em;
        }
        
        .txtNew ul[dir="rtl"] ul,
        .txtNew ul[dir="rtl"] ol,
        .txtNew ol[dir="rtl"] ul,
        .txtNew ol[dir="rtl"] ol {
            padding-left: 0;
            padding-right: 1.3em;
            margin-left: 0;
            margin-right: 0.5em;
        }
        
        .txtNew p {
            margin: 0;
            line-height: normal;
            letter-spacing: normal;
        }
        
        .txtNew h1 {
            margin: 0;
            line-height: normal;
            letter-spacing: normal;
        }
        
        .txtNew h2 {
            margin: 0;
            line-height: normal;
            letter-spacing: normal;
        }
        
        .txtNew h3 {
            margin: 0;
            line-height: normal;
            letter-spacing: normal;
        }
        
        .txtNew h4 {
            margin: 0;
            line-height: normal;
            letter-spacing: normal;
        }
        
        .txtNew h5 {
            margin: 0;
            line-height: normal;
            letter-spacing: normal;
        }
        
        .txtNew h6 {
            margin: 0;
            line-height: normal;
            letter-spacing: normal;
        }
        
        .txtNew a {
            color: inherit;
        }
    </style>
    <style type="text/css" data-styleid="lb1">
        .lb1itemsContainer {
            position: absolute;
            width: 100%;
            height: 100%;
            white-space: nowrap;
        }
        
        .lb1itemsContainer > li:last-child {
            margin: 0 !important;
        }
        
        .lb1[data-state~="mobileView"] .lb1itemsContainer {
            position: absolute;
            width: 100%;
            height: 100%;
            white-space: normal;
        }
        
        .lb1 a {
            display: block;
            height: 100%;
        }
        
        .lb1imageItemlink {
            cursor: pointer;
        }
        
        .lb1imageItemimageimage {
            position: static;
            box-shadow: #000 0 0 0;
            -webkit-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            user-select: none;
        }
    </style>
    <style type="text/css" data-styleid="style-j7tluq6n">
        .style-j7tluq6n[data-state~="shouldUseFlex"] .style-j7tluq6nlink,
        .style-j7tluq6n[data-state~="shouldUseFlex"] .style-j7tluq6nlabelwrapper {
            text-align: initial;
            display: -webkit-box;
            display: -webkit-flex;
            display: flex;
            -webkit-box-align: center;
            -webkit-align-items: center;
            align-items: center;
        }
        
        .style-j7tluq6n[data-state~="shouldUseFlex"][data-state~="center"] .style-j7tluq6nlink,
        .style-j7tluq6n[data-state~="shouldUseFlex"][data-state~="center"] .style-j7tluq6nlabelwrapper {
            -webkit-box-pack: center;
            -webkit-justify-content: center;
            justify-content: center;
        }
        
        .style-j7tluq6n[data-state~="shouldUseFlex"][data-state~="left"] .style-j7tluq6nlink,
        .style-j7tluq6n[data-state~="shouldUseFlex"][data-state~="left"] .style-j7tluq6nlabelwrapper {
            -webkit-box-pack: start;
            -webkit-justify-content: flex-start;
            justify-content: flex-start;
        }
        
        .style-j7tluq6n[data-state~="shouldUseFlex"][data-state~="right"] .style-j7tluq6nlink,
        .style-j7tluq6n[data-state~="shouldUseFlex"][data-state~="right"] .style-j7tluq6nlabelwrapper {
            -webkit-box-pack: end;
            -webkit-justify-content: flex-end;
            justify-content: flex-end;
        }
        
        .style-j7tluq6n[data-disabled="false"] {
            cursor: pointer;
        }
        
        .style-j7tluq6n[data-disabled="false"]:active[data-state~="mobile"] .style-j7tluq6nlabel,
        .style-j7tluq6n[data-disabled="false"]:hover[data-state~="desktop"] .style-j7tluq6nlabel,
        .style-j7tluq6n[data-disabled="false"][data-preview~="hover"] .style-j7tluq6nlabel {
            color: #E6E72E;
            transition: color 0.4s ease 0s;
        }
        
        .style-j7tluq6nlink {
            position: absolute;
            top: 0;
            right: 0;
            bottom: 0;
            left: 0;
        }
        
        .style-j7tluq6nlabel {
            font: normal normal normal 13px/1.4em wfont_9012a2_cf30a994f829458daa5b46a9a39e7eac, wf_cf30a994f829458daa5b46a9a, orig_cooperhewittbook;
            transition: color 0.4s ease 0s;
            color: #FFFFFF;
            white-space: nowrap;
            display: inline-block;
        }
        
        .style-j7tluq6n[data-disabled="true"] .style-j7tluq6nlabel,
        .style-j7tluq6n[data-preview~="disabled"] .style-j7tluq6nlabel {
            color: #FFFFFF;
        }
    </style>
    <style type="text/css" data-styleid="style-j7tm467w">
        .style-j7tm467w[data-state~="shouldUseFlex"] .style-j7tm467wlink,
        .style-j7tm467w[data-state~="shouldUseFlex"] .style-j7tm467wlabelwrapper {
            text-align: initial;
            display: -webkit-box;
            display: -webkit-flex;
            display: flex;
            -webkit-box-align: center;
            -webkit-align-items: center;
            align-items: center;
        }
        
        .style-j7tm467w[data-state~="shouldUseFlex"][data-state~="center"] .style-j7tm467wlink,
        .style-j7tm467w[data-state~="shouldUseFlex"][data-state~="center"] .style-j7tm467wlabelwrapper {
            -webkit-box-pack: center;
            -webkit-justify-content: center;
            justify-content: center;
        }
        
        .style-j7tm467w[data-state~="shouldUseFlex"][data-state~="left"] .style-j7tm467wlink,
        .style-j7tm467w[data-state~="shouldUseFlex"][data-state~="left"] .style-j7tm467wlabelwrapper {
            -webkit-box-pack: start;
            -webkit-justify-content: flex-start;
            justify-content: flex-start;
        }
        
        .style-j7tm467w[data-state~="shouldUseFlex"][data-state~="right"] .style-j7tm467wlink,
        .style-j7tm467w[data-state~="shouldUseFlex"][data-state~="right"] .style-j7tm467wlabelwrapper {
            -webkit-box-pack: end;
            -webkit-justify-content: flex-end;
            justify-content: flex-end;
        }
        
        .style-j7tm467w[data-disabled="false"] {
            cursor: pointer;
        }
        
        .style-j7tm467w[data-disabled="false"]:active[data-state~="mobile"] .style-j7tm467wlabel,
        .style-j7tm467w[data-disabled="false"]:hover[data-state~="desktop"] .style-j7tm467wlabel,
        .style-j7tm467w[data-disabled="false"][data-preview~="hover"] .style-j7tm467wlabel {
            color: #E6E72E;
            transition: color 0.4s ease 0s;
        }
        
        .style-j7tm467wlink {
            position: absolute;
            top: 0;
            right: 0;
            bottom: 0;
            left: 0;
        }
        
        .style-j7tm467wlabel {
            font: normal normal normal 13px/1.4em wfont_9012a2_cf30a994f829458daa5b46a9a39e7eac, wf_cf30a994f829458daa5b46a9a, orig_cooperhewittbook;
            transition: color 0.4s ease 0s;
            color: #FFFFFF;
            white-space: nowrap;
            display: inline-block;
        }
        
        .style-j7tm467w[data-disabled="true"] .style-j7tm467wlabel,
        .style-j7tm467w[data-preview~="disabled"] .style-j7tm467wlabel {
            color: #FFFFFF;
        }
    </style>
    <style type="text/css" data-styleid="style-j7v07ua7">
        .style-j7v07ua7[data-state~="shouldUseFlex"] .style-j7v07ua7link,
        .style-j7v07ua7[data-state~="shouldUseFlex"] .style-j7v07ua7labelwrapper {
            text-align: initial;
            display: -webkit-box;
            display: -webkit-flex;
            display: flex;
            -webkit-box-align: center;
            -webkit-align-items: center;
            align-items: center;
        }
        
        .style-j7v07ua7[data-state~="shouldUseFlex"][data-state~="center"] .style-j7v07ua7link,
        .style-j7v07ua7[data-state~="shouldUseFlex"][data-state~="center"] .style-j7v07ua7labelwrapper {
            -webkit-box-pack: center;
            -webkit-justify-content: center;
            justify-content: center;
        }
        
        .style-j7v07ua7[data-state~="shouldUseFlex"][data-state~="left"] .style-j7v07ua7link,
        .style-j7v07ua7[data-state~="shouldUseFlex"][data-state~="left"] .style-j7v07ua7labelwrapper {
            -webkit-box-pack: start;
            -webkit-justify-content: flex-start;
            justify-content: flex-start;
        }
        
        .style-j7v07ua7[data-state~="shouldUseFlex"][data-state~="right"] .style-j7v07ua7link,
        .style-j7v07ua7[data-state~="shouldUseFlex"][data-state~="right"] .style-j7v07ua7labelwrapper {
            -webkit-box-pack: end;
            -webkit-justify-content: flex-end;
            justify-content: flex-end;
        }
        
        .style-j7v07ua7[data-disabled="false"] {
            cursor: pointer;
        }
        
        .style-j7v07ua7[data-disabled="false"]:active[data-state~="mobile"] .style-j7v07ua7label,
        .style-j7v07ua7[data-disabled="false"]:hover[data-state~="desktop"] .style-j7v07ua7label,
        .style-j7v07ua7[data-disabled="false"][data-preview~="hover"] .style-j7v07ua7label {
            color: #E6E72E;
            transition: color 0.4s ease 0s;
        }
        
        .style-j7v07ua7link {
            position: absolute;
            top: 0;
            right: 0;
            bottom: 0;
            left: 0;
        }
        
        .style-j7v07ua7label {
            font: normal normal normal 13px/1.4em wfont_9012a2_cf30a994f829458daa5b46a9a39e7eac, wf_cf30a994f829458daa5b46a9a, orig_cooperhewittbook;
            transition: color 0.4s ease 0s;
            color: #FFFFFF;
            white-space: nowrap;
            display: inline-block;
        }
        
        .style-j7v07ua7[data-disabled="true"] .style-j7v07ua7label,
        .style-j7v07ua7[data-preview~="disabled"] .style-j7v07ua7label {
            color: #FFFFFF;
        }
    </style>
    <style type="text/css" data-styleid="pc1">
        .pc1screenWidthBackground {
            position: absolute;
            top: 0;
            right: 0;
            bottom: 0;
            left: 0;
        }
        
        .pc1[data-state~="fixedPosition"] {
            position: fixed !important;
            left: auto !important;
            z-index: 50;
        }
        
        .pc1[data-state~="fixedPosition"].pc1_footer {
            top: auto;
            bottom: 0;
        }
        
        .pc1bg {
            position: absolute;
            top: 0;
            right: 0;
            bottom: 0;
            left: 0;
        }
        
        .pc1inlineContent {
            position: absolute;
            top: 0;
            right: 0;
            bottom: 0;
            left: 0;
        }
        
        .pc1centeredContent {
            position: absolute;
            top: 0;
            right: 0;
            bottom: 0;
            left: 0;
        }
    </style>
    <style type="text/css" data-styleid="s_VOwPageGroupSkin">
        .s_VOwPageGroupSkin {
            height: 100px;
            width: 100px;
        }
        
        .s_VOwPageGroupSkinoverlay {
            position: absolute;
            top: 0;
            right: 0;
            bottom: 0;
            left: 0;
            background-color: rgba(0, 0, 0, 0.664);
        }
        
        .s_VOwPageGroupSkininlineContent {
            position: absolute;
            top: 0;
            right: 0;
            bottom: 0;
            left: 0;
        }
    </style>
    <style type="text/css" data-styleid="p1">
        .p1bg {
            position: absolute;
            top: 0;
            right: 0;
            bottom: 0;
            left: 0;
        }
        
        .p1[data-state~="mobileView"] .p1bg {
            left: 10px;
            right: 10px;
        }
        
        .p1inlineContent {
            position: absolute;
            top: 0;
            right: 0;
            bottom: 0;
            left: 0;
        }
    </style>
    <style type="text/css" data-styleid="style-ja78349v">
        .style-ja78349v {
            background-color: transparent;
            box-sizing: border-box !important;
            position: relative;
            min-height: 50px;
        }
        
        .style-ja78349v[data-shouldhideoverflowcontent="true"] .style-ja78349vinlineContent {
            overflow: hidden;
        }
        
        .style-ja78349vnavigationArrows[data-show-navigation-arrows="false"] {
            display: none;
        }
        
        .style-ja78349vdotsMenuWrapper[data-show-navigation-dots="false"] {
            display: none;
        }
        
        .style-ja78349vbg {
            position: absolute;
            top: 0;
            right: 0;
            bottom: 0;
            left: 0;
        }
        
        .style-ja78349vinlineContent {
            position: absolute;
            top: 0;
            right: 0;
            bottom: 0;
            left: 0;
        }
        
        .style-ja78349vinlineContentParent {
            position: absolute;
            width: 100%;
            height: 100%;
        }
        
        .style-ja78349vdotsMenuWrapper {
            display: -webkit-box;
            display: -webkit-flex;
            display: flex;
            -webkit-box-align: center;
            -webkit-align-items: center;
            align-items: center;
            position: absolute;
            width: 100%;
            pointer-events: none;
        }
        
        .style-ja78349v_navigation-arrows {
            position: absolute;
            width: 100%;
        }
        
        .style-ja78349v_navigation-dot {
            pointer-events: auto;
            position: relative;
            display: inline-block;
            cursor: pointer;
            border-radius: 50%;
            background-color: rgba(204, 204, 204, 1);
        }
        
        .style-ja78349v_btn:hover {
            opacity: .6;
        }
        
        .style-ja78349vnextButton {
            -webkit-transform: scale(-1);
            transform: scale(-1);
        }
        
        .style-ja78349v_navigation-dot.style-ja78349v_selected {
            background-color: rgba(255, 255, 255, 0.3);
        }
        
        .style-ja78349v_btn {
            position: absolute;
            cursor: pointer;
        }
        
        .style-ja78349v_btn svg {
            fill: rgba(48, 48, 48, 1);
            stroke: rgba(48, 48, 48, 1);
            stroke-width: 1px;
        }
    </style>
    <style type="text/css" data-styleid="style-j02kihlz">
        .style-j02kihlzborderNode {
            border: 0px solid rgba(167, 78, 48, 1);
            position: absolute;
            top: 0;
            right: 0;
            bottom: 0;
            left: 0;
            pointer-events: none;
        }
        
        .style-j02kihlzinlineContent {
            position: absolute;
            top: 0;
            right: 0;
            bottom: 0;
            left: 0;
        }
        
        .style-j02kihlzinlineContentParent {
            position: absolute;
            width: 100%;
            height: 100%;
        }
        
        .style-j02kihlz[data-shouldhideoverflowcontent="true"] {
            overflow: hidden;
        }
    </style>
    <style type="text/css" data-styleid="style-j02lno1h">
        .style-j02lno1hbg {
            border: 3px solid rgba(255, 255, 255, 1);
            background-color: transparent;
            border-radius: 0;
        }
        
        .style-j02lno1hinlineContent,
        .style-j02lno1hbg {
            position: absolute;
            top: 0;
            right: 0;
            bottom: 0;
            left: 0;
        }
    </style>
    <style type="text/css" data-styleid="style-j94qfmna">
        .style-j94qfmnaborderNode {
            border: 0px solid rgba(167, 78, 48, 1);
            position: absolute;
            top: 0;
            right: 0;
            bottom: 0;
            left: 0;
            pointer-events: none;
        }
        
        .style-j94qfmnainlineContent {
            position: absolute;
            top: 0;
            right: 0;
            bottom: 0;
            left: 0;
        }
        
        .style-j94qfmnainlineContentParent {
            position: absolute;
            width: 100%;
            height: 100%;
        }
        
        .style-j94qfmna[data-shouldhideoverflowcontent="true"] {
            overflow: hidden;
        }
    </style>
    <style type="text/css" data-styleid="style-j02kihmf">
        .style-j02kihmfborderNode {
            border: 0px solid rgba(167, 78, 48, 1);
            position: absolute;
            top: 0;
            right: 0;
            bottom: 0;
            left: 0;
            pointer-events: none;
        }
        
        .style-j02kihmfinlineContent {
            position: absolute;
            top: 0;
            right: 0;
            bottom: 0;
            left: 0;
        }
        
        .style-j02kihmfinlineContentParent {
            position: absolute;
            width: 100%;
            height: 100%;
        }
        
        .style-j02kihmf[data-shouldhideoverflowcontent="true"] {
            overflow: hidden;
        }
    </style>
    <style type="text/css" data-styleid="style-j02lm6i0">
        .style-j02lm6i0bg {
            border: 3px solid rgba(255, 102, 0, 1);
            background-color: transparent;
            border-radius: 0;
        }
        
        .style-j02lm6i0inlineContent,
        .style-j02lm6i0bg {
            position: absolute;
            top: 0;
            right: 0;
            bottom: 0;
            left: 0;
        }
    </style>
    <style type="text/css" data-styleid="style-j02lihjj3">
        .style-j02lihjj3borderNode {
            border: 0px solid rgba(167, 78, 48, 1);
            position: absolute;
            top: 0;
            right: 0;
            bottom: 0;
            left: 0;
            pointer-events: none;
        }
        
        .style-j02lihjj3inlineContent {
            position: absolute;
            top: 0;
            right: 0;
            bottom: 0;
            left: 0;
        }
        
        .style-j02lihjj3inlineContentParent {
            position: absolute;
            width: 100%;
            height: 100%;
        }
        
        .style-j02lihjj3[data-shouldhideoverflowcontent="true"] {
            overflow: hidden;
        }
    </style>
    <style type="text/css" data-styleid="style-j02tb4sc">
        .style-j02tb4scborderNode {
            border: 0px solid rgba(167, 78, 48, 1);
            position: absolute;
            top: 0;
            right: 0;
            bottom: 0;
            left: 0;
            pointer-events: none;
        }
        
        .style-j02tb4scinlineContent {
            position: absolute;
            top: 0;
            right: 0;
            bottom: 0;
            left: 0;
        }
        
        .style-j02tb4scinlineContentParent {
            position: absolute;
            width: 100%;
            height: 100%;
        }
        
        .style-j02tb4sc[data-shouldhideoverflowcontent="true"] {
            overflow: hidden;
        }
    </style>
    <style type="text/css" data-styleid="style-j02tb4sk">
        .style-j02tb4skbg {
            border: 3px solid rgba(255, 102, 0, 1);
            background-color: transparent;
            border-radius: 0;
        }
        
        .style-j02tb4skinlineContent,
        .style-j02tb4skbg {
            position: absolute;
            top: 0;
            right: 0;
            bottom: 0;
            left: 0;
        }
    </style>
    <style type="text/css" data-styleid="style-j34nhe9g">
        .style-j34nhe9gborderNode {
            border: 0px solid rgba(167, 78, 48, 1);
            position: absolute;
            top: 0;
            right: 0;
            bottom: 0;
            left: 0;
            pointer-events: none;
        }
        
        .style-j34nhe9ginlineContent {
            position: absolute;
            top: 0;
            right: 0;
            bottom: 0;
            left: 0;
        }
        
        .style-j34nhe9ginlineContentParent {
            position: absolute;
            width: 100%;
            height: 100%;
        }
        
        .style-j34nhe9g[data-shouldhideoverflowcontent="true"] {
            overflow: hidden;
        }
    </style>
    <style type="text/css" data-styleid="style-ja783u0e">
        .style-ja783u0eborderNode {
            border: 0px solid rgba(167, 78, 48, 1);
            position: absolute;
            top: 0;
            right: 0;
            bottom: 0;
            left: 0;
            pointer-events: none;
        }
        
        .style-ja783u0einlineContent {
            position: absolute;
            top: 0;
            right: 0;
            bottom: 0;
            left: 0;
        }
        
        .style-ja783u0einlineContentParent {
            position: absolute;
            width: 100%;
            height: 100%;
        }
        
        .style-ja783u0e[data-shouldhideoverflowcontent="true"] {
            overflow: hidden;
        }
    </style>
    <style type="text/css" data-styleid="style-ja783u0m1">
        .style-ja783u0m1bg {
            border: 3px solid rgba(255, 255, 255, 1);
            background-color: transparent;
            border-radius: 0;
        }
        
        .style-ja783u0m1inlineContent,
        .style-ja783u0m1bg {
            position: absolute;
            top: 0;
            right: 0;
            bottom: 0;
            left: 0;
        }
    </style>
    <style type="text/css" data-styleid="strc1">
        .strc1inlineContent {
            position: absolute;
            top: 0;
            right: 0;
            bottom: 0;
            left: 0;
        }
    </style>
    <style type="text/css" data-styleid="style-iuonx797">
        .style-iuonx797wrapper {
            min-width: 180px;
            max-width: 980px;
            position: absolute;
            width: 100%;
        }
        
        .style-iuonx797_hiddenField {
            display: none !important;
        }
        
        .style-iuonx797 span {
            color: #FFFFFF;
            float: left;
            font: normal 14px 'Helvetica Neue', Helvetica, sans-serif;
            max-width: 60%;
        }
        
        .style-iuonx797 span.style-iuonx797_success {
            color: #68B04D;
        }
        
        .style-iuonx797 span.style-iuonx797_error {
            color: #E21C21;
        }
        
        .style-iuonx797 button,
        .style-iuonx797 input,
        .style-iuonx797 textarea {
            border-radius: 0;
            padding: 5px;
        }
        
        .style-iuonx797 input,
        .style-iuonx797 textarea {
            font: normal normal normal 16px/1.4em helvetica-w01-light, helvetica-w02-light, sans-serif;
            background-color: rgba(97, 97, 97, 1);
            -webkit-appearance: none;
            -moz-appearance: none;
            border: 0px solid rgba(61, 155, 233, 1);
            color: #FFFFFF;
            margin: 0 0 5px;
            width: 100%;
        }
        
        .style-iuonx797 input.style-iuonx797_error,
        .style-iuonx797 textarea.style-iuonx797_error {
            font: normal normal normal 16px/1.4em helvetica-w01-light, helvetica-w02-light, sans-serif;
            border: 1px solid #E21C21;
            color: #E21C21;
        }
        
        .style-iuonx797 input::-webkit-input-placeholder,
        .style-iuonx797 textarea::-webkit-input-placeholder {
            color: #FFFFFF;
        }
        
        .style-iuonx797 input:-ms-input-placeholder,
        .style-iuonx797 textarea:-ms-input-placeholder {
            color: #FFFFFF;
        }
        
        .style-iuonx797 input::-ms-input-placeholder,
        .style-iuonx797 textarea::-ms-input-placeholder {
            color: #FFFFFF;
        }
        
        .style-iuonx797 input::placeholder,
        .style-iuonx797 textarea::placeholder {
            color: #FFFFFF;
        }
        
        .style-iuonx797 button {
            background-color: rgba(255, 102, 0, 1);
            font: normal normal normal 16px/1.4em helvetica-w01-light, helvetica-w02-light, sans-serif;
            border: 0;
            color: #FFFFFF;
            cursor: pointer;
            float: right;
            margin: 0;
            max-width: 35%;
        }
        
        .style-iuonx797 textarea {
            min-height: 130px;
            resize: none;
        }
        
        .style-iuonx797[data-state~="mobile"] input {
            color: #FFFFFF;
            font-size: 16px;
            height: 45px;
            line-height: 45px;
            margin: 0 0 15px;
            padding: 0 5px;
        }
        
        .style-iuonx797[data-state~="mobile"] input::-webkit-input-placeholder {
            color: #FFFFFF;
            font-size: 16px;
        }
        
        .style-iuonx797[data-state~="mobile"] input:-ms-input-placeholder {
            color: #FFFFFF;
            font-size: 16px;
        }
        
        .style-iuonx797[data-state~="mobile"] input::-ms-input-placeholder {
            color: #FFFFFF;
            font-size: 16px;
        }
        
        .style-iuonx797[data-state~="mobile"] input::placeholder {
            color: #FFFFFF;
            font-size: 16px;
        }
        
        .style-iuonx797[data-state~="mobile"] textarea {
            color: #FFFFFF;
            font-size: 16px;
            height: 150px;
            margin: 0 0 10px;
        }
        
        .style-iuonx797[data-state~="mobile"]::-webkit-input-placeholder {
            color: #FFFFFF;
            font-size: 16px;
        }
        
        .style-iuonx797[data-state~="mobile"]:-ms-input-placeholder {
            color: #FFFFFF;
            font-size: 16px;
        }
        
        .style-iuonx797[data-state~="mobile"]::-ms-input-placeholder {
            color: #FFFFFF;
            font-size: 16px;
        }
        
        .style-iuonx797[data-state~="mobile"]::placeholder {
            color: #FFFFFF;
            font-size: 16px;
        }
        
        .style-iuonx797[data-state~="right"] {
            direction: rtl;
            text-align: right;
        }
        
        .style-iuonx797[data-state~="right"] span {
            float: right;
        }
        
        .style-iuonx797[data-state~="right"] button {
            float: left;
        }
        
        .style-iuonx797[data-state~="left"] {
            direction: ltr;
            text-align: left;
        }
        
        .style-iuonx797[data-state~="left"] span {
            float: left;
        }
        
        .style-iuonx797[data-state~="left"] button {
            float: right;
        }
    </style>
    <style type="text/css" data-styleid="style-jb2sf9se">
        .style-jb2sf9sebg {
            overflow: hidden;
            position: absolute;
            top: 0;
            right: 0;
            bottom: 0;
            left: 0;
            background-color: rgba(255, 102, 0, 1);
        }
        
        .style-jb2sf9seinlineContent {
            position: absolute;
            top: 0;
            right: 0;
            bottom: 0;
            left: 0;
        }
    </style>
    <style type="text/css" data-styleid="tpaw0">
        .tpaw0 {
            overflow: hidden;
        }
        
        .tpaw0 iframe {
            position: absolute;
            width: 100%;
            height: 100%;
            overflow: hidden;
        }
        
        .tpaw0preloaderOverlay {
            position: absolute;
            top: 0;
            left: 0;
            color: #373737;
            width: 100%;
            height: 100%;
        }
        
        .tpaw0preloaderOverlaycontent {
            width: 100%;
            height: 100%;
        }
        
        .tpaw0unavailableMessageOverlay {
            position: absolute;
            top: 0;
            left: 0;
            color: #373737;
            width: 100%;
            height: 100%;
        }
        
        .tpaw0unavailableMessageOverlaycontent {
            width: 100%;
            height: 100%;
            background: rgba(255, 255, 255, 0.9);
            font-size: 0;
            margin-top: 5px;
        }
        
        .tpaw0unavailableMessageOverlaytextContainer {
            color: #373737;
            font-family: "Helvetica Neue", "HelveticaNeueW01-55Roma", "HelveticaNeueW02-55Roma", "HelveticaNeueW10-55Roma", Helvetica, Arial, sans-serif;
            font-size: 14px;
            display: inline-block;
            vertical-align: middle;
            width: 100%;
            margin-top: 10px;
            text-align: center;
        }
        
        .tpaw0unavailableMessageOverlayreloadButton {
            display: inline-block;
        }
        
        .tpaw0unavailableMessageOverlay a {
            color: #0099FF;
            text-decoration: underline;
            cursor: pointer;
        }
        
        .tpaw0unavailableMessageOverlayiconContainer {
            display: none;
        }
        
        .tpaw0unavailableMessageOverlaydismissButton {
            display: none;
        }
        
        .tpaw0unavailableMessageOverlaytextTitle {
            font-family: "Helvetica Neue", "HelveticaNeueW01-55Roma", "HelveticaNeueW02-55Roma", "HelveticaNeueW10-55Roma", Helvetica, Arial, sans-serif;
            display: none;
        }
        
        .tpaw0unavailableMessageOverlay[data-state~="hideIframe"] .tpaw0unavailableMessageOverlay_buttons {
            opacity: 1;
        }
        
        .tpaw0unavailableMessageOverlay[data-state~="hideOverlay"] {
            display: none;
        }
    </style>
    <style type="text/css" data-styleid="Anchor_1">
        .Anchor_1 {
            visibility: hidden;
            pointer-events: none;
            width: 0 !important;
        }
    </style>
    <style type="text/css" data-styleid="style-iuoguloj">
        .style-iuogulojshowMore {
            color: #2F2E2E;
            font: normal normal normal 12px/1.4em wfont_9012a2_cf30a994f829458daa5b46a9a39e7eac, wf_cf30a994f829458daa5b46a9a, orig_cooperhewittbook;
            cursor: pointer;
            text-decoration: underline !important;
            height: 30px;
            line-height: 30px;
            position: absolute;
            bottom: 0;
            left: 35%;
            right: 35%;
            text-align: center;
        }
        
        .style-iuoguloj[data-state~="fullView"] .style-iuogulojshowMore {
            display: none;
        }
        
        .style-iuogulojimageItem {
            position: absolute;
            top: 0;
            right: 0;
            bottom: 0;
            left: 0;
        }
        
        .style-iuogulojimageItemimageWrapper {
            position: absolute;
            top: 0;
            right: 0;
            bottom: 0;
            left: 0;
            border-radius: 0;
            background-color: rgba(204, 204, 204, 0.19);
        }
        
        .style-iuogulojimageItem_imgBorder {
            position: absolute;
            top: 0;
            right: 0;
            bottom: 0;
            left: 0;
            border-radius: 0;
            border: rgba(204, 204, 204, 0.19) solid 8px;
        }
        
        .style-iuogulojimageItemimage {
            border-radius: 0;
        }
        
        .style-iuogulojimageItemzoom {
            position: absolute;
            top: 8px;
            right: 8px;
            bottom: 8px;
            left: 8px;
            background: rgba(255, 255, 255, 0.8);
            border-radius: 0;
            opacity: 0;
            transition: opacity 0.4s ease 0s;
        }
        
        .style-iuogulojimageItemtitle {
            font: normal normal normal 14px/1.4em wfont_9012a2_eab04d8add6b4f4eb1026227d1089542, wf_eab04d8add6b4f4eb1026227d, orig_montserratlight;
            color: #000000;
            white-space: nowrap;
            display: block;
        }
        
        .style-iuogulojimageItemdescription {
            color: #2F2E2E;
            font: normal normal normal 12px/1.4em wfont_9012a2_cf30a994f829458daa5b46a9a39e7eac, wf_cf30a994f829458daa5b46a9a, orig_cooperhewittbook;
            display: block;
            margin-top: .05em;
        }
        
        .style-iuogulojimageItemlink {
            font: normal normal normal 12px/1.4em wfont_9012a2_cf30a994f829458daa5b46a9a39e7eac, wf_cf30a994f829458daa5b46a9a, orig_cooperhewittbook;
            display: block;
            color: #2F2E2E;
            position: static !important;
        }
        
        .style-iuogulojimageItem_panel {
            border-radius: 0;
            height: 78px;
            position: absolute;
            bottom: 0;
            left: 0;
            right: 0;
            overflow: hidden;
            box-sizing: border-box;
            background: rgba(204, 204, 204, 0.37);
        }
        
        .style-iuogulojimageItem_panelWrap {
            position: absolute;
            left: 0px;
            right: 0px;
            top: 0px;
            bottom: 0px;
            overflow: hidden;
        }
        
        .style-iuogulojimageItem a {
            position: absolute;
            left: 0px;
            right: 0px;
            bottom: 0px;
            overflow: hidden;
            text-decoration: underline !important;
        }
        
        .style-iuogulojimageItemtitle,
        .style-iuogulojimageItemdescription {
            overflow: hidden;
            text-overflow: ellipsis;
        }
        
        .style-iuogulojimageItem[data-state~="noLink"] .style-iuogulojimageItem_panelWrap {
            bottom: 0px;
        }
        
        .style-iuogulojimageItem:hover .style-iuogulojimageItemzoom {
            opacity: 1;
            transition: opacity 0.4s ease 0s;
        }
        
        .style-iuogulojimageItemimageimage {
            position: static;
            box-shadow: #000 0 0 0;
            -webkit-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            user-select: none;
        }
    </style>
    <style type="text/css" data-styleid="v1">
        .v1videoFrame {
            position: relative;
            width: 100% !important;
            height: 100% !important;
            display: block;
        }
        
        .v1_hidden {
            display: none !important;
        }
    </style>
    <style type="text/css" data-styleid="style-ja795yey">
        .style-ja795yey {
            overflow: hidden;
        }
        
        .style-ja795yey iframe {
            position: absolute;
            width: 100%;
            height: 100%;
            overflow: hidden;
        }
        
        .style-ja795yeypreloaderOverlay {
            position: absolute;
            top: 0;
            left: 0;
            color: #373737;
            width: 100%;
            height: 100%;
        }
        
        .style-ja795yeypreloaderOverlaycontent {
            width: 100%;
            height: 100%;
        }
        
        .style-ja795yeyunavailableMessageOverlay {
            position: absolute;
            top: 0;
            left: 0;
            color: #373737;
            width: 100%;
            height: 100%;
        }
        
        .style-ja795yeyunavailableMessageOverlaycontent {
            width: 100%;
            height: 100%;
            background: rgba(255, 255, 255, 0.9);
            font-size: 0;
            margin-top: 5px;
        }
        
        .style-ja795yeyunavailableMessageOverlaytextContainer {
            color: #373737;
            font-family: "Helvetica Neue", "HelveticaNeueW01-55Roma", "HelveticaNeueW02-55Roma", "HelveticaNeueW10-55Roma", Helvetica, Arial, sans-serif;
            font-size: 14px;
            display: inline-block;
            vertical-align: middle;
            width: 100%;
            margin-top: 10px;
            text-align: center;
        }
        
        .style-ja795yeyunavailableMessageOverlayreloadButton {
            display: inline-block;
        }
        
        .style-ja795yeyunavailableMessageOverlay a {
            color: #0099FF;
            text-decoration: underline;
            cursor: pointer;
        }
        
        .style-ja795yeyunavailableMessageOverlayiconContainer {
            display: none;
        }
        
        .style-ja795yeyunavailableMessageOverlaydismissButton {
            display: none;
        }
        
        .style-ja795yeyunavailableMessageOverlaytextTitle {
            font-family: "Helvetica Neue", "HelveticaNeueW01-55Roma", "HelveticaNeueW02-55Roma", "HelveticaNeueW10-55Roma", Helvetica, Arial, sans-serif;
            display: none;
        }
        
        .style-ja795yeyunavailableMessageOverlay[data-state~="hideIframe"] .style-ja795yeyunavailableMessageOverlay_buttons {
            opacity: 1;
        }
        
        .style-ja795yeyunavailableMessageOverlay[data-state~="hideOverlay"] {
            display: none;
        }
    </style>
    <style type="text/css" data-styleid="mc1">
        .mc1inlineContent {
            position: absolute;
            top: 0;
            right: 0;
            bottom: 0;
            left: 0;
        }
        
        .mc1container {
            position: absolute;
            top: 0;
            left: 0;
            width: 100%;
            height: 100%;
        }
    </style>
    <style type="text/css" data-styleid="style-j532j2y6">
        .style-j532j2y6[data-state~="shouldUseFlex"] .style-j532j2y6link,
        .style-j532j2y6[data-state~="shouldUseFlex"] .style-j532j2y6labelwrapper {
            text-align: initial;
            display: -webkit-box;
            display: -webkit-flex;
            display: flex;
            -webkit-box-align: center;
            -webkit-align-items: center;
            align-items: center;
        }
        
        .style-j532j2y6[data-state~="shouldUseFlex"][data-state~="center"] .style-j532j2y6link,
        .style-j532j2y6[data-state~="shouldUseFlex"][data-state~="center"] .style-j532j2y6labelwrapper {
            -webkit-box-pack: center;
            -webkit-justify-content: center;
            justify-content: center;
        }
        
        .style-j532j2y6[data-state~="shouldUseFlex"][data-state~="left"] .style-j532j2y6link,
        .style-j532j2y6[data-state~="shouldUseFlex"][data-state~="left"] .style-j532j2y6labelwrapper {
            -webkit-box-pack: start;
            -webkit-justify-content: flex-start;
            justify-content: flex-start;
        }
        
        .style-j532j2y6[data-state~="shouldUseFlex"][data-state~="right"] .style-j532j2y6link,
        .style-j532j2y6[data-state~="shouldUseFlex"][data-state~="right"] .style-j532j2y6labelwrapper {
            -webkit-box-pack: end;
            -webkit-justify-content: flex-end;
            justify-content: flex-end;
        }
        
        .style-j532j2y6link {
            border-radius: 0;
            position: absolute;
            top: 0;
            right: 0;
            bottom: 0;
            left: 0;
            transition: border-color 0.4s ease 0s, background-color 0.4s ease 0s;
        }
        
        .style-j532j2y6label {
            font: normal normal normal 14px/1.4em spinnaker, sans-serif;
            transition: color 0.4s ease 0s;
            color: #FFFFFF;
            display: inline-block;
            margin: calc(-1 * 3px) 3px 0;
            position: relative;
            white-space: nowrap;
        }
        
        .style-j532j2y6[data-state~="shouldUseFlex"] .style-j532j2y6label {
            margin: 0;
        }
        
        .style-j532j2y6[data-disabled="false"] .style-j532j2y6link {
            background-color: rgba(219, 219, 0, 1);
            border: solid transparent 3px;
            cursor: pointer !important;
        }
        
        .style-j532j2y6[data-disabled="false"]:active[data-state~="mobile"] .style-j532j2y6link,
        .style-j532j2y6[data-disabled="false"]:hover[data-state~="desktop"] .style-j532j2y6link,
        .style-j532j2y6[data-disabled="false"][data-preview~="hover"] .style-j532j2y6link {
            background-color: transparent;
            border-color: rgba(97, 97, 97, 1);
        }
        
        .style-j532j2y6[data-disabled="false"]:active[data-state~="mobile"] .style-j532j2y6label,
        .style-j532j2y6[data-disabled="false"]:hover[data-state~="desktop"] .style-j532j2y6label,
        .style-j532j2y6[data-disabled="false"][data-preview~="hover"] .style-j532j2y6label {
            color: #616161;
        }
        
        .style-j532j2y6[data-disabled="true"] .style-j532j2y6link,
        .style-j532j2y6[data-preview~="disabled"] .style-j532j2y6link {
            background-color: rgba(204, 204, 204, 1);
            border-color: rgba(204, 204, 204, 1);
        }
        
        .style-j532j2y6[data-disabled="true"] .style-j532j2y6label,
        .style-j532j2y6[data-preview~="disabled"] .style-j532j2y6label {
            color: #FFFFFF;
        }
    </style>
    <style type="text/css" data-styleid="style-iuq3miwd">
        .style-iuq3miwd[data-state~="shouldUseFlex"] .style-iuq3miwdlink,
        .style-iuq3miwd[data-state~="shouldUseFlex"] .style-iuq3miwdlabelwrapper {
            text-align: initial;
            display: -webkit-box;
            display: -webkit-flex;
            display: flex;
            -webkit-box-align: center;
            -webkit-align-items: center;
            align-items: center;
        }
        
        .style-iuq3miwd[data-state~="shouldUseFlex"][data-state~="center"] .style-iuq3miwdlink,
        .style-iuq3miwd[data-state~="shouldUseFlex"][data-state~="center"] .style-iuq3miwdlabelwrapper {
            -webkit-box-pack: center;
            -webkit-justify-content: center;
            justify-content: center;
        }
        
        .style-iuq3miwd[data-state~="shouldUseFlex"][data-state~="left"] .style-iuq3miwdlink,
        .style-iuq3miwd[data-state~="shouldUseFlex"][data-state~="left"] .style-iuq3miwdlabelwrapper {
            -webkit-box-pack: start;
            -webkit-justify-content: flex-start;
            justify-content: flex-start;
        }
        
        .style-iuq3miwd[data-state~="shouldUseFlex"][data-state~="right"] .style-iuq3miwdlink,
        .style-iuq3miwd[data-state~="shouldUseFlex"][data-state~="right"] .style-iuq3miwdlabelwrapper {
            -webkit-box-pack: end;
            -webkit-justify-content: flex-end;
            justify-content: flex-end;
        }
        
        .style-iuq3miwdlink {
            border-radius: 0;
            position: absolute;
            top: 0;
            right: 0;
            bottom: 0;
            left: 0;
            transition: border-color 0.4s ease 0s, background-color 0.4s ease 0s;
        }
        
        .style-iuq3miwdlabel {
            font: normal normal normal 14px/1.4em spinnaker, sans-serif;
            transition: color 0.4s ease 0s;
            color: #FFFFFF;
            display: inline-block;
            margin: calc(-1 * 3px) 3px 0;
            position: relative;
            white-space: nowrap;
        }
        
        .style-iuq3miwd[data-state~="shouldUseFlex"] .style-iuq3miwdlabel {
            margin: 0;
        }
        
        .style-iuq3miwd[data-disabled="false"] .style-iuq3miwdlink {
            background-color: rgba(255, 102, 0, 1);
            border: solid transparent 3px;
            cursor: pointer !important;
        }
        
        .style-iuq3miwd[data-disabled="false"]:active[data-state~="mobile"] .style-iuq3miwdlink,
        .style-iuq3miwd[data-disabled="false"]:hover[data-state~="desktop"] .style-iuq3miwdlink,
        .style-iuq3miwd[data-disabled="false"][data-preview~="hover"] .style-iuq3miwdlink {
            background-color: transparent;
            border-color: rgba(97, 97, 97, 1);
        }
        
        .style-iuq3miwd[data-disabled="false"]:active[data-state~="mobile"] .style-iuq3miwdlabel,
        .style-iuq3miwd[data-disabled="false"]:hover[data-state~="desktop"] .style-iuq3miwdlabel,
        .style-iuq3miwd[data-disabled="false"][data-preview~="hover"] .style-iuq3miwdlabel {
            color: #616161;
        }
        
        .style-iuq3miwd[data-disabled="true"] .style-iuq3miwdlink,
        .style-iuq3miwd[data-preview~="disabled"] .style-iuq3miwdlink {
            background-color: rgba(204, 204, 204, 1);
            border-color: rgba(204, 204, 204, 1);
        }
        
        .style-iuq3miwd[data-disabled="true"] .style-iuq3miwdlabel,
        .style-iuq3miwd[data-preview~="disabled"] .style-iuq3miwdlabel {
            color: #FFFFFF;
        }
    </style>
    <style type="text/css" data-styleid="deadComp">
        .deadComp {
            background: transparent;
        }
    </style>
    <style type="text/css" data-styleid="siteBackground">
        .siteBackground {
            width: 100%;
            position: absolute;
        }
        
        .siteBackgroundbgBeforeTransition {
            position: absolute;
            top: 0;
        }
        
        .siteBackgroundbgAfterTransition {
            position: absolute;
            top: 0;
        }
    </style>
    <style type="text/css" data-styleid="loginDialog">
        .loginDialog {
            position: fixed;
            width: 100%;
            height: 100%;
            z-index: 99;
            font-family: Arial, sans-serif;
            font-size: 1em;
            color: #9C9C9C;
        }
        
        .loginDialogblockingLayer {
            background-color: rgba(85, 85, 85, 0.75);
            position: fixed;
            top: 0;
            right: 0;
            bottom: 0;
            left: 0;
            visibility: visible;
            zoom: 1;
            overflow: auto;
        }
        
        .loginDialogdialog {
            background-color: rgba(170, 170, 170, 0.7);
            width: 455px;
            position: fixed;
            padding: 20px;
        }
        
        .loginDialog_wrapper {
            background-color: rgba(255, 255, 255, 1);
            padding: 45px 40px 0 40px;
        }
        
        .loginDialogxButton {
            position: absolute;
            top: -14px;
            right: -14px;
            cursor: pointer;
            background: transparent url(https://static.parastorage.com/services/skins/2.1229.79/images/wysiwyg/core/themes/base/viewer_login_sprite.png) no-repeat right top;
            height: 30px;
            width: 30px;
        }
        
        .loginDialogxButton:hover {
            background-position: right -80px;
        }
        
        .loginDialogheader {
            padding-bottom: 25px;
            line-height: 30px;
        }
        
        .loginDialogfavIcon {
            float: left;
            margin: 7px 7px 0 0;
            width: 16px;
            height: 16px;
        }
        
        .loginDialogtitle {
            font-size: 20px;
            font-weight: bold;
            color: #555555;
        }
        
        .loginDialog[data-state~="mobile"] {
            position: absolute;
            width: 100%;
            height: 100%;
            z-index: 99;
            font-family: Arial, sans-serif;
            font-size: 1em;
            color: #9C9C9C;
            top: 0;
        }
        
        .loginDialog[data-state~="mobile"] .loginDialogheader {
            padding-bottom: 10px;
            line-height: 30px;
        }
        
        .loginDialog[data-state~="mobile"] .loginDialogfavIcon {
            display: none;
        }
        
        .loginDialog[data-state~="mobile"] .loginDialogtitle {
            font-size: 14px;
        }
        
        .loginDialog[data-state~="mobile"] .loginDialogdialog {
            width: 260px;
            padding: 10px;
            position: absolute;
        }
        
        .loginDialog[data-state~="mobile"] .loginDialog_footer {
            margin-top: 0;
            padding-bottom: 10px;
        }
        
        .loginDialog[data-state~="mobile"] .loginDialogcancel {
            font-size: 14px;
            line-height: 30px;
        }
        
        .loginDialog[data-state~="mobile"] .loginDialog_wrapper {
            padding: 14px 12px 0 12px;
        }
        
        .loginDialog[data-state~="mobile"] .loginDialogsubmitButton {
            height: 30px;
            width: 100px;
            font-size: 14px;
        }
        
        .loginDialog_forgot {
            text-align: left;
            font-size: 12px;
        }
        
        .loginDialog_forgot a {
            color: #0198ff;
            border-bottom: 1px solid #0198ff;
        }
        
        .loginDialog_forgot a:hover {
            color: #0044ff;
            border-bottom: 1px solid #0044ff;
        }
        
        .loginDialog_error {
            font-size: 12px;
            color: #d74401;
            text-align: right;
        }
        
        .loginDialog_footer {
            width: 100%;
            margin-top: 27px;
            padding-bottom: 40px;
        }
        
        .loginDialogcancel {
            color: #9C9C9C;
            font-size: 18px;
            text-decoration: underline;
            line-height: 36px;
        }
        
        .loginDialogcancel:hover {
            color: #9c3c3c;
        }
        
        .loginDialogpasswordInput label {
            font-size: 14px;
        }
        
        .loginDialogpasswordInput label[data-type="password"] {
            font-size: 14px;
            line-height: 30px;
            height: 30px;
        }
        
        .loginDialogsubmitButton {
            float: right;
            cursor: pointer;
            border: solid 2px #0064a8;
            height: 36px;
            width: 143px;
            background: transparent url(https://static.parastorage.com/services/skins/2.1229.79/images/wysiwyg/core/themes/base/viewer_login_sprite.png) repeat-x right -252px;
            color: #ffffff;
            font-size: 24px;
            font-weight: bold;
            box-shadow: 0 1px 3px rgba(0, 0, 0, 0.5);
        }
        
        .loginDialogsubmitButton:hover {
            background-position: right -352px;
            border-color: #004286;
        }
        
        .loginDialog[data-state="normal"] .loginDialogerror {
            display: none;
        }
        
        .loginDialog[data-state="error"] .loginDialogerror {
            display: block;
            font-size: 12px;
            color: #d74401;
            text-align: right;
        }
        
        .loginDialog[data-state="error"] .loginDialogpasswordInput {
            border-color: #d74401;
        }
        
        .loginDialogpasswordInput {
            font-size: 1em;
        }
        
        .loginDialogpasswordInput label {
            float: none;
            font-size: 17px;
            line-height: 25px;
            color: #585858;
        }
        
        .loginDialogpasswordInput[data-state~="mobile"] label {
            float: none;
            font-size: 14px;
            line-height: 20px;
            color: #585858;
        }
        
        .loginDialogpasswordInputinput {
            padding: 0 15px;
            width: 100%;
            height: 42px;
            font-size: 19px;
            line-height: 42px;
            color: #0198ff;
            margin: 0 -3px;
            background: transparent url(https://static.parastorage.com/services/skins/2.1229.79/images/wysiwyg/core/themes/base/viewer_login_sprite.png) repeat-x right -170px;
            border: solid 1px #a1a1a1;
            box-sizing: border-box;
        }
        
        .loginDialogpasswordInput[data-state~="mobile"] .loginDialogpasswordInputinput {
            padding: 0 15px;
            width: 100%;
            height: 30px;
            font-size: 14px;
            line-height: 30px;
            color: #0198ff;
            margin: 0 -3px;
            background: transparent url(https://static.parastorage.com/services/skins/2.1229.79/images/wysiwyg/core/themes/base/viewer_login_sprite.png) repeat-x right -170px;
            border: solid 1px #a1a1a1;
            box-sizing: border-box;
        }
        
        .loginDialogpasswordInputinput[data-type="password"] {
            font-size: 38px;
        }
        
        .loginDialogpasswordInput[data-state~="mobile"] .loginDialogpasswordInputinput[data-type="password"] {
            font-size: 14px;
        }
        
        .loginDialogpasswordInputerrorMessage {
            display: block;
            font-size: 12px;
            color: #d74401;
            text-align: right;
            height: 15px;
        }
        
        .loginDialogpasswordInput:not([data-state~="invalid"]) .loginDialogpasswordInputerrorMessage {
            visibility: hidden;
        }
        
        .loginDialogpasswordInput[data-state~="invalid"] .loginDialogpasswordInputerrorMessage {
            visibility: visible;
        }
        
        .loginDialogpasswordInput[data-state~="invalid"] input {
            border-color: #d74401;
        }
        
        .loginDialogpasswordInput[data-state~="invalid"] .loginDialogpasswordInputinput {
            border-color: #ff0000;
        }
    </style>
    <style type="text/css">
        .testStyles {
            position: absolute;
            display: none;
            z-index: 1
        }
    </style>
    <div class="testStyles"></div>
    <style type="text/css" data-styleid="uploadedFonts">
        @font-face {
            font-family: wf_6d1c84e5242d4b0e8c7bd107c;
            src: url("<?php echo get_template_directory_uri();?>/assets/fonts/file.woff") format("woff"), url("<?php echo get_template_directory_uri();?>/assets/fonts/file.woff2") format("woff2"), url("<?php echo get_template_directory_uri();?>/assets/fonts/file.ttf") format("ttf");
        }
        
        @font-face {
            font-family: wf_cf30a994f829458daa5b46a9a;
            src: url("<?php echo get_template_directory_uri();?>/assets/fonts/file.woff") format("woff"), url("<?php echo get_template_directory_uri();?>/assets/fonts/file.woff2") format("woff2"), url("<?php echo get_template_directory_uri();?>/assets/fonts/file.ttf") format("ttf");
        }
        
        @font-face {
            font-family: wf_eab04d8add6b4f4eb1026227d;
            src: url("<?php echo get_template_directory_uri();?>/assets/fonts/file.woff") format("woff"), url("<?php echo get_template_directory_uri();?>/assets/fonts/file.woff2") format("woff2"), url("<?php echo get_template_directory_uri();?>/assets/fonts/file.ttf") format("ttf");
        }
        
        @font-face {
            font-family: wf_eab04d8add6b4f4eb1026227d;
            src: url("<?php echo get_template_directory_uri();?>/assets/fonts/file.woff") format("woff"), url("<?php echo get_template_directory_uri();?>/assets/fonts/file.woff2") format("woff2"), url("<?php echo get_template_directory_uri();?>/assets/fonts/file.ttf") format("ttf");
        }
        
        @font-face {
            font-family: wf_cf30a994f829458daa5b46a9a;
            src: url("<?php echo get_template_directory_uri();?>/assets/fonts/file.woff") format("woff"), url("<?php echo get_template_directory_uri();?>/assets/fonts/file.woff2") format("woff2"), url("<?php echo get_template_directory_uri();?>/assets/fonts/file.ttf") format("ttf");
        }
    </style>
    <div style="overflow: hidden; visibility: hidden; max-height: 0px; max-width: 0px; position: absolute;">
        <style>
            .font-ruler-content::after {
                content: "@#$%%^&*~IAO"
            }
        </style>
        <div style="position: absolute; overflow: hidden; font-size: 1200px; left: -2000px; visibility: hidden; width: 3190px; height: 0px;">
            <div style="position: relative; white-space: nowrap; font-family: serif;">
                <div style="position: absolute; width: 100%; height: 100%; overflow: hidden;">
                    <div style="width: 3192px; height: 1px;"></div>
                </div><span class="font-ruler-content" style="font-family: wfont_9012a2_6d1c84e5242d4b0e8c7bd107ccec04ac, serif;"></span></div>
        </div>
        <div style="position: absolute; overflow: hidden; font-size: 1200px; left: -2000px; visibility: hidden; width: 3190px; height: 0px;">
            <div style="position: relative; white-space: nowrap; font-family: serif;">
                <div style="position: absolute; width: 100%; height: 100%; overflow: hidden;">
                    <div style="width: 3192px; height: 1px;"></div>
                </div><span class="font-ruler-content" style="font-family: wfont_9012a2_cf30a994f829458daa5b46a9a39e7eac, serif;"></span></div>
        </div>
        <div style="position: absolute; overflow: hidden; font-size: 1200px; left: -2000px; visibility: hidden; width: 3191px; height: 0px;">
            <div style="position: relative; white-space: nowrap; font-family: serif;">
                <div style="position: absolute; width: 100%; height: 100%; overflow: hidden;">
                    <div style="width: 3193px; height: 1px;"></div>
                </div><span class="font-ruler-content" style="font-family: wfont_9012a2_eab04d8add6b4f4eb1026227d1089542, serif;"></span></div>
        </div>
    </div>
</div>

<body class="" style="">
    <div id="SITE_CONTAINER" data-santa-render-status="CLIENT">
        <div data-reactroot="" class="noop" data-santa-version="1.2682.22">
            <div id="SITE_BACKGROUND" class="siteBackground" style="position: absolute; top: 0px; height: 5499px; width: 1349px;">
                <div id="SITE_BACKGROUND_previous_noPrev" class="siteBackgroundprevious" style="width: 100%; height: 100%;">
                    <div id="SITE_BACKGROUNDpreviousImage" class="siteBackgroundpreviousImage"></div>
                    <div id="SITE_BACKGROUNDpreviousVideo" class="siteBackgroundpreviousVideo"></div>
                    <div id="SITE_BACKGROUND_previousOverlay_noPrev" class="siteBackgroundpreviousOverlay"></div>
                </div>
                <div id="SITE_BACKGROUND_current_customBgImg3vn" data-position="fixed" class="siteBackgroundcurrent" style="top: 0px; background-color: rgb(255, 255, 255); position: fixed; width: 100%; height: 100%;">
                    <div id="SITE_BACKGROUND_currentImage_customBgImg3vn" data-type="bgimage" data-height="100%" class="siteBackgroundcurrentImage" style="position: absolute; top: 0px; width: 100%; opacity: 0.12; background-size: cover; background-position: center center; background-repeat: no-repeat; height: 100%; background-image: url(&quot;<?php echo get_template_directory_uri();?>/assets/img/9012a2_dd3d0352ce7d4ee69562a1b20b6f2316_mv2_d_1600_1200_s_2.webp&quot;);" data-image-css="{&quot;backgroundSize&quot;:&quot;cover&quot;,&quot;backgroundPosition&quot;:&quot;center center&quot;,&quot;backgroundRepeat&quot;:&quot;no-repeat&quot;,&quot;height&quot;:&quot;100%&quot;}"></div>
                    <div id="SITE_BACKGROUNDcurrentVideo" class="siteBackgroundcurrentVideo"></div>
                    <div id="SITE_BACKGROUND_currentOverlay_customBgImg3vn" class="siteBackgroundcurrentOverlay" style="position: absolute; top: 0px; width: 100%; height: 100%;"></div>
                </div>
            </div>
            <div class="SITE_ROOT" id="SITE_ROOT" style="width: 980px; padding-bottom: 0px;">
                <div id="masterPage" style="width: 980px; position: static; top: 0px; height: 5499px;">
                    <header class="style-iuoiszjc" data-state="fixedPosition" id="SITE_HEADER" style="width: 980px; z-index: 50; top: 0px; height: 56px; left: 0px; position: fixed;">
                        <div id="SITE_HEADERscreenWidthBackground" class="style-iuoiszjcscreenWidthBackground" style="width: 1349px; left: -185px;">
                            <div class="style-iuoiszjc_bg"></div>
                        </div>
                        <div id="SITE_HEADERcenteredContent" class="style-iuoiszjccenteredContent">
                            <div id="SITE_HEADERbg" class="style-iuoiszjcbg"></div>
                            <div id="SITE_HEADERinlineContent" class="style-iuoiszjcinlineContent">
                                <nav id="iebebgbn" data-menuborder-y="0" data-menubtn-border="0" data-ribbon-els="0" data-label-pad="0" data-ribbon-extra="0" data-drophposition="" data-dropalign="left" dir="ltr" class="style-iurapags" data-state="left notMobile" style="left: 263px; width: 717px; position: absolute; top: 11px; height: 36px;" data-dropmode="dropDown">
                                    <ul aria-label="Site navigation" role="navigation" id="iebebgbnitemsContainer" class="style-iurapagsitemsContainer" style="text-align: left;">
                                        <li data-direction="ltr" data-listposition="left" data-data-id="bmi23nr" class="style-iurapagsrepeaterButton" data-state="menu  idle link notMobile" id="iebebgbn0" data-original-gap-between-text-and-btn="10" aria-hidden="false" style="width: 71px; height: 36px; position: relative; box-sizing: border-box; overflow: visible;">
                                            <a role="button" tabindex="0" aria-haspopup="false" data-listposition="left" href="" target="_self" id="iebebgbn0linkElement" class="style-iurapagsrepeaterButtonlinkElement">
                                                <div class="style-iurapagsrepeaterButton_gapper">
                                                    <div id="iebebgbn0bg" class="style-iurapagsrepeaterButtonbg" style="text-align: right;">
                                                        <p id="iebebgbn0label" class="style-iurapagsrepeaterButtonlabel" style="text-align: right; line-height: 36px;">INICIO</p>
                                                    </div>
                                                </div>
                                            </a>
                                        </li>
                                        <li data-direction="ltr" data-listposition="center" data-data-id="bmi1crc" class="style-iurapagsrepeaterButton" data-state="menu  idle link notMobile" id="iebebgbn1" data-original-gap-between-text-and-btn="11" aria-hidden="false" style="width: 80px; height: 36px; position: relative; box-sizing: border-box; overflow: visible;">
                                            <a role="button" tabindex="0" aria-haspopup="false" data-listposition="center" href="" target="_self" data-keep-roots="true" data-anchor="c21z" id="iebebgbn1linkElement" class="style-iurapagsrepeaterButtonlinkElement">
                                                <div class="style-iurapagsrepeaterButton_gapper">
                                                    <div id="iebebgbn1bg" class="style-iurapagsrepeaterButtonbg" style="text-align: right;">
                                                        <p id="iebebgbn1label" class="style-iurapagsrepeaterButtonlabel" style="text-align: right; line-height: 36px;">PLANES</p>
                                                    </div>
                                                </div>
                                            </a>
                                        </li>
                                        <li data-direction="ltr" data-listposition="center" data-data-id="bmitt4" class="style-iurapagsrepeaterButton" data-state="menu  idle link notMobile" id="iebebgbn2" data-original-gap-between-text-and-btn="10" aria-hidden="false" style="width: 147px; height: 36px; position: relative; box-sizing: border-box; overflow: visible;">
                                            <a role="button" tabindex="0" aria-haspopup="false" data-listposition="center" href="" target="_self" data-keep-roots="true" data-anchor="c18fq" id="iebebgbn2linkElement" class="style-iurapagsrepeaterButtonlinkElement">
                                                <div class="style-iurapagsrepeaterButton_gapper">
                                                    <div id="iebebgbn2bg" class="style-iurapagsrepeaterButtonbg" style="text-align: right;">
                                                        <p id="iebebgbn2label" class="style-iurapagsrepeaterButtonlabel" style="text-align: right; line-height: 36px;">CONTACTO / COTIZA</p>
                                                    </div>
                                                </div>
                                            </a>
                                        </li>
                                        <li data-direction="ltr" data-listposition="center" data-data-id="bmi1wt6" class="style-iurapagsrepeaterButton" data-state="menu  idle link notMobile" id="iebebgbn3" data-original-gap-between-text-and-btn="11" aria-hidden="false" style="width: 81px; height: 36px; position: relative; box-sizing: border-box; overflow: visible;">
                                            <a role="button" tabindex="0" aria-haspopup="false" data-listposition="center" href="" target="_self" data-keep-roots="true" data-anchor="cal6" id="iebebgbn3linkElement" class="style-iurapagsrepeaterButtonlinkElement">
                                                <div class="style-iurapagsrepeaterButton_gapper">
                                                    <div id="iebebgbn3bg" class="style-iurapagsrepeaterButtonbg" style="text-align: right;">
                                                        <p id="iebebgbn3label" class="style-iurapagsrepeaterButtonlabel" style="text-align: right; line-height: 36px;">EQUIPO</p>
                                                    </div>
                                                </div>
                                            </a>
                                        </li>
                                        <li data-direction="ltr" data-listposition="center" data-data-id="dataItem-iuiqlsy6" class="style-iurapagsrepeaterButton" data-state="menu selected idle link notMobile" id="iebebgbn4" data-original-gap-between-text-and-btn="10" aria-hidden="false" style="width: 88px; height: 36px; position: relative; box-sizing: border-box; overflow: visible;">
                                            <a role="button" tabindex="0" aria-haspopup="false" data-listposition="center" href="" target="_self" data-keep-roots="true" data-anchor="dataItem-iuk2rpbr" id="iebebgbn4linkElement" class="style-iurapagsrepeaterButtonlinkElement">
                                                <div class="style-iurapagsrepeaterButton_gapper">
                                                    <div id="iebebgbn4bg" class="style-iurapagsrepeaterButtonbg" style="text-align: right;">
                                                        <p id="iebebgbn4label" class="style-iurapagsrepeaterButtonlabel" style="text-align: right; line-height: 36px;">NOTICIAS</p>
                                                    </div>
                                                </div>
                                            </a>
                                        </li>
                                        <li data-direction="ltr" data-listposition="center" data-data-id="dataItem-iupjtjuw" class="style-iurapagsrepeaterButton" data-state="menu  idle link notMobile" id="iebebgbn5" data-original-gap-between-text-and-btn="11" aria-hidden="false" style="width: 127px; height: 36px; position: relative; box-sizing: border-box; overflow: visible;">
                                            <a role="button" tabindex="0" aria-haspopup="false" data-listposition="center" href="" target="_self" id="iebebgbn5linkElement" class="style-iurapagsrepeaterButtonlinkElement">
                                                <div class="style-iurapagsrepeaterButton_gapper">
                                                    <div id="iebebgbn5bg" class="style-iurapagsrepeaterButtonbg" style="text-align: right;">
                                                        <p id="iebebgbn5label" class="style-iurapagsrepeaterButtonlabel" style="text-align: right; line-height: 36px;">RESERVA / PAGA</p>
                                                    </div>
                                                </div>
                                            </a>
                                        </li>
                                        <li data-direction="ltr" data-listposition="center" data-data-id="dataItem-j7tlf9yp" class="style-iurapagsrepeaterButton" data-state="menu  idle link notMobile" id="iebebgbn6" data-original-gap-between-text-and-btn="10" aria-hidden="false" style="width: 57px; height: 36px; position: relative; box-sizing: border-box; overflow: visible;">
                                            <a role="button" tabindex="0" aria-haspopup="false" data-listposition="center" href="" target="_self" id="iebebgbn6linkElement" class="style-iurapagsrepeaterButtonlinkElement">
                                                <div class="style-iurapagsrepeaterButton_gapper">
                                                    <div id="iebebgbn6bg" class="style-iurapagsrepeaterButtonbg" style="text-align: right;">
                                                        <p id="iebebgbn6label" class="style-iurapagsrepeaterButtonlabel" style="text-align: right; line-height: 36px;">FAQ</p>
                                                    </div>
                                                </div>
                                            </a>
                                        </li>
                                        <li data-direction="ltr" data-listposition="right" data-data-id="dataItem-ix7z0gbt" class="style-iurapagsrepeaterButton" data-state="menu  idle link notMobile" id="iebebgbn7" data-original-gap-between-text-and-btn="11" aria-hidden="false" style="width: 66px; height: 36px; position: relative; box-sizing: border-box; overflow: visible;">
                                            <a role="button" tabindex="0" aria-haspopup="false" data-listposition="right" href="" target="_self" id="iebebgbn7linkElement" class="style-iurapagsrepeaterButtonlinkElement">
                                                <div class="style-iurapagsrepeaterButton_gapper">
                                                    <div id="iebebgbn7bg" class="style-iurapagsrepeaterButtonbg" style="text-align: right;">
                                                        <p id="iebebgbn7label" class="style-iurapagsrepeaterButtonlabel" style="text-align: right; line-height: 36px;">BLOG</p>
                                                    </div>
                                                </div>
                                            </a>
                                        </li>
                                        <li data-listposition="right" class="style-iurapagsrepeaterButton" data-state="menu  idle header notMobile" id="iebebgbn__more__" data-original-gap-between-text-and-btn="10" aria-hidden="true" style="height: 0px; overflow: hidden; position: absolute;">
                                            <a role="button" tabindex="-1" aria-haspopup="true" data-listposition="right" id="iebebgbn__more__linkElement" class="style-iurapagsrepeaterButtonlinkElement">
                                                <div class="style-iurapagsrepeaterButton_gapper">
                                                    <div id="iebebgbn__more__bg" class="style-iurapagsrepeaterButtonbg" style="text-align: right;">
                                                        <p id="iebebgbn__more__label" class="style-iurapagsrepeaterButtonlabel" style="text-align: right;">More</p>
                                                    </div>
                                                </div>
                                            </a>
                                        </li>
                                    </ul>
                                    <div id="iebebgbnmoreButton" class="style-iurapagsmoreButton"></div>
                                    <nav data-drophposition="" data-dropalign="left" id="iebebgbndropWrapper" class="style-iurapagsdropWrapper" style="visibility: hidden;">
                                        <ul id="iebebgbnmoreContainer" class="style-iurapagsmoreContainer" style="visibility: hidden;"></ul>
                                    </nav>
                                </nav>
                                <div data-exact-height="51" data-content-padding-horizontal="0" data-content-padding-vertical="0" title="TUCASAOK fondo blanco.png" class="wp2" id="comp-iuraqq1n" style="left: 3px; position: absolute; top: 4px; width: 235px; height: 51px;">
                                    <div id="comp-iuraqq1nlink" class="wp2link" style="width: 235px; height: 51px;">
                                        <div data-style="" class="wp2img" id="comp-iuraqq1nimg" style="position: relative; width: 235px; height: 51px;">
                                            <div class="wp2imgpreloader" id="comp-iuraqq1nimgpreloader"></div><img id="comp-iuraqq1nimgimage" alt="" data-type="image" src="<?php echo get_template_directory_uri();?>/assets/img/9012a2_1423de08d3ad4139927fe074486d6795_mv2.png" style="width: 235px; height: 51px; object-fit: cover;"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </header>
                    <footer class="style-iuonybvz_footer style-iuonybvz" data-state=" " id="SITE_FOOTER" style="width: 980px; position: absolute; left: 0px; height: 309px; bottom: auto; top: 5190px;">
                        <div id="SITE_FOOTERscreenWidthBackground" class="style-iuonybvzscreenWidthBackground" style="width: 1349px; left: -185px;">
                            <div class="style-iuonybvz_bg"></div>
                        </div>
                        <div id="SITE_FOOTERcenteredContent" class="style-iuonybvzcenteredContent">
                            <div id="SITE_FOOTERbg" class="style-iuonybvzbg"></div>
                            <div id="SITE_FOOTERinlineContent" class="style-iuonybvzinlineContent">
                                <div data-exact-height="40" data-content-padding-horizontal="0" data-content-padding-vertical="0" title="" class="ca1" data-angle="180" id="iebcx6jn" style="left: 469px; position: absolute; transform: rotate(180deg) translateZ(0px); top: 1px; width: 44px; height: 40px;">
                                    <a href="" target="_self" data-keep-roots="true" data-anchor="SCROLL_TO_TOP" id="iebcx6jnlink" class="ca1link" style="cursor: pointer; width: 44px; height: 40px;">
                                        <div data-style="" class="ca1img" id="iebcx6jnimg" style="position: relative; width: 44px; height: 40px;">
                                            <div class="ca1imgpreloader" id="iebcx6jnimgpreloader"></div><img id="iebcx6jnimgimage" alt="" data-type="image" style="width: 44px; height: 40px; object-fit: contain;" src="<?php echo get_template_directory_uri();?>/assets/img/17e0089d060d470e903213dcb8e4d4be.png"></div>
                                    </a>
                                </div>
                                <div data-exact-height="22" data-content-padding-horizontal="0" data-content-padding-vertical="0" title="" class="wp2" id="comp-iuiqbyk6" style="left: 4px; position: absolute; top: 126px; width: 141px; height: 22px;">
                                    <a href="" target="_blank" data-content="" data-type="external" id="comp-iuiqbyk6link" class="wp2link" style="cursor: pointer; width: 141px; height: 22px;">
                                        <div data-style="" class="wp2img" id="comp-iuiqbyk6img" style="position: relative; width: 141px; height: 22px;">
                                            <div class="wp2imgpreloader" id="comp-iuiqbyk6imgpreloader"></div><img id="comp-iuiqbyk6imgimage" alt="" data-type="image" style="width: 141px; height: 22px; object-fit: fill;" src="<?php echo get_template_directory_uri();?>/assets/img/9012a2_d391ef036bdd4d59a1c9db738600ca91_mv2.png"></div>
                                    </a>
                                </div>
                                <div data-exact-height="21" data-content-padding-horizontal="0" data-content-padding-vertical="0" title="" class="wp2" id="comp-j7co0qhb" style="left: 4px; position: absolute; top: 164px; width: 141px; height: 21px;">
                                    <a href="" target="_blank" data-content="" data-type="external" id="comp-j7co0qhblink" class="wp2link" style="cursor: pointer; width: 141px; height: 21px;">
                                        <div data-style="" class="wp2img" id="comp-j7co0qhbimg" style="position: relative; width: 141px; height: 21px;">
                                            <div class="wp2imgpreloader" id="comp-j7co0qhbimgpreloader"></div><img id="comp-j7co0qhbimgimage" alt="" data-type="image" style="width: 141px; height: 21px; object-fit: fill;" src="<?php echo get_template_directory_uri();?>/assets/img/9012a2_bd3ebd5b9458442d90bb7106eab6584f_mv2.png"></div>
                                    </a>
                                </div>
                                <div data-exact-height="23" data-content-padding-horizontal="0" data-content-padding-vertical="0" title="" class="wp2" id="comp-j7gazpgn" style="left: 4px; position: absolute; top: 192px; width: 78px; height: 23px;">
                                    <a href="" target="_blank" data-content="" data-type="external" id="comp-j7gazpgnlink" class="wp2link" style="cursor: pointer; width: 78px; height: 23px;">
                                        <div data-style="" class="wp2img" id="comp-j7gazpgnimg" style="position: relative; width: 78px; height: 23px;">
                                            <div class="wp2imgpreloader" id="comp-j7gazpgnimgpreloader"></div><img id="comp-j7gazpgnimgimage" alt="" data-type="image" style="width: 78px; height: 23px; object-fit: cover;" src="<?php echo get_template_directory_uri();?>/assets/img/9012a2_fd926cd9fe9349f8b70b1b9ed4c6aee2_mv2.png"></div>
                                    </a>
                                </div>
                                <div class="txtNew" id="comp-j7tmkocb" style="left: 4px; width: 159px; position: absolute; top: 71px;">
                                    <h5 class="font_5" style="line-height:1em; font-size:15px;"><span style="font-size:15px;"><span style="color:rgb(255, 255, 255); font-family:wfont_9012a2_6d1c84e5242d4b0e8c7bd107ccec04ac, wf_6d1c84e5242d4b0e8c7bd107c, orig_helvetica_bold; letter-spacing:0.08em;">TRABAJAMOS CON</span></span></h5></div>
                                <div class="lb1" id="comp-iuoizmpz" style="width: 110px; left: 181px; position: absolute; top: 112px; height: 36px;">
                                    <ul aria-label="Social bar" id="comp-iuoizmpzitemsContainer" class="lb1itemsContainer">
                                        <li class="lb1imageItem" id="comp-iuoizmpz0image" style="width: 36px; height: 36px; margin-bottom: 0px; margin-right: 1px; display: inline-block;">
                                            <a href="" target="_blank" data-content="" data-type="external" id="comp-iuoizmpz0imagelink" class="lb1imageItemlink">
                                                <div data-style="position:absolute" class="lb1imageItemimage" id="comp-iuoizmpz0imageimage" style="position: absolute; width: 36px; height: 36px;">
                                                    <div class="lb1imageItemimagepreloader" id="comp-iuoizmpz0imageimagepreloader"></div><img id="comp-iuoizmpz0imageimageimage" alt="White Instagram Icon" data-type="image" style="width: 36px; height: 36px; object-fit: cover;" src="<?php echo get_template_directory_uri();?>/assets/img/81af6121f84c41a5b4391d7d37fce12a.png"></div>
                                            </a>
                                        </li>
                                        <li class="lb1imageItem" id="comp-iuoizmpz1image" style="width: 36px; height: 36px; margin-bottom: 0px; margin-right: 1px; display: inline-block;">
                                            <a href="" target="_blank" data-content="" data-type="external" id="comp-iuoizmpz1imagelink" class="lb1imageItemlink">
                                                <div data-style="position:absolute" class="lb1imageItemimage" id="comp-iuoizmpz1imageimage" style="position: absolute; width: 36px; height: 36px;">
                                                    <div class="lb1imageItemimagepreloader" id="comp-iuoizmpz1imageimagepreloader"></div><img id="comp-iuoizmpz1imageimageimage" alt="White Facebook Icon" data-type="image" style="width: 36px; height: 36px; object-fit: cover;" src="<?php echo get_template_directory_uri();?>/assets/img/23fd2a2be53141ed810f4d3dcdcd01fa.png"></div>
                                            </a>
                                        </li>
                                        <li class="lb1imageItem" id="comp-iuoizmpz2image" style="width: 36px; height: 36px; margin-bottom: 0px; margin-right: 1px; display: inline-block;">
                                            <a href="" target="_blank" data-content="" data-type="external" id="comp-iuoizmpz2imagelink" class="lb1imageItemlink">
                                                <div data-style="position:absolute" class="lb1imageItemimage" id="comp-iuoizmpz2imageimage" style="position: absolute; width: 36px; height: 36px;">
                                                    <div class="lb1imageItemimagepreloader" id="comp-iuoizmpz2imageimagepreloader"></div><img id="comp-iuoizmpz2imageimageimage" alt="White Twitter Icon" data-type="image" style="width: 36px; height: 36px; object-fit: cover;" src="<?php echo get_template_directory_uri();?>/assets/img/01ab6619093f45388d66736ec22e5885.png"></div>
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                                <div class="txtNew" id="comp-j7tmgts0" style="left: 196px; width: 120px; position: absolute; top: 67px;">
                                    <h5 class="font_5" style="font-size:15px;"><span style="font-size:15px;"><span style="color:rgb(255, 255, 255); font-family:wfont_9012a2_6d1c84e5242d4b0e8c7bd107ccec04ac, wf_6d1c84e5242d4b0e8c7bd107c, orig_helvetica_bold; letter-spacing:0.08em;">SÍGUENOS</span></span></h5></div>
                                <div id="comp-j7tlsgzt" data-align="left" data-disabled="false" data-margin="0" data-should-use-flex="true" class="style-j7tluq6n" data-state="desktop shouldUseFlex left" style="left: 350px; position: absolute; top: 96px; height: 40px; min-height: 18px; width: 142px;" data-prev-text="Planes" data-prev-min-width="38" data-prev-width="142"><a href="" target="_self" data-keep-roots="true" data-anchor="c21z" role="button" id="comp-j7tlsgztlink" class="style-j7tluq6nlink"><span id="comp-j7tlsgztlabel" class="style-j7tluq6nlabel" style="margin-left: 0px;">Planes</span></a></div>
                                <div id="comp-j7tlvom7" data-align="left" data-disabled="false" data-margin="0" data-should-use-flex="true" class="style-j7tluq6n" data-state="desktop shouldUseFlex left" style="left: 350px; position: absolute; top: 138px; height: 40px; min-height: 18px; width: 142px;" data-prev-text="Reserva" data-prev-min-width="46" data-prev-width="142"><a href="/book-online" target="_self" role="button" id="comp-j7tlvom7link" class="style-j7tluq6nlink"><span id="comp-j7tlvom7label" class="style-j7tluq6nlabel" style="margin-left: 0px;">Reserva</span></a></div>
                                <div class="txtNew" id="comp-j7tmsaqh" style="left: 350px; width: 147px; position: absolute; top: 67px;">
                                    <h5 class="font_5" style="font-size:15px;"><span style="font-family:wfont_9012a2_6d1c84e5242d4b0e8c7bd107ccec04ac,wf_6d1c84e5242d4b0e8c7bd107c,orig_helvetica_bold;"><span style="font-size:15px;"><span style="color:rgb(255, 255, 255); letter-spacing:0.08em;">TUCASAOK</span></span></span></h5></div>
                                <div id="comp-j7tn7t4r" data-align="left" data-disabled="false" data-margin="0" data-should-use-flex="true" class="style-j7tluq6n" data-state="desktop shouldUseFlex left" style="left: 350px; position: absolute; top: 178px; height: 40px; min-height: 18px; width: 142px;" data-prev-text="Quienes somos" data-prev-min-width="87" data-prev-width="142"><a href="" target="_self" data-keep-roots="true" data-anchor="cal6" role="button" id="comp-j7tn7t4rlink" class="style-j7tluq6nlink"><span id="comp-j7tn7t4rlabel" class="style-j7tluq6nlabel" style="margin-left: 0px;">Quienes somos</span></a></div>
                                <div id="comp-j7tn8u9u" data-align="left" data-disabled="false" data-margin="0" data-should-use-flex="true" class="style-j7tluq6n" data-state="desktop shouldUseFlex left" style="left: 350px; position: absolute; top: 215px; height: 40px; min-height: 18px; width: 142px;" data-prev-text="Noticias" data-prev-min-width="47" data-prev-width="142"><a href="" target="_self" data-keep-roots="true" data-anchor="dataItem-iuk2rpbr" role="button" id="comp-j7tn8u9ulink" class="style-j7tluq6nlink"><span id="comp-j7tn8u9ulabel" class="style-j7tluq6nlabel" style="margin-left: 0px;">Noticias</span></a></div>
                                <div class="txtNew" id="comp-j7tml36b" style="left: 539px; width: 112px; position: absolute; top: 67px;">
                                    <h5 class="font_5" style="font-size:15px;"><span style="font-size:15px;"><span style="color:rgb(255, 255, 255); font-family:wfont_9012a2_6d1c84e5242d4b0e8c7bd107ccec04ac, wf_6d1c84e5242d4b0e8c7bd107c, orig_helvetica_bold; letter-spacing:0.08em;">AYUDA</span></span></h5></div>
                                <div id="comp-j7tlvp1y" data-align="left" data-disabled="false" data-margin="0" data-should-use-flex="true" class="style-j7tluq6n" data-state="desktop shouldUseFlex left" style="left: 539px; position: absolute; top: 178px; height: 40px; min-height: 18px; width: 142px;" data-prev-text="Blog" data-prev-min-width="24" data-prev-width="142"><a href="/blog" target="_self" role="button" id="comp-j7tlvp1ylink" class="style-j7tluq6nlink"><span id="comp-j7tlvp1ylabel" class="style-j7tluq6nlabel" style="margin-left: 0px;">Blog</span></a></div>
                                <div id="comp-j7tlvoe7" data-align="left" data-disabled="false" data-margin="0" data-should-use-flex="true" class="style-j7tluq6n" data-state="desktop shouldUseFlex left" style="left: 539px; position: absolute; top: 98px; height: 40px; min-height: 18px; width: 142px;" data-prev-text="Contacto" data-prev-min-width="53" data-prev-width="142"><a href="" target="_self" data-keep-roots="true" data-anchor="c18fq" role="button" id="comp-j7tlvoe7link" class="style-j7tluq6nlink"><span id="comp-j7tlvoe7label" class="style-j7tluq6nlabel" style="margin-left: 0px;">Contacto</span></a></div>
                                <div id="comp-j7tlvove" data-align="left" data-disabled="false" data-margin="0" data-should-use-flex="true" class="style-j7tm467w" data-state="desktop shouldUseFlex left" style="left: 539px; position: absolute; top: 138px; height: 40px; min-height: 18px; width: 123px;" data-prev-text="Preguntas frecuentes" data-prev-min-width="123" data-prev-width="123"><a href="/preguntas-frecuentes" target="_self" role="button" id="comp-j7tlvovelink" class="style-j7tm467wlink"><span id="comp-j7tlvovelabel" class="style-j7tm467wlabel" style="margin-left: 0px;">Preguntas frecuentes</span></a></div>
                                <div id="comp-j7tn5vpc" data-align="left" data-disabled="false" data-margin="0" data-should-use-flex="true" class="style-j7tm467w" data-state="desktop shouldUseFlex left" style="left: 539px; position: absolute; top: 215px; height: 40px; min-height: 18px; width: 96px;" data-prev-text="¿Cómo funciona?" data-prev-min-width="96" data-prev-width="96"><a href="/preguntas-frecuentes" target="_self" data-keep-roots="true" data-anchor="#dataItem-j7uzwac3" role="button" id="comp-j7tn5vpclink" class="style-j7tm467wlink"><span id="comp-j7tn5vpclabel" class="style-j7tm467wlabel" style="margin-left: 0px;">¿Cómo funciona?</span></a></div>
                                <div class="txtNew" id="comp-j7tmm7oz" style="left: 720px; width: 216px; position: absolute; top: 67px;">
                                    <h5 class="font_5" style="font-size:15px;"><span style="font-size:15px;"><span style="color:rgb(255, 255, 255); font-family:wfont_9012a2_6d1c84e5242d4b0e8c7bd107ccec04ac, wf_6d1c84e5242d4b0e8c7bd107c, orig_helvetica_bold; letter-spacing:0.08em;">ACÁ ESTAMOS</span></span></h5></div>
                                <div class="txtNew" id="comp-j7tndvkq" style="left: 720px; width: 239px; position: absolute; top: 145px;">
                                    <p class="font_8" style="font-size:13px; line-height:2em;"><span style="letter-spacing:0em;"><span style="font-size:13px;"><span style="font-family:wfont_9012a2_cf30a994f829458daa5b46a9a39e7eac,wf_cf30a994f829458daa5b46a9a,orig_cooperhewittbook;"><span style="color:#FFFFFF;"><object height="0"><a class="auto-generated-link" data-auto-recognition="true" data-content="contacto@tucasaok.cl" href="mailto:contacto@tucasaok.cl" data-type="mail">contacto@tucasaok.cl</a></object></span></span>
                                        </span>
                                        </span>
                                    </p>
                                </div>
                                <div id="comp-j7v075q4" data-align="left" data-disabled="false" data-margin="0" data-should-use-flex="true" class="style-j7v07ua7" data-state="desktop shouldUseFlex left" style="left: 720px; position: absolute; top: 98px; height: 40px; min-height: 18px; width: 142px;" data-prev-text="+56944720173" data-prev-min-width="94" data-prev-width="142"><a href="tel:+56944720173" data-content="+56944720173" data-type="phone" role="button" id="comp-j7v075q4link" class="style-j7v07ua7link"><span id="comp-j7v075q4label" class="style-j7v07ua7label" style="margin-left: 0px;">+56944720173</span></a></div>
                            </div>
                        </div>
                    </footer>
                    <div class="pc1" data-state="" id="PAGES_CONTAINER" style="width: 980px; position: absolute; top: 56px; height: 5134px; left: 0px;">
                        <div id="PAGES_CONTAINERscreenWidthBackground" class="pc1screenWidthBackground" style="width: 1349px; left: -185px;"></div>
                        <div id="PAGES_CONTAINERcenteredContent" class="pc1centeredContent">
                            <div id="PAGES_CONTAINERbg" class="pc1bg"></div>
                            <div id="PAGES_CONTAINERinlineContent" class="pc1inlineContent">
                                <div class="s_VOwPageGroupSkin" id="SITE_PAGES" style="left: 0px; width: 980px; position: absolute; top: 0px; height: 5134px;">
                                    <div class="p1" id="c1dmp" style="left: 0px; width: 980px; position: absolute; top: 0px; height: 5134px;">
                                        <div id="c1dmpbg" class="p1bg"></div>
                                        <div id="c1dmpinlineContent" class="p1inlineContent">
                                            <div role="region" aria-label="Slideshow" class="style-ja78349v" id="comp-j02kihhq" style="position: absolute; top: 0px; height: 463px; left: 0px; width: 980px;">
                                                <div id="comp-j02kihhqbg" class="style-ja78349vbg"></div>
                                                <div tabindex="-1" id="comp-j02kihhqinlineContentParent" class="style-ja78349vinlineContentParent" style="overflow: visible; left: -185px; width: 1349px;">
                                                    <div aria-live="polite" id="comp-j02kihhqinlineContent" class="style-ja78349vinlineContent" style="left: 185px; width: 980px;">
                                                        <div data-flexibleboxheight="false" data-parent-id="comp-j02kihhq" data-min-height="463" class="style-j94qfmna" id="comp-j94qfmlp" style="min-height: 463px; position: absolute; top: 0px; height: 463px; left: 0px; width: 980px; opacity: 1; visibility: inherit;">
                                                            <div data-enable-video="true" class="style-j94qfmnabalata" id="comp-j94qfmlpbalata" style="position: absolute; top: 0px; height: 100%; pointer-events: auto; overflow: hidden; left: -185px; width: 1349px; clip: rect(0px 1349px 463px 0px);">
                                                                <div class="bgColor" id="comp-j94qfmlpbalatabgcolor" style="position: absolute; width: 100%; height: 100%;">
                                                                    <div id="comp-j94qfmlpbalatabgcoloroverlay" class="bgColoroverlay" style="width: 100%; height: 100%; position: absolute; background-color: rgb(48, 48, 48);"></div>
                                                                </div>
                                                                <div data-effect="none" data-fitting="fill" data-align="center" class="bgMedia" id="comp-j94qfmlpbalatamedia" style="position: absolute; pointer-events: auto; top: 0px; width: 1349px; left: 0px; height: 463px;">
                                                                    <div data-type="image" data-style="" class="bgImage" id="comp-j94qfmlpbalatamediacontent" style="position: relative; width: 1349px; height: 463px;">
                                                                        <div class="bgImagepreloader" id="comp-j94qfmlpbalatamediacontentpreloader"></div><img id="comp-j94qfmlpbalatamediacontentimage" alt="" data-type="image" src="<?php echo get_template_directory_uri();?>/assets/img/9012a2_a3bc5d3bd9db4724b69e5c1610f672a5_mv2.png" style="width: 1349px; height: 463px; object-fit: cover;"></div>
                                                                </div>
                                                            </div>
                                                            <div id="comp-j94qfmlpborderNode" class="style-j94qfmnaborderNode"></div>
                                                            <div id="comp-j94qfmlpinlineContentParent" class="style-j94qfmnainlineContentParent" style="overflow: visible; left: -185px; width: 1349px;">
                                                                <div id="comp-j94qfmlpinlineContent" class="style-j94qfmnainlineContent" style="left: 185px; width: 980px;"></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div id="comp-j02kihhqshownOnAllSlides" class="style-ja78349vshownOnAllSlides"></div>
                                                <ol data-show-navigation-dots="true" id="comp-j02kihhqdotsMenuWrapper" class="style-ja78349vdotsMenuWrapper" style="bottom: 37px; justify-content: center;">
                                                    <li class="style-ja78349v_navigation-dot-wrapper">
                                                        <button class="style-ja78349v_navigation-dot reset-button" aria-label="Go to slide 1" style="width: 6px; height: 6px; margin-right: 6px; margin-left: 6px;"></button>
                                                    </li>
                                                    <li class="style-ja78349v_navigation-dot-wrapper">
                                                        <button class="style-ja78349v_navigation-dot style-ja78349v_selected reset-button" aria-label="Go to slide 2" style="width: 6px; height: 6px; margin-right: 6px; margin-left: 6px;"></button>
                                                    </li>
                                                    <li class="style-ja78349v_navigation-dot-wrapper">
                                                        <button class="style-ja78349v_navigation-dot reset-button" aria-label="Go to slide 3" style="width: 6px; height: 6px; margin-right: 6px; margin-left: 6px;"></button>
                                                    </li>
                                                    <li class="style-ja78349v_navigation-dot-wrapper">
                                                        <button class="style-ja78349v_navigation-dot reset-button" aria-label="Go to slide 4" style="width: 6px; height: 6px; margin-right: 6px; margin-left: 6px;"></button>
                                                    </li>
                                                    <li class="style-ja78349v_navigation-dot-wrapper">
                                                        <button class="style-ja78349v_navigation-dot reset-button" aria-label="Go to slide 5" style="width: 6px; height: 6px; margin-right: 6px; margin-left: 6px;"></button>
                                                    </li>
                                                    <li class="style-ja78349v_navigation-dot-wrapper">
                                                        <button class="style-ja78349v_navigation-dot reset-button" aria-label="Go to slide 6" style="width: 6px; height: 6px; margin-right: 6px; margin-left: 6px;"></button>
                                                    </li>
                                                    <li class="style-ja78349v_navigation-dot-wrapper">
                                                        <button class="style-ja78349v_navigation-dot reset-button" aria-label="Go to slide 7" style="width: 6px; height: 6px; margin-right: 6px; margin-left: 6px;"></button>
                                                    </li>
                                                </ol>
                                                <div data-show-navigation-arrows="true" data-navigation-button-margin="100" id="comp-j02kihhqnavigationArrows" class="style-ja78349v_navigation-arrows style-ja78349vnavigationArrows" style="top: calc(50% - 23px);">
                                                    <button title="next" class="reset-button style-ja78349v_btn style-ja78349vnextButton" id="comp-j02kihhqnextButton" style="width: 23px; right: -85px;">
                                                        <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 21 41" enable-background="new 0 0 21 41">
                                                            <polygon points="20.3,40.8 0,20.5 20.3,0.2 21,0.9 1.3,20.5 21,40.1 "></polygon>
                                                        </svg>
                                                    </button>
                                                    <button title="previous" class="reset-button style-ja78349v_btn style-ja78349vprevButton" id="comp-j02kihhqprevButton" style="width: 23px; left: -85px;">
                                                        <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 21 41" enable-background="new 0 0 21 41">
                                                            <polygon points="20.3,40.8 0,20.5 20.3,0.2 21,0.9 1.3,20.5 21,40.1 "></polygon>
                                                        </svg>
                                                    </button>
                                                </div>
                                            </div>
                                            <section class="strc1" id="comp-if6tyiyc" style="width: 980px; position: absolute; top: 1972px; height: 564px; left: 0px;">
                                                <div data-enable-video="true" class="strc1balata" id="comp-if6tyiycbalata" style="position: absolute; top: 0px; width: calc((100vw - 17px) - 0px); height: 100%; pointer-events: auto; bottom: 0px; left: calc(490px - (((100vw - 17px) - 0px) / 2)); right: calc(490px + (((100vw - 17px) - 0px) / 2)); overflow: hidden; clip: rect(0px 1349px 564px 0px);">
                                                    <div class="bgColor" id="comp-if6tyiycbalatabgcolor" style="position: absolute; width: 100%; height: 100%;">
                                                        <div id="comp-if6tyiycbalatabgcoloroverlay" class="bgColoroverlay" style="width: 100%; height: 100%; position: absolute; background-color: transparent;"></div>
                                                    </div>
                                                </div>
                                                <div id="comp-if6tyiycinlineContent" class="strc1inlineContent">
                                                    <div class="strc1" id="mediait1obida7" style="position: absolute; left: calc((((((100vw - 17px) - 0px) * 0.5) + ((980px - ((100vw - 17px) - 0px)) / 2)) + 0px) + -490px); width: 980px; top: 0px; height: 564px;">
                                                        <div data-enable-video="true" class="strc1balata" id="mediait1obida7balata" style="position: absolute; top: 0px; width: calc((((100vw - 17px) - 0px) * 1) + 1px); height: 100%; pointer-events: auto; left: calc((((((100vw - 17px) - 0px) * 1) - 980px) * -0.5) - 1px); bottom: 0px; overflow: hidden; clip: rect(0px 1350px 564px 0px);">
                                                            <div class="bgColor" id="mediait1obida7balatabgcolor" style="position: absolute; width: 100%; height: 100%;">
                                                                <div id="mediait1obida7balatabgcoloroverlay" class="bgColoroverlay" style="width: 100%; height: 100%; position: absolute; background-color: rgba(204, 204, 204, 0.2);"></div>
                                                            </div>
                                                        </div>
                                                        <div id="mediait1obida7inlineContent" class="strc1inlineContent" style="position: absolute; width: 980px; top: 0px; bottom: 0px; left: calc((100% - 980px) * 0.5);">
                                                            <div class="style-iuonx797" data-state="      desktop left" id="comp-iemp2qax" style="left: 268px; width: 446px; position: absolute; top: 121px; height: 354px;" data-dcf-columns="3">
                                                                <form role="form" aria-label="contact form" novalidate="" id="comp-iemp2qaxform-wrapper" class="style-iuonx797form-wrapper">
                                                                    <div id="comp-iemp2qaxwrapper" class="style-iuonx797wrapper">
                                                                        <div>
                                                                            <input type="text" id="field1" required="" aria-invalid="false" name="Nombre" value="" class="style-iuonx797_required" placeholder="Nombre" data-aid="nameField">
                                                                            <input type="text" id="field2" required="" aria-invalid="false" name="Email" value="" class="style-iuonx797_required" placeholder="Email" data-aid="emailField">
                                                                            <input type="tel" id="field3" aria-invalid="false" name="Teléfono" value="" class="" placeholder="Teléfono" data-aid="phoneField">
                                                                            <input type="text" id="field4" aria-invalid="false" name="Dirección y ciudad a inspeccionar" value="" class="" placeholder="Dirección y ciudad a inspeccionar" data-aid="addressField">
                                                                            <input type="text" id="field5" aria-invalid="false" name="m2 útiles y de terraza" value="" class="" placeholder="m2 útiles y de terraza" data-aid="subjectField">
                                                                        </div>
                                                                        <textarea name="¿Qué necesitas? Inspección casa nueva o usada." class="style-iuonx797fieldMessage" placeholder="¿Qué necesitas? Inspección casa nueva o usada." data-aid="messageField" id="comp-iemp2qaxfieldMessage"></textarea>
                                                                        <button type="submit" id="comp-iemp2qaxsubmit" class="style-iuonx797submit">Enviar</button><span aria-live="polite" class="style-iuonx797_success style-iuonx797notifications" id="comp-iemp2qaxnotifications"></span></div>
                                                                </form>
                                                            </div>
                                                            <div class="txtNew" id="iebd057y_1" style="left: 316px; width: 350px; position: absolute; top: 25px;">
                                                                <h2 class="font_2" style="text-align:center;"><span style="letter-spacing:0.1em;">RESERVA TU HORA / COTIZA</span></h2>
                                                            </div>
                                                            <div class="txtNew" id="iebd057y" style="left: 268px; width: 446px; position: absolute; top: 487px;">
                                                                <p class="font_8" style="font-size:13px; line-height:1.2em; text-align:center;"><span style="font-size:13px;"><span style="letter-spacing:0.1em;"><span class="color_14"><span style="font-family:wfont_9012a2_cf30a994f829458daa5b46a9a39e7eac,wf_cf30a994f829458daa5b46a9a,orig_cooperhewittbook;">Lo Matta 1647, Vitacura</span></span>
                                                                    </span>
                                                                    </span>
                                                                </p>

                                                                <p class="font_8" style="font-size:13px; line-height:1.2em; text-align:center;"><span style="font-size:13px;"><span style="letter-spacing:0.1em;"><span class="color_14"><span style="font-style:normal;"><span style="font-weight:normal;"><span style="font-family:wfont_9012a2_cf30a994f829458daa5b46a9a39e7eac,wf_cf30a994f829458daa5b46a9a,orig_cooperhewittbook;">Tel:&nbsp;+56 2 28911907</span></span>
                                                                    </span>
                                                                    </span>
                                                                    </span>
                                                                    </span>
                                                                </p>

                                                                <p class="font_8" style="font-size:13px; line-height:1.2em; text-align:center;"><span style="font-size:13px;"><span style="letter-spacing:0.1em;"><span class="color_14"><span style="font-style:normal;"><span style="font-weight:normal;"><span style="font-family:wfont_9012a2_cf30a994f829458daa5b46a9a39e7eac,wf_cf30a994f829458daa5b46a9a,orig_cooperhewittbook;">Cel: +56 9&nbsp;</span></span>
                                                                    </span><span style="font-family:wfont_9012a2_eab04d8add6b4f4eb1026227d1089542,wf_eab04d8add6b4f4eb1026227d,orig_montserratlight;">4472 0173</span><span style="font-style:normal;"><span style="font-weight:normal;"><span style="font-family:wfont_9012a2_cf30a994f829458daa5b46a9a39e7eac,wf_cf30a994f829458daa5b46a9a,orig_cooperhewittbook;"> </span></span>
                                                                    </span>
                                                                    </span>
                                                                    </span>
                                                                    </span>
                                                                </p>

                                                                <p class="font_8" style="font-size:13px; line-height:1.2em; text-align:center;"><span style="font-size:13px;"><span style="letter-spacing:0.1em;"><span class="color_14"><span style="font-family:wfont_9012a2_cf30a994f829458daa5b46a9a39e7eac,wf_cf30a994f829458daa5b46a9a,orig_cooperhewittbook;"><a href="mailto:contacto@tucasaok.cl" target="_self" data-content="contacto@tucasaok.cl" data-type="mail">contacto@tucasaok.cl</a></span></span>
                                                                    </span>
                                                                    </span>
                                                                </p>
                                                            </div>
                                                            <div class="txtNew" id="comp-iujrtcay" style="left: 268px; width: 446px; position: absolute; top: 45px;">
                                                                <p class="font_8" style="text-align:center;"><span class="wixGuard">&#8203;</span></p>

                                                                <p class="font_8" style="text-align:center;"><span style="font-family:wfont_9012a2_cf30a994f829458daa5b46a9a39e7eac,wf_cf30a994f829458daa5b46a9a,orig_cooperhewittbook;">Para recibir una cotización, por favor indícanos los metros cuadrados de la propiedad y la comuna donde se ubica.</span></p>

                                                                <p class="font_7" style="line-height:1.5em; text-align:center;"><span class="wixGuard">&#8203;</span></p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </section>
                                            <section class="strc1" id="comp-if6tnksz" style="width: 980px; position: absolute; top: 2536px; height: 496px; left: 0px;">
                                                <div data-enable-video="true" class="strc1balata" id="comp-if6tnkszbalata" style="position: absolute; top: 0px; width: calc((100vw - 17px) - 0px); height: 100%; pointer-events: auto; bottom: 0px; left: calc(490px - (((100vw - 17px) - 0px) / 2)); right: calc(490px + (((100vw - 17px) - 0px) / 2)); overflow: hidden; clip: rect(0px 1349px 496px 0px);">
                                                    <div class="bgColor" id="comp-if6tnkszbalatabgcolor" style="position: absolute; width: 100%; height: 100%;">
                                                        <div id="comp-if6tnkszbalatabgcoloroverlay" class="bgColoroverlay" style="width: 100%; height: 100%; position: absolute; background-color: transparent;"></div>
                                                    </div>
                                                </div>
                                                <div id="comp-if6tnkszinlineContent" class="strc1inlineContent">
                                                    <div class="strc1" id="mediait1obida13" style="position: absolute; left: calc((((((100vw - 17px) - 0px) * 0.5) + ((980px - ((100vw - 17px) - 0px)) / 2)) + 0px) + -490px); width: 980px; top: 0px; height: 496px;">
                                                        <div data-enable-video="true" class="strc1balata" id="mediait1obida13balata" style="position: absolute; top: 0px; width: calc((((100vw - 17px) - 0px) * 1) + 1px); height: 100%; pointer-events: auto; left: calc((((((100vw - 17px) - 0px) * 1) - 980px) * -0.5) - 1px); bottom: 0px; overflow: hidden; clip: rect(0px 1350px 496px 0px);">
                                                            <div class="bgColor" id="mediait1obida13balatabgcolor" style="position: absolute; width: 100%; height: 100%;">
                                                                <div id="mediait1obida13balatabgcoloroverlay" class="bgColoroverlay" style="width: 100%; height: 100%; position: absolute; background-color: rgb(255, 255, 255);"></div>
                                                            </div>
                                                            <div data-effect="none" data-fitting="tile" data-align="center" class="bgMedia" id="mediait1obida13balatamedia" style="position: absolute; pointer-events: auto; top: 0px; width: 1350px; left: 0px; height: 496px;">
                                                                <div class="bgImage" id="mediait1obida13balatamediacontent" style="width: 100%;">
                                                                    <div data-type="bgimage" id="mediait1obida13balatamediacontentimage" class="bgImageimage" style="position: absolute; width: 100%; opacity: 0.33; height: 496px; background-size: auto; background-repeat: repeat; background-position: center center; background-image: url(&quot;<?php echo get_template_directory_uri();?>/assets/img/38567b0b5eb55f8eba96f60fa6356630.png&quot;);" data-image-css="{&quot;height&quot;:496,&quot;backgroundSize&quot;:&quot;auto&quot;,&quot;backgroundRepeat&quot;:&quot;repeat&quot;,&quot;backgroundPosition&quot;:&quot;center center&quot;}"></div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div id="mediait1obida13inlineContent" class="strc1inlineContent" style="position: absolute; width: 980px; top: 0px; bottom: 0px; left: calc((100% - 980px) * 0.5);">
                                                            <div class="txtNew" id="iebcr8mn" style="left: 314px; width: 349px; position: absolute; top: 19px;">
                                                                <h2 class="font_2" style="text-align:center;"><span style="letter-spacing:0.1em;">EQUIPO</span></h2>
                                                            </div>
                                                            <div class="txtNew" id="iebcr8mo_0" style="left: 209px; width: 560px; position: absolute; top: 56px;">
                                                                <p class="font_8" style="line-height:1.4em; text-align:center;"><span style="letter-spacing:0.05em;"><span style="font-family:wfont_9012a2_cf30a994f829458daa5b46a9a39e7eac,wf_cf30a994f829458daa5b46a9a,orig_cooperhewittbook;">Nuestra empresa está compuesta por arquitectos y constructores con amplia experiencia en el rubro y todos buscamos lo mismo:&nbsp;Que te entreguen tu propiedad en perfectas condiciones</span>.</span>
                                                                </p>
                                                            </div>
                                                            <div data-exact-height="305" data-content-padding-horizontal="0" data-content-padding-vertical="0" title="Manuela y Paulina.jpg" class="wp2" id="comp-iujseygf" style="left: 48px; position: absolute; top: 124px; width: 365px; height: 305px;">
                                                                <div id="comp-iujseygflink" class="wp2link" style="width: 365px; height: 305px;">
                                                                    <div data-style="" class="wp2img" id="comp-iujseygfimg" style="position: relative; width: 365px; height: 305px;">
                                                                        <div class="wp2imgpreloader" id="comp-iujseygfimgpreloader"></div><img id="comp-iujseygfimgimage" alt="" data-type="image" style="width: 365px; height: 305px; object-fit: cover;" src="<?php echo get_template_directory_uri();?>/assets/img/9012a2_baf8483d896d40db8f352c55ca830465.webp"></div>
                                                                </div>
                                                            </div>
                                                            <div class="txtNew" id="comp-iujslm8f" style="left: 433px; width: 550px; position: absolute; top: 126px;">
                                                                <h5 class="font_5"><span style="font-family:wfont_9012a2_6d1c84e5242d4b0e8c7bd107ccec04ac,wf_6d1c84e5242d4b0e8c7bd107c,orig_helvetica_bold;">Manuela Maturana</span></h5>

                                                                <p class="font_8"><span style="font-family:wfont_9012a2_eab04d8add6b4f4eb1026227d1089542,wf_eab04d8add6b4f4eb1026227d,orig_montserratlight;">Socia Fundadora.</span></p>

                                                                <p class="font_8">&nbsp;</p>

                                                                <p class="font_8"><span style="font-family:wfont_9012a2_cf30a994f829458daa5b46a9a39e7eac,wf_cf30a994f829458daa5b46a9a,orig_cooperhewittbook;">Arquitecto Universidad del Desarrollo, Concepción.&nbsp;</span></p>

                                                                <p class="font_8"><span style="font-family:wfont_9012a2_cf30a994f829458daa5b46a9a39e7eac,wf_cf30a994f829458daa5b46a9a,orig_cooperhewittbook;">11 años de experiencia en el mercado inmobiliario e industrial, &nbsp;en áreas de&nbsp;construcción, inspección y post venta; además de diseño y coordinación de proyectos.</span></p>
                                                            </div>
                                                            <div class="txtNew" id="comp-iujsoqbi" style="left: 433px; width: 550px; position: absolute; top: 302px;">
                                                                <h5 class="font_5"><span style="font-family:wfont_9012a2_6d1c84e5242d4b0e8c7bd107ccec04ac,wf_6d1c84e5242d4b0e8c7bd107c,orig_helvetica_bold;">Paulina Salas</span></h5>

                                                                <p class="font_8"><span style="font-family:wfont_9012a2_eab04d8add6b4f4eb1026227d1089542,wf_eab04d8add6b4f4eb1026227d,orig_montserratlight;">Socia Fundadora.</span></p>

                                                                <p class="font_8"><span style="font-family:wfont_9012a2_eab04d8add6b4f4eb1026227d1089542,wf_eab04d8add6b4f4eb1026227d,orig_montserratlight;">&nbsp;</span></p>

                                                                <p class="font_8"><span style="font-family:wfont_9012a2_cf30a994f829458daa5b46a9a39e7eac,wf_cf30a994f829458daa5b46a9a,orig_cooperhewittbook;">Arquitecto&nbsp;Universidad Mayor de Santiago.</span></p>

                                                                <p class="font_8">10<span style="font-family:wfont_9012a2_cf30a994f829458daa5b46a9a39e7eac,wf_cf30a994f829458daa5b46a9a,orig_cooperhewittbook;"> años de&nbsp;experiencia en el mercado nacional e internacional desarrollando proyectos en el área inmobiliaria, hotelera, de retail e industrial.</span></p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </section>
                                            <section class="strc1" id="comp-if6tmdvp" style="width: 980px; position: absolute; top: 1500px; height: 834px; left: 0px;">
                                                <div class="cd-pricing-container cd-has-margins">
                                                    <div class="cd-pricing-switcher">
                                                        <p class="fieldset">
                                                            <input type="radio" name="duration-2" value="monthly" id="monthly-2" checked="">
                                                            <label for="monthly-2">Monthly</label>
                                                            <input type="radio" name="duration-2" value="yearly" id="yearly-2">
                                                            <label for="yearly-2">Yearly</label>
                                                            <span class="cd-switch"></span>
                                                        </p>
                                                    </div> <!-- .cd-pricing-switcher -->

                                                    <ul class="cd-pricing-list cd-bounce-invert">
                                                        <li>
                                                            <ul class="cd-pricing-wrapper">
                                                                <li data-type="monthly" class="is-visible is-ended">
                                                                    <header class="cd-pricing-header">
                                                                        <h2>Basic</h2>

                                                                        <div class="cd-price">
                                                                            <span class="cd-currency">$</span>
                                                                            <span class="cd-value">30</span>
                                                                            <span class="cd-duration">mo</span>
                                                                        </div>
                                                                    </header> <!-- .cd-pricing-header -->

                                                                    <div class="cd-pricing-body">
                                                                        <ul class="cd-pricing-features">
                                                                            <li><em>256MB</em> Memory</li>
                                                                            <li><em>1</em> User</li>
                                                                            <li><em>1</em> Website</li>
                                                                            <li><em>1</em> Domain</li>
                                                                            <li><em>Unlimited</em> Bandwidth</li>
                                                                            <li><em>24/7</em> Support</li>
                                                                        </ul>
                                                                    </div> <!-- .cd-pricing-body -->

                                                                    <footer class="cd-pricing-footer">
                                                                        <a class="cd-select" href="http://codyhouse.co/?p=429">Select</a>
                                                                    </footer>  <!-- .cd-pricing-footer -->
                                                                </li>

                                                                <li data-type="yearly" class="is-hidden is-ended">
                                                                    <header class="cd-pricing-header">
                                                                        <h2>Basic</h2>

                                                                        <div class="cd-price">
                                                                            <span class="cd-currency">$</span>
                                                                            <span class="cd-value">320</span>
                                                                            <span class="cd-duration">yr</span>
                                                                        </div>
                                                                    </header> <!-- .cd-pricing-header -->

                                                                    <div class="cd-pricing-body">
                                                                        <ul class="cd-pricing-features">
                                                                            <li><em>256MB</em> Memory</li>
                                                                            <li><em>1</em> User</li>
                                                                            <li><em>1</em> Website</li>
                                                                            <li><em>1</em> Domain</li>
                                                                            <li><em>Unlimited</em> Bandwidth</li>
                                                                            <li><em>24/7</em> Support</li>
                                                                        </ul>
                                                                    </div> <!-- .cd-pricing-body -->

                                                                    <footer class="cd-pricing-footer">
                                                                        <a class="cd-select" href="http://codyhouse.co/?p=429">Select</a>
                                                                    </footer>  <!-- .cd-pricing-footer -->
                                                                </li>
                                                            </ul> <!-- .cd-pricing-wrapper -->
                                                        </li>

                                                        <li class="cd-popular">
                                                            <ul class="cd-pricing-wrapper">
                                                                <li data-type="monthly" class="is-visible is-ended">
                                                                    <header class="cd-pricing-header">
                                                                        <h2>Popular</h2>
                                                                        <div class="cd-price">
                                                                            <span class="cd-currency">$</span>
                                                                            <span class="cd-value">60</span>
                                                                            <span class="cd-duration">mo</span>
                                                                        </div>
                                                                    </header> <!-- .cd-pricing-header -->

                                                                    <div class="cd-pricing-body">
                                                                        <ul class="cd-pricing-features">
                                                                            <li><em>512MB</em> Memory</li>
                                                                            <li><em>3</em> Users</li>
                                                                            <li><em>5</em> Websites</li>
                                                                            <li><em>7</em> Domains</li>
                                                                            <li><em>Unlimited</em> Bandwidth</li>
                                                                            <li><em>24/7</em> Support</li>
                                                                        </ul>
                                                                    </div> <!-- .cd-pricing-body -->

                                                                    <footer class="cd-pricing-footer">
                                                                        <a class="cd-select" href="http://codyhouse.co/?p=429">Select</a>
                                                                    </footer>  <!-- .cd-pricing-footer -->
                                                                </li>

                                                                <li data-type="yearly" class="is-hidden is-ended">
                                                                    <header class="cd-pricing-header">
                                                                        <h2>Popular</h2>

                                                                        <div class="cd-price">
                                                                            <span class="cd-currency">$</span>
                                                                            <span class="cd-value">630</span>
                                                                            <span class="cd-duration">yr</span>
                                                                        </div>
                                                                    </header> <!-- .cd-pricing-header -->

                                                                    <div class="cd-pricing-body">
                                                                        <ul class="cd-pricing-features">
                                                                            <li><em>512MB</em> Memory</li>
                                                                            <li><em>3</em> Users</li>
                                                                            <li><em>5</em> Websites</li>
                                                                            <li><em>7</em> Domains</li>
                                                                            <li><em>Unlimited</em> Bandwidth</li>
                                                                            <li><em>24/7</em> Support</li>
                                                                        </ul>
                                                                    </div> <!-- .cd-pricing-body -->

                                                                    <footer class="cd-pricing-footer">
                                                                        <a class="cd-select" href="http://codyhouse.co/?p=429">Select</a>
                                                                    </footer>  <!-- .cd-pricing-footer -->
                                                                </li>
                                                            </ul> <!-- .cd-pricing-wrapper -->
                                                        </li>

                                                        <li>
                                                            <ul class="cd-pricing-wrapper">
                                                                <li data-type="monthly" class="is-visible is-ended">
                                                                    <header class="cd-pricing-header">
                                                                        <h2>Premier</h2>

                                                                        <div class="cd-price">
                                                                            <span class="cd-currency">$</span>
                                                                            <span class="cd-value">90</span>
                                                                            <span class="cd-duration">mo</span>
                                                                        </div>
                                                                    </header> <!-- .cd-pricing-header -->

                                                                    <div class="cd-pricing-body">
                                                                        <ul class="cd-pricing-features">
                                                                            <li><em>1024MB</em> Memory</li>
                                                                            <li><em>5</em> Users</li>
                                                                            <li><em>10</em> Websites</li>
                                                                            <li><em>10</em> Domains</li>
                                                                            <li><em>Unlimited</em> Bandwidth</li>
                                                                            <li><em>24/7</em> Support</li>
                                                                        </ul>
                                                                    </div> <!-- .cd-pricing-body -->

                                                                    <footer class="cd-pricing-footer">
                                                                        <a class="cd-select" href="http://codyhouse.co/?p=429">Select</a>
                                                                    </footer>  <!-- .cd-pricing-footer -->
                                                                </li>

                                                                <li data-type="yearly" class="is-hidden is-ended">
                                                                    <header class="cd-pricing-header">
                                                                        <h2>Premier</h2>

                                                                        <div class="cd-price">
                                                                            <span class="cd-currency">$</span>
                                                                            <span class="cd-value">950</span>
                                                                            <span class="cd-duration">yr</span>
                                                                        </div>
                                                                    </header> <!-- .cd-pricing-header -->

                                                                    <div class="cd-pricing-body">
                                                                        <ul class="cd-pricing-features">
                                                                            <li><em>1024MB</em> Memory</li>
                                                                            <li><em>5</em> Users</li>
                                                                            <li><em>10</em> Websites</li>
                                                                            <li><em>10</em> Domains</li>
                                                                            <li><em>Unlimited</em> Bandwidth</li>
                                                                            <li><em>24/7</em> Support</li>
                                                                        </ul>
                                                                    </div> <!-- .cd-pricing-body -->

                                                                    <footer class="cd-pricing-footer">
                                                                        <a class="cd-select" href="http://codyhouse.co/?p=429">Select</a>
                                                                    </footer>  <!-- .cd-pricing-footer -->
                                                                </li>
                                                            </ul> <!-- .cd-pricing-wrapper -->
                                                        </li>
                                                    </ul> <!-- .cd-pricing-list -->
                                                </div>

                                            </section>
                                            <div class="Anchor_1" id="iebef3w2" style="left: 0px; width: 1920px; position: absolute; top: 1139px; height: 20px;"></div>
                                            <div class="Anchor_1" id="iebefvgs" style="left: 0px; width: 1920px; position: absolute; top: 2536px; height: 20px;"></div>
                                            <div class="Anchor_1" id="iecfudcd" style="left: 0px; width: 1920px; position: absolute; top: 1972px; height: 20px;"></div>
                                            <div class="Anchor_1" id="comp-iuk2rpaq" style="left: -469px; width: 1920px; position: absolute; top: 3047px; height: 21px;"></div>
                                            <div data-height-diff="40" data-width-diff="0" data-presented-row="6" class="style-iuoguloj" data-state="notMobile desktopView hiddenChildren" id="comp-iuk2u8c3" style="left: 0px; position: absolute; top: 3086px; height: 1572px; width: 980px;">
                                                <div id="comp-iuk2u8c3itemsContainer" class="style-iuogulojitemsContainer" style="height: 1572px; width: 980px;">
                                                    <div data-image-index="0" data-displayer-width="1028" data-displayer-height="1292" data-displayer-uri="9012a2_ddd4366521c841608f74336fa318f061_mv2.jpg" data-height-diff="78" data-width-diff="0" data-bottom-gap="0" data-image-wrapper-right="8" data-image-wrapper-left="8" data-image-wrapper-top="8" data-image-wrapper-bottom="8" data-margin-to-container="0" itemscope="" itemtype="http://schema.org/ImageObject" class="style-iuogulojimageItem" data-state="notShowPanel desktopView alignCenter unselected clipImage noTransition normal link" id="comp-iuk2u8c3dataItem-j9e7mmrg" style="position: absolute; height: 247px; width: 237px; left: 0px; top: 0px;">
                                                        <a draggable="false" data-page-item-context="galleryId:comp-iuk2u8c3 galleryCompId:comp-iuk2u8c3" data-gallery-id="comp-iuk2u8c3" href="?lightbox=dataItem-j9e7mmrg" target="_self" id="comp-iuk2u8c3dataItem-j9e7mmrglink" class="style-iuogulojimageItemlink" style="cursor: pointer; height: 100%; width: 100%; position: absolute; top: 0px; left: 0px; user-select: none; -webkit-user-drag: none; -webkit-user-modify: read-only;">
                                                            <div id="comp-iuk2u8c3dataItem-j9e7mmrgimageWrapper" class="style-iuogulojimageItemimageWrapper" style="height: 169px; width: 237px; margin: 0px;">
                                                                <div class="style-iuogulojimageItem_imgBorder">
                                                                    <div data-style="position:relative;overflow:hidden" class="style-iuogulojimageItemimage" id="comp-iuk2u8c3dataItem-j9e7mmrgimage" style="position: relative; width: 221px; height: 153px; overflow: hidden;">
                                                                        <div class="style-iuogulojimageItemimagepreloader" id="comp-iuk2u8c3dataItem-j9e7mmrgimagepreloader"></div><img id="comp-iuk2u8c3dataItem-j9e7mmrgimageimage" alt="TUCASAOK en LUN" data-type="image" itemprop="contentUrl" style="width: 221px; height: 153px; object-fit: cover;" src="<?php echo get_template_directory_uri();?>/assets/img/9012a2_ddd4366521c841608f74336fa318f061_mv2.webp"></div>
                                                                </div>
                                                                <div id="comp-iuk2u8c3dataItem-j9e7mmrgzoom" class="style-iuogulojimageItemzoom" style="cursor: pointer;"></div>
                                                            </div>
                                                        </a>
                                                        <div id="comp-iuk2u8c3dataItem-j9e7mmrgpanel" class="style-iuogulojimageItem_panel style-iuogulojimageItempanel">
                                                            <div class="style-iuogulojimageItem_panelWrap">
                                                                <h6 aria-hidden="true" itemprop="name" id="comp-iuk2u8c3dataItem-j9e7mmrgtitle" class="style-iuogulojimageItemtitle" style="text-align: center;">TUCASAOK en LUN</h6><span id="comp-iuk2u8c3dataItem-j9e7mmrgDescription" itemprop="description" class="style-iuogulojimageItemdescription" style="text-align: center;"><!-- react-text: 396 -->Enterate de los problemas y como te podemos ayudar en este reportaje! <!-- /react-text --><br><!-- react-text: 398 -->             Octubre 2017<!-- /react-text --></span></div>
                                                        </div>
                                                    </div>
                                                    <div data-image-index="1" data-displayer-width="1872" data-displayer-height="1048" data-displayer-uri="9012a2_163f7f00a71044d2b814ecc80ea5ed48_mv2.jpg" data-height-diff="78" data-width-diff="0" data-bottom-gap="0" data-image-wrapper-right="8" data-image-wrapper-left="8" data-image-wrapper-top="8" data-image-wrapper-bottom="8" data-margin-to-container="0" itemscope="" itemtype="http://schema.org/ImageObject" class="style-iuogulojimageItem" data-state="notShowPanel desktopView alignCenter unselected clipImage noTransition normal link" id="comp-iuk2u8c3dataItem-j7824jth" style="position: absolute; height: 247px; width: 237px; left: 247px; top: 0px;">
                                                        <a draggable="false" data-page-item-context="galleryId:comp-iuk2u8c3 galleryCompId:comp-iuk2u8c3" data-gallery-id="comp-iuk2u8c3" href="?lightbox=dataItem-j7824jth" target="_self" id="comp-iuk2u8c3dataItem-j7824jthlink" class="style-iuogulojimageItemlink" style="cursor: pointer; height: 100%; width: 100%; position: absolute; top: 0px; left: 0px; user-select: none; -webkit-user-drag: none; -webkit-user-modify: read-only;">
                                                            <div id="comp-iuk2u8c3dataItem-j7824jthimageWrapper" class="style-iuogulojimageItemimageWrapper" style="height: 169px; width: 237px; margin: 0px;">
                                                                <div class="style-iuogulojimageItem_imgBorder">
                                                                    <div data-style="position:relative;overflow:hidden" class="style-iuogulojimageItemimage" id="comp-iuk2u8c3dataItem-j7824jthimage" style="position: relative; width: 221px; height: 153px; overflow: hidden;">
                                                                        <div class="style-iuogulojimageItemimagepreloader" id="comp-iuk2u8c3dataItem-j7824jthimagepreloader"></div><img id="comp-iuk2u8c3dataItem-j7824jthimageimage" alt="TUCASAOK en Ahora Noticias" data-type="image" itemprop="contentUrl" style="width: 221px; height: 153px; object-fit: cover;" src="<?php echo get_template_directory_uri();?>/assets/img/9012a2_163f7f00a71044d2b814ecc80ea5ed48_mv2.webp"></div>
                                                                </div>
                                                                <div id="comp-iuk2u8c3dataItem-j7824jthzoom" class="style-iuogulojimageItemzoom" style="cursor: pointer;"></div>
                                                            </div>
                                                        </a>
                                                        <div id="comp-iuk2u8c3dataItem-j7824jthpanel" class="style-iuogulojimageItem_panel style-iuogulojimageItempanel">
                                                            <div class="style-iuogulojimageItem_panelWrap">
                                                                <h6 aria-hidden="true" itemprop="name" id="comp-iuk2u8c3dataItem-j7824jthtitle" class="style-iuogulojimageItemtitle" style="text-align: center;">TUCASAOK en Ahora Noticias</h6><span id="comp-iuk2u8c3dataItem-j7824jthDescription" itemprop="description" class="style-iuogulojimageItemdescription" style="text-align: center;"><!-- react-text: 411 -->Explicamos la importancia de revisar tu casa antes de habitarla.<!-- /react-text --><br><!-- react-text: 413 -->Agosto 2017<!-- /react-text --></span></div>
                                                        </div>
                                                    </div>
                                                    <div data-image-index="2" data-displayer-width="1140" data-displayer-height="936" data-displayer-uri="9012a2_81abb5fdba00466fac15e3fff25dac4d_mv2.jpg" data-height-diff="78" data-width-diff="0" data-bottom-gap="0" data-image-wrapper-right="8" data-image-wrapper-left="8" data-image-wrapper-top="8" data-image-wrapper-bottom="8" data-margin-to-container="0" itemscope="" itemtype="http://schema.org/ImageObject" class="style-iuogulojimageItem" data-state="notShowPanel desktopView alignCenter unselected clipImage noTransition normal link" id="comp-iuk2u8c3dataItem-j5hf8g7d" style="position: absolute; height: 247px; width: 237px; left: 494px; top: 0px;">
                                                        <a draggable="false" data-page-item-context="galleryId:comp-iuk2u8c3 galleryCompId:comp-iuk2u8c3" data-gallery-id="comp-iuk2u8c3" href="?lightbox=dataItem-j5hf8g7d" target="_self" id="comp-iuk2u8c3dataItem-j5hf8g7dlink" class="style-iuogulojimageItemlink" style="cursor: pointer; height: 100%; width: 100%; position: absolute; top: 0px; left: 0px; user-select: none; -webkit-user-drag: none; -webkit-user-modify: read-only;">
                                                            <div id="comp-iuk2u8c3dataItem-j5hf8g7dimageWrapper" class="style-iuogulojimageItemimageWrapper" style="height: 169px; width: 237px; margin: 0px;">
                                                                <div class="style-iuogulojimageItem_imgBorder">
                                                                    <div data-style="position:relative;overflow:hidden" class="style-iuogulojimageItemimage" id="comp-iuk2u8c3dataItem-j5hf8g7dimage" style="position: relative; width: 221px; height: 153px; overflow: hidden;">
                                                                        <div class="style-iuogulojimageItemimagepreloader" id="comp-iuk2u8c3dataItem-j5hf8g7dimagepreloader"></div><img id="comp-iuk2u8c3dataItem-j5hf8g7dimageimage" alt="Mujer del mes revista Velvet" data-type="image" itemprop="contentUrl" style="width: 221px; height: 153px; object-fit: cover;" src="<?php echo get_template_directory_uri();?>/assets/img/9012a2_81abb5fdba00466fac15e3fff25dac4d_mv2.webp"></div>
                                                                </div>
                                                                <div id="comp-iuk2u8c3dataItem-j5hf8g7dzoom" class="style-iuogulojimageItemzoom" style="cursor: pointer;"></div>
                                                            </div>
                                                        </a>
                                                        <div id="comp-iuk2u8c3dataItem-j5hf8g7dpanel" class="style-iuogulojimageItem_panel style-iuogulojimageItempanel">
                                                            <div class="style-iuogulojimageItem_panelWrap">
                                                                <h6 aria-hidden="true" itemprop="name" id="comp-iuk2u8c3dataItem-j5hf8g7dtitle" class="style-iuogulojimageItemtitle" style="text-align: center;">Mujer del mes revista Velvet</h6><span id="comp-iuk2u8c3dataItem-j5hf8g7dDescription" itemprop="description" class="style-iuogulojimageItemdescription" style="text-align: center;"><!-- react-text: 426 -->Reportaje a Manuela Maturana, Co fundadora de TUCASAOK<!-- /react-text --><br><!-- react-text: 428 -->Julio 2017<!-- /react-text --></span></div>
                                                        </div>
                                                    </div>
                                                    <div data-image-index="3" data-displayer-width="2028" data-displayer-height="2292" data-displayer-uri="9012a2_80feec4fd69649679ebf384820d40465_mv2_d_2028_2292_s_2.jpg" data-height-diff="78" data-width-diff="0" data-bottom-gap="0" data-image-wrapper-right="8" data-image-wrapper-left="8" data-image-wrapper-top="8" data-image-wrapper-bottom="8" data-margin-to-container="0" itemscope="" itemtype="http://schema.org/ImageObject" class="style-iuogulojimageItem" data-state="notShowPanel desktopView alignCenter unselected clipImage noTransition normal link" id="comp-iuk2u8c3dataItem-j3k9yp6m" style="position: absolute; height: 247px; width: 237px; left: 741px; top: 0px;">
                                                        <a draggable="false" data-page-item-context="galleryId:comp-iuk2u8c3 galleryCompId:comp-iuk2u8c3" data-gallery-id="comp-iuk2u8c3" href="?lightbox=dataItem-j3k9yp6m" target="_self" id="comp-iuk2u8c3dataItem-j3k9yp6mlink" class="style-iuogulojimageItemlink" style="cursor: pointer; height: 100%; width: 100%; position: absolute; top: 0px; left: 0px; user-select: none; -webkit-user-drag: none; -webkit-user-modify: read-only;">
                                                            <div id="comp-iuk2u8c3dataItem-j3k9yp6mimageWrapper" class="style-iuogulojimageItemimageWrapper" style="height: 169px; width: 237px; margin: 0px;">
                                                                <div class="style-iuogulojimageItem_imgBorder">
                                                                    <div data-style="position:relative;overflow:hidden" class="style-iuogulojimageItemimage" id="comp-iuk2u8c3dataItem-j3k9yp6mimage" style="position: relative; width: 221px; height: 153px; overflow: hidden;">
                                                                        <div class="style-iuogulojimageItemimagepreloader" id="comp-iuk2u8c3dataItem-j3k9yp6mimagepreloader"></div><img id="comp-iuk2u8c3dataItem-j3k9yp6mimageimage" alt="Propiedades de El Mercurio" data-type="image" itemprop="contentUrl" style="width: 221px; height: 153px; object-fit: cover;" src="<?php echo get_template_directory_uri();?>/assets/img/9012a2_80feec4fd69649679ebf384820d40465_mv2_d_2028_2292_s_2.webp"></div>
                                                                </div>
                                                                <div id="comp-iuk2u8c3dataItem-j3k9yp6mzoom" class="style-iuogulojimageItemzoom" style="cursor: pointer;"></div>
                                                            </div>
                                                        </a>
                                                        <div id="comp-iuk2u8c3dataItem-j3k9yp6mpanel" class="style-iuogulojimageItem_panel style-iuogulojimageItempanel">
                                                            <div class="style-iuogulojimageItem_panelWrap">
                                                                <h6 aria-hidden="true" itemprop="name" id="comp-iuk2u8c3dataItem-j3k9yp6mtitle" class="style-iuogulojimageItemtitle" style="text-align: center;">Propiedades de El Mercurio</h6><span id="comp-iuk2u8c3dataItem-j3k9yp6mDescription" itemprop="description" class="style-iuogulojimageItemdescription" style="text-align: center;">TUCASAOK destaca frente a la competencia en profesionalismo, tecnología, experiencia y respuesta de los clientes. Junio 2017</span></div>
                                                        </div>
                                                    </div>
                                                    <div data-image-index="4" data-displayer-width="3119" data-displayer-height="1680" data-displayer-uri="9012a2_08694222ffdf4edb98b7c0a47f3935fc_mv2_d_3119_1680_s_2.jpg" data-height-diff="78" data-width-diff="0" data-bottom-gap="0" data-image-wrapper-right="8" data-image-wrapper-left="8" data-image-wrapper-top="8" data-image-wrapper-bottom="8" data-margin-to-container="0" itemscope="" itemtype="http://schema.org/ImageObject" class="style-iuogulojimageItem" data-state="notShowPanel desktopView alignCenter unselected clipImage noTransition normal link" id="comp-iuk2u8c3dataItem-j2t4of4z" style="position: absolute; height: 247px; width: 237px; left: 0px; top: 257px;">
                                                        <a draggable="false" data-page-item-context="galleryId:comp-iuk2u8c3 galleryCompId:comp-iuk2u8c3" data-gallery-id="comp-iuk2u8c3" href="?lightbox=dataItem-j2t4of4z" target="_self" id="comp-iuk2u8c3dataItem-j2t4of4zlink" class="style-iuogulojimageItemlink" style="cursor: pointer; height: 100%; width: 100%; position: absolute; top: 0px; left: 0px; user-select: none; -webkit-user-drag: none; -webkit-user-modify: read-only;">
                                                            <div id="comp-iuk2u8c3dataItem-j2t4of4zimageWrapper" class="style-iuogulojimageItemimageWrapper" style="height: 169px; width: 237px; margin: 0px;">
                                                                <div class="style-iuogulojimageItem_imgBorder">
                                                                    <div data-style="position:relative;overflow:hidden" class="style-iuogulojimageItemimage" id="comp-iuk2u8c3dataItem-j2t4of4zimage" style="position: relative; width: 221px; height: 153px; overflow: hidden;">
                                                                        <div class="style-iuogulojimageItemimagepreloader" id="comp-iuk2u8c3dataItem-j2t4of4zimagepreloader"></div><img id="comp-iuk2u8c3dataItem-j2t4of4zimageimage" alt="Web serie EL GÜEN DATO" data-type="image" itemprop="contentUrl" style="width: 221px; height: 153px; object-fit: cover;" src="<?php echo get_template_directory_uri();?>/assets/img/9012a2_08694222ffdf4edb98b7c0a47f3935fc_mv2_d_3119_1680_s_2.webp"></div>
                                                                </div>
                                                                <div id="comp-iuk2u8c3dataItem-j2t4of4zzoom" class="style-iuogulojimageItemzoom" style="cursor: pointer;"></div>
                                                            </div>
                                                        </a>
                                                        <div id="comp-iuk2u8c3dataItem-j2t4of4zpanel" class="style-iuogulojimageItem_panel style-iuogulojimageItempanel">
                                                            <div class="style-iuogulojimageItem_panelWrap">
                                                                <h6 aria-hidden="true" itemprop="name" id="comp-iuk2u8c3dataItem-j2t4of4ztitle" class="style-iuogulojimageItemtitle" style="text-align: center;">Web serie EL GÜEN DATO</h6><span id="comp-iuk2u8c3dataItem-j2t4of4zDescription" itemprop="description" class="style-iuogulojimageItemdescription" style="text-align: center;"><!-- react-text: 453 -->Espacio donde emprendedores cuentan sobre su proyecto.<!-- /react-text --><br><!-- react-text: 455 -->Mayo 2017<!-- /react-text --></span></div>
                                                        </div>
                                                    </div>
                                                    <div data-image-index="5" data-displayer-width="1434" data-displayer-height="1136" data-displayer-uri="9012a2_82c0faa90f9445a19c408aa82a17d40d_mv2.jpg" data-height-diff="78" data-width-diff="0" data-bottom-gap="0" data-image-wrapper-right="8" data-image-wrapper-left="8" data-image-wrapper-top="8" data-image-wrapper-bottom="8" data-margin-to-container="0" itemscope="" itemtype="http://schema.org/ImageObject" class="style-iuogulojimageItem" data-state="notShowPanel desktopView alignCenter unselected clipImage noTransition normal link" id="comp-iuk2u8c3dataItem-j2hn0k9c" style="position: absolute; height: 247px; width: 237px; left: 247px; top: 257px;">
                                                        <a draggable="false" data-page-item-context="galleryId:comp-iuk2u8c3 galleryCompId:comp-iuk2u8c3" data-gallery-id="comp-iuk2u8c3" href="?lightbox=dataItem-j2hn0k9c" target="_self" id="comp-iuk2u8c3dataItem-j2hn0k9clink" class="style-iuogulojimageItemlink" style="cursor: pointer; height: 100%; width: 100%; position: absolute; top: 0px; left: 0px; user-select: none; -webkit-user-drag: none; -webkit-user-modify: read-only;">
                                                            <div id="comp-iuk2u8c3dataItem-j2hn0k9cimageWrapper" class="style-iuogulojimageItemimageWrapper" style="height: 169px; width: 237px; margin: 0px;">
                                                                <div class="style-iuogulojimageItem_imgBorder">
                                                                    <div data-style="position:relative;overflow:hidden" class="style-iuogulojimageItemimage" id="comp-iuk2u8c3dataItem-j2hn0k9cimage" style="position: relative; width: 221px; height: 153px; overflow: hidden;">
                                                                        <div class="style-iuogulojimageItemimagepreloader" id="comp-iuk2u8c3dataItem-j2hn0k9cimagepreloader"></div><img id="comp-iuk2u8c3dataItem-j2hn0k9cimageimage" alt="Dc. Pyme en Emol." data-type="image" itemprop="contentUrl" style="width: 221px; height: 153px; object-fit: cover;" src="<?php echo get_template_directory_uri();?>/assets/img/9012a2_82c0faa90f9445a19c408aa82a17d40d_mv2.webp"></div>
                                                                </div>
                                                                <div id="comp-iuk2u8c3dataItem-j2hn0k9czoom" class="style-iuogulojimageItemzoom" style="cursor: pointer;"></div>
                                                            </div>
                                                        </a>
                                                        <div id="comp-iuk2u8c3dataItem-j2hn0k9cpanel" class="style-iuogulojimageItem_panel style-iuogulojimageItempanel">
                                                            <div class="style-iuogulojimageItem_panelWrap">
                                                                <h6 aria-hidden="true" itemprop="name" id="comp-iuk2u8c3dataItem-j2hn0k9ctitle" class="style-iuogulojimageItemtitle" style="text-align: center;">Dc. Pyme en Emol.</h6><span id="comp-iuk2u8c3dataItem-j2hn0k9cDescription" itemprop="description" class="style-iuogulojimageItemdescription" style="text-align: center;">Nuestra experiencia emprendiendo. Camino largo y complicado, pero sumamente satisfactorio. 8 Mayo 2017</span></div>
                                                        </div>
                                                    </div>
                                                    <div data-image-index="6" data-displayer-width="1442" data-displayer-height="1496" data-displayer-uri="9012a2_8116cd8e83eb45158d223537f05482db_mv2_d_1442_1496_s_2.jpg" data-height-diff="78" data-width-diff="0" data-bottom-gap="0" data-image-wrapper-right="8" data-image-wrapper-left="8" data-image-wrapper-top="8" data-image-wrapper-bottom="8" data-margin-to-container="0" itemscope="" itemtype="http://schema.org/ImageObject" class="style-iuogulojimageItem" data-state="notShowPanel desktopView alignCenter unselected clipImage noTransition normal link" id="comp-iuk2u8c3dataItem-j1ghxk9c" style="position: absolute; height: 247px; width: 237px; left: 494px; top: 257px;">
                                                        <a draggable="false" data-page-item-context="galleryId:comp-iuk2u8c3 galleryCompId:comp-iuk2u8c3" data-gallery-id="comp-iuk2u8c3" href="?lightbox=dataItem-j1ghxk9c" target="_self" id="comp-iuk2u8c3dataItem-j1ghxk9clink" class="style-iuogulojimageItemlink" style="cursor: pointer; height: 100%; width: 100%; position: absolute; top: 0px; left: 0px; user-select: none; -webkit-user-drag: none; -webkit-user-modify: read-only;">
                                                            <div id="comp-iuk2u8c3dataItem-j1ghxk9cimageWrapper" class="style-iuogulojimageItemimageWrapper" style="height: 169px; width: 237px; margin: 0px;">
                                                                <div class="style-iuogulojimageItem_imgBorder">
                                                                    <div data-style="position:relative;overflow:hidden" class="style-iuogulojimageItemimage" id="comp-iuk2u8c3dataItem-j1ghxk9cimage" style="position: relative; width: 221px; height: 153px; overflow: hidden;">
                                                                        <div class="style-iuogulojimageItemimagepreloader" id="comp-iuk2u8c3dataItem-j1ghxk9cimagepreloader"></div><img id="comp-iuk2u8c3dataItem-j1ghxk9cimageimage" alt="Modelo de negocio de TUCASAOK Emol" data-type="image" itemprop="contentUrl" style="width: 221px; height: 153px; object-fit: cover;" src="<?php echo get_template_directory_uri();?>/assets/img/9012a2_8116cd8e83eb45158d223537f05482db_mv2_d_1442_1496_s_2.webp"></div>
                                                                </div>
                                                                <div id="comp-iuk2u8c3dataItem-j1ghxk9czoom" class="style-iuogulojimageItemzoom" style="cursor: pointer;"></div>
                                                            </div>
                                                        </a>
                                                        <div id="comp-iuk2u8c3dataItem-j1ghxk9cpanel" class="style-iuogulojimageItem_panel style-iuogulojimageItempanel">
                                                            <div class="style-iuogulojimageItem_panelWrap">
                                                                <h6 aria-hidden="true" itemprop="name" id="comp-iuk2u8c3dataItem-j1ghxk9ctitle" class="style-iuogulojimageItemtitle" style="text-align: center;">Modelo de negocio de TUCASAOK Emol</h6><span id="comp-iuk2u8c3dataItem-j1ghxk9cDescription" itemprop="description" class="style-iuogulojimageItemdescription" style="text-align: center;"><!-- react-text: 480 -->TUCASAOK a la vanguardia, conoce aquí por qué?.<!-- /react-text --><br><!-- react-text: 482 -->11 Abril 2017<!-- /react-text --></span></div>
                                                        </div>
                                                    </div>
                                                    <div data-image-index="7" data-displayer-width="2451" data-displayer-height="1455" data-displayer-uri="9012a2_f5950eccf59141b9878ecb61d2e01d32_mv2_d_2451_1455_s_2.jpg" data-height-diff="78" data-width-diff="0" data-bottom-gap="0" data-image-wrapper-right="8" data-image-wrapper-left="8" data-image-wrapper-top="8" data-image-wrapper-bottom="8" data-margin-to-container="0" itemscope="" itemtype="http://schema.org/ImageObject" class="style-iuogulojimageItem" data-state="notShowPanel desktopView alignCenter unselected clipImage noTransition normal link" id="comp-iuk2u8c3dataItem-j1ghsem7" style="position: absolute; height: 247px; width: 237px; left: 741px; top: 257px;">
                                                        <a draggable="false" data-page-item-context="galleryId:comp-iuk2u8c3 galleryCompId:comp-iuk2u8c3" data-gallery-id="comp-iuk2u8c3" href="?lightbox=dataItem-j1ghsem7" target="_self" id="comp-iuk2u8c3dataItem-j1ghsem7link" class="style-iuogulojimageItemlink" style="cursor: pointer; height: 100%; width: 100%; position: absolute; top: 0px; left: 0px; user-select: none; -webkit-user-drag: none; -webkit-user-modify: read-only;">
                                                            <div id="comp-iuk2u8c3dataItem-j1ghsem7imageWrapper" class="style-iuogulojimageItemimageWrapper" style="height: 169px; width: 237px; margin: 0px;">
                                                                <div class="style-iuogulojimageItem_imgBorder">
                                                                    <div data-style="position:relative;overflow:hidden" class="style-iuogulojimageItemimage" id="comp-iuk2u8c3dataItem-j1ghsem7image" style="position: relative; width: 221px; height: 153px; overflow: hidden;">
                                                                        <div class="style-iuogulojimageItemimagepreloader" id="comp-iuk2u8c3dataItem-j1ghsem7imagepreloader"></div><img id="comp-iuk2u8c3dataItem-j1ghsem7imageimage" alt="Reportaje en LUN" data-type="image" itemprop="contentUrl" style="width: 221px; height: 153px; object-fit: cover;" src="<?php echo get_template_directory_uri();?>/assets/img/9012a2_f5950eccf59141b9878ecb61d2e01d32_mv2_d_2451_1455_s_2.webp"></div>
                                                                </div>
                                                                <div id="comp-iuk2u8c3dataItem-j1ghsem7zoom" class="style-iuogulojimageItemzoom" style="cursor: pointer;"></div>
                                                            </div>
                                                        </a>
                                                        <div id="comp-iuk2u8c3dataItem-j1ghsem7panel" class="style-iuogulojimageItem_panel style-iuogulojimageItempanel">
                                                            <div class="style-iuogulojimageItem_panelWrap">
                                                                <h6 aria-hidden="true" itemprop="name" id="comp-iuk2u8c3dataItem-j1ghsem7title" class="style-iuogulojimageItemtitle" style="text-align: center;">Reportaje en LUN</h6><span id="comp-iuk2u8c3dataItem-j1ghsem7Description" itemprop="description" class="style-iuogulojimageItemdescription" style="text-align: center;"><!-- react-text: 495 -->Calendario para que mantengas tu hogar en perfectas condiciones.<!-- /react-text --><br><!-- react-text: 497 -->8 Abril 2017<!-- /react-text --></span></div>
                                                        </div>
                                                    </div>
                                                    <div data-image-index="8" data-displayer-width="600" data-displayer-height="344" data-displayer-uri="9012a2_a6ae356685c8493ebe483cf33512cb04_mv2.jpg" data-height-diff="78" data-width-diff="0" data-bottom-gap="0" data-image-wrapper-right="8" data-image-wrapper-left="8" data-image-wrapper-top="8" data-image-wrapper-bottom="8" data-margin-to-container="0" itemscope="" itemtype="http://schema.org/ImageObject" class="style-iuogulojimageItem" data-state="notShowPanel desktopView alignCenter unselected clipImage noTransition normal link" id="comp-iuk2u8c3dataItem-j02ivurv" style="position: absolute; height: 247px; width: 237px; left: 0px; top: 514px;">
                                                        <a draggable="false" data-page-item-context="galleryId:comp-iuk2u8c3 galleryCompId:comp-iuk2u8c3" data-gallery-id="comp-iuk2u8c3" href="?lightbox=dataItem-j02ivurv" target="_self" id="comp-iuk2u8c3dataItem-j02ivurvlink" class="style-iuogulojimageItemlink" style="cursor: pointer; height: 100%; width: 100%; position: absolute; top: 0px; left: 0px; user-select: none; -webkit-user-drag: none; -webkit-user-modify: read-only;">
                                                            <div id="comp-iuk2u8c3dataItem-j02ivurvimageWrapper" class="style-iuogulojimageItemimageWrapper" style="height: 169px; width: 237px; margin: 0px;">
                                                                <div class="style-iuogulojimageItem_imgBorder">
                                                                    <div data-style="position:relative;overflow:hidden" class="style-iuogulojimageItemimage" id="comp-iuk2u8c3dataItem-j02ivurvimage" style="position: relative; width: 221px; height: 153px; overflow: hidden;">
                                                                        <div class="style-iuogulojimageItemimagepreloader" id="comp-iuk2u8c3dataItem-j02ivurvimagepreloader"></div><img id="comp-iuk2u8c3dataItem-j02ivurvimageimage" alt="¡Felicidades a Nosotras!" data-type="image" itemprop="contentUrl" style="width: 221px; height: 153px; object-fit: cover;" src="<?php echo get_template_directory_uri();?>/assets/img/9012a2_a6ae356685c8493ebe483cf33512cb04_mv2.webp"></div>
                                                                </div>
                                                                <div id="comp-iuk2u8c3dataItem-j02ivurvzoom" class="style-iuogulojimageItemzoom" style="cursor: pointer;"></div>
                                                            </div>
                                                        </a>
                                                        <div id="comp-iuk2u8c3dataItem-j02ivurvpanel" class="style-iuogulojimageItem_panel style-iuogulojimageItempanel">
                                                            <div class="style-iuogulojimageItem_panelWrap">
                                                                <h6 aria-hidden="true" itemprop="name" id="comp-iuk2u8c3dataItem-j02ivurvtitle" class="style-iuogulojimageItemtitle" style="text-align: center;">¡Felicidades a Nosotras!</h6><span id="comp-iuk2u8c3dataItem-j02ivurvDescription" itemprop="description" class="style-iuogulojimageItemdescription" style="text-align: center;">“Chile está lleno de mujeres talentosas, quienes con sus ideas y proyectos pueden proponer una nueva forma de hacer País”.</span></div>
                                                        </div>
                                                    </div>
                                                    <div data-image-index="9" data-displayer-width="2550" data-displayer-height="1651" data-displayer-uri="9012a2_3cf71792ea5648d2b341d039102beb9b_mv2_d_2550_1651_s_2.jpg" data-height-diff="78" data-width-diff="0" data-bottom-gap="0" data-image-wrapper-right="8" data-image-wrapper-left="8" data-image-wrapper-top="8" data-image-wrapper-bottom="8" data-margin-to-container="0" itemscope="" itemtype="http://schema.org/ImageObject" class="style-iuogulojimageItem" data-state="notShowPanel desktopView alignCenter unselected clipImage noTransition normal link" id="comp-iuk2u8c3dataItem-j02in831" style="position: absolute; height: 247px; width: 237px; left: 247px; top: 514px;">
                                                        <a draggable="false" data-page-item-context="galleryId:comp-iuk2u8c3 galleryCompId:comp-iuk2u8c3" data-gallery-id="comp-iuk2u8c3" href="?lightbox=dataItem-j02in831" target="_self" id="comp-iuk2u8c3dataItem-j02in831link" class="style-iuogulojimageItemlink" style="cursor: pointer; height: 100%; width: 100%; position: absolute; top: 0px; left: 0px; user-select: none; -webkit-user-drag: none; -webkit-user-modify: read-only;">
                                                            <div id="comp-iuk2u8c3dataItem-j02in831imageWrapper" class="style-iuogulojimageItemimageWrapper" style="height: 169px; width: 237px; margin: 0px;">
                                                                <div class="style-iuogulojimageItem_imgBorder">
                                                                    <div data-style="position:relative;overflow:hidden" class="style-iuogulojimageItemimage" id="comp-iuk2u8c3dataItem-j02in831image" style="position: relative; width: 221px; height: 153px; overflow: hidden;">
                                                                        <div class="style-iuogulojimageItemimagepreloader" id="comp-iuk2u8c3dataItem-j02in831imagepreloader"></div><img id="comp-iuk2u8c3dataItem-j02in831imageimage" alt="Reportaje revista VELVET" data-type="image" itemprop="contentUrl" style="width: 221px; height: 153px; object-fit: cover;" src="<?php echo get_template_directory_uri();?>/assets/img/9012a2_3cf71792ea5648d2b341d039102beb9b_mv2_d_2550_1651_s_2.webp"></div>
                                                                </div>
                                                                <div id="comp-iuk2u8c3dataItem-j02in831zoom" class="style-iuogulojimageItemzoom" style="cursor: pointer;"></div>
                                                            </div>
                                                        </a>
                                                        <div id="comp-iuk2u8c3dataItem-j02in831panel" class="style-iuogulojimageItem_panel style-iuogulojimageItempanel">
                                                            <div class="style-iuogulojimageItem_panelWrap">
                                                                <h6 aria-hidden="true" itemprop="name" id="comp-iuk2u8c3dataItem-j02in831title" class="style-iuogulojimageItemtitle" style="text-align: center;">Reportaje revista VELVET</h6><span id="comp-iuk2u8c3dataItem-j02in831Description" itemprop="description" class="style-iuogulojimageItemdescription" style="text-align: center;"><!-- react-text: 522 -->Comenzamos con el pie derecho en Conce, y acá contamos nuestra experiencia.<!-- /react-text --><br><!-- react-text: 524 -->Marzo 2017<!-- /react-text --></span></div>
                                                        </div>
                                                    </div>
                                                    <div data-image-index="10" data-displayer-width="1276" data-displayer-height="629" data-displayer-uri="9012a2_82094904b3f64fbe94c0c28cc3538632_mv2.jpg" data-height-diff="78" data-width-diff="0" data-bottom-gap="0" data-image-wrapper-right="8" data-image-wrapper-left="8" data-image-wrapper-top="8" data-image-wrapper-bottom="8" data-margin-to-container="0" itemscope="" itemtype="http://schema.org/ImageObject" class="style-iuogulojimageItem" data-state="notShowPanel desktopView alignCenter unselected clipImage noTransition normal link" id="comp-iuk2u8c3dataItem-iy4fsxuo" style="position: absolute; height: 247px; width: 237px; left: 494px; top: 514px;">
                                                        <a draggable="false" data-page-item-context="galleryId:comp-iuk2u8c3 galleryCompId:comp-iuk2u8c3" data-gallery-id="comp-iuk2u8c3" href="?lightbox=dataItem-iy4fsxuo" target="_self" id="comp-iuk2u8c3dataItem-iy4fsxuolink" class="style-iuogulojimageItemlink" style="cursor: pointer; height: 100%; width: 100%; position: absolute; top: 0px; left: 0px; user-select: none; -webkit-user-drag: none; -webkit-user-modify: read-only;">
                                                            <div id="comp-iuk2u8c3dataItem-iy4fsxuoimageWrapper" class="style-iuogulojimageItemimageWrapper" style="height: 169px; width: 237px; margin: 0px;">
                                                                <div class="style-iuogulojimageItem_imgBorder">
                                                                    <div data-style="position:relative;overflow:hidden" class="style-iuogulojimageItemimage" id="comp-iuk2u8c3dataItem-iy4fsxuoimage" style="position: relative; width: 221px; height: 153px; overflow: hidden;">
                                                                        <div class="style-iuogulojimageItemimagepreloader" id="comp-iuk2u8c3dataItem-iy4fsxuoimagepreloader"></div><img id="comp-iuk2u8c3dataItem-iy4fsxuoimageimage" alt="Entrevista TVU Concepción" data-type="image" itemprop="contentUrl" style="width: 221px; height: 153px; object-fit: cover;" src="<?php echo get_template_directory_uri();?>/assets/img/9012a2_82094904b3f64fbe94c0c28cc3538632_mv2.webp"></div>
                                                                </div>
                                                                <div id="comp-iuk2u8c3dataItem-iy4fsxuozoom" class="style-iuogulojimageItemzoom" style="cursor: pointer;"></div>
                                                            </div>
                                                        </a>
                                                        <div id="comp-iuk2u8c3dataItem-iy4fsxuopanel" class="style-iuogulojimageItem_panel style-iuogulojimageItempanel">
                                                            <div class="style-iuogulojimageItem_panelWrap">
                                                                <h6 aria-hidden="true" itemprop="name" id="comp-iuk2u8c3dataItem-iy4fsxuotitle" class="style-iuogulojimageItemtitle" style="text-align: center;">Entrevista TVU Concepción</h6><span id="comp-iuk2u8c3dataItem-iy4fsxuoDescription" itemprop="description" class="style-iuogulojimageItemdescription" style="text-align: center;"><!-- react-text: 537 -->Conversamos sobre tips, asesorías y mucha información relevante para recibir tu casa.<!-- /react-text --><br><!-- react-text: 539 -->16 de Enero 2017<!-- /react-text --></span></div>
                                                        </div>
                                                    </div>
                                                    <div data-image-index="11" data-displayer-width="1147" data-displayer-height="1014" data-displayer-uri="9012a2_8fe7167966da475383409d2e972c4931_mv2.jpg" data-height-diff="78" data-width-diff="0" data-bottom-gap="0" data-image-wrapper-right="8" data-image-wrapper-left="8" data-image-wrapper-top="8" data-image-wrapper-bottom="8" data-margin-to-container="0" itemscope="" itemtype="http://schema.org/ImageObject" class="style-iuogulojimageItem" data-state="notShowPanel desktopView alignCenter unselected clipImage noTransition normal link" id="comp-iuk2u8c3dataItem-ixrx96fl" style="position: absolute; height: 247px; width: 237px; left: 741px; top: 514px;">
                                                        <a draggable="false" data-page-item-context="galleryId:comp-iuk2u8c3 galleryCompId:comp-iuk2u8c3" data-gallery-id="comp-iuk2u8c3" href="?lightbox=dataItem-ixrx96fl" target="_self" id="comp-iuk2u8c3dataItem-ixrx96fllink" class="style-iuogulojimageItemlink" style="cursor: pointer; height: 100%; width: 100%; position: absolute; top: 0px; left: 0px; user-select: none; -webkit-user-drag: none; -webkit-user-modify: read-only;">
                                                            <div id="comp-iuk2u8c3dataItem-ixrx96flimageWrapper" class="style-iuogulojimageItemimageWrapper" style="height: 169px; width: 237px; margin: 0px;">
                                                                <div class="style-iuogulojimageItem_imgBorder">
                                                                    <div data-style="position:relative;overflow:hidden" class="style-iuogulojimageItemimage" id="comp-iuk2u8c3dataItem-ixrx96flimage" style="position: relative; width: 221px; height: 153px; overflow: hidden;">
                                                                        <div class="style-iuogulojimageItemimagepreloader" id="comp-iuk2u8c3dataItem-ixrx96flimagepreloader"></div><img id="comp-iuk2u8c3dataItem-ixrx96flimageimage" alt="Entrevista Diario Concepcion" data-type="image" itemprop="contentUrl" style="width: 221px; height: 153px; object-fit: cover;" src="<?php echo get_template_directory_uri();?>/assets/img/9012a2_8fe7167966da475383409d2e972c4931_mv2.webp"></div>
                                                                </div>
                                                                <div id="comp-iuk2u8c3dataItem-ixrx96flzoom" class="style-iuogulojimageItemzoom" style="cursor: pointer;"></div>
                                                            </div>
                                                        </a>
                                                        <div id="comp-iuk2u8c3dataItem-ixrx96flpanel" class="style-iuogulojimageItem_panel style-iuogulojimageItempanel">
                                                            <div class="style-iuogulojimageItem_panelWrap">
                                                                <h6 aria-hidden="true" itemprop="name" id="comp-iuk2u8c3dataItem-ixrx96fltitle" class="style-iuogulojimageItemtitle" style="text-align: center;">Entrevista Diario Concepcion</h6><span id="comp-iuk2u8c3dataItem-ixrx96flDescription" itemprop="description" class="style-iuogulojimageItemdescription" style="text-align: center;"><!-- react-text: 552 -->Llegamos a Conce para que recibas tu propiedad en perfectas condiciones<!-- /react-text --><br><!-- react-text: 554 -->7 de Enero 2017<!-- /react-text --></span></div>
                                                        </div>
                                                    </div>
                                                    <div data-image-index="12" data-displayer-width="2048" data-displayer-height="1270" data-displayer-uri="9012a2_49c19f44b8dd4b08ab9fd17a1e23a20f_mv2_d_2048_1270_s_2.jpg" data-height-diff="78" data-width-diff="0" data-bottom-gap="0" data-image-wrapper-right="8" data-image-wrapper-left="8" data-image-wrapper-top="8" data-image-wrapper-bottom="8" data-margin-to-container="0" itemscope="" itemtype="http://schema.org/ImageObject" class="style-iuogulojimageItem" data-state="notShowPanel desktopView alignCenter unselected clipImage noTransition normal link" id="comp-iuk2u8c3dataItem-ive9ptil" style="position: absolute; height: 247px; width: 237px; left: 0px; top: 771px;">
                                                        <a draggable="false" data-page-item-context="galleryId:comp-iuk2u8c3 galleryCompId:comp-iuk2u8c3" data-gallery-id="comp-iuk2u8c3" href="?lightbox=dataItem-ive9ptil" target="_self" id="comp-iuk2u8c3dataItem-ive9ptillink" class="style-iuogulojimageItemlink" style="cursor: pointer; height: 100%; width: 100%; position: absolute; top: 0px; left: 0px; user-select: none; -webkit-user-drag: none; -webkit-user-modify: read-only;">
                                                            <div id="comp-iuk2u8c3dataItem-ive9ptilimageWrapper" class="style-iuogulojimageItemimageWrapper" style="height: 169px; width: 237px; margin: 0px;">
                                                                <div class="style-iuogulojimageItem_imgBorder">
                                                                    <div data-style="position:relative;overflow:hidden" class="style-iuogulojimageItemimage" id="comp-iuk2u8c3dataItem-ive9ptilimage" style="position: relative; width: 221px; height: 153px; overflow: hidden;">
                                                                        <div class="style-iuogulojimageItemimagepreloader" id="comp-iuk2u8c3dataItem-ive9ptilimagepreloader"></div><img id="comp-iuk2u8c3dataItem-ive9ptilimageimage" alt="TUCASAOK en LUN" data-type="image" itemprop="contentUrl" style="width: 221px; height: 153px; object-fit: cover;" src="<?php echo get_template_directory_uri();?>/assets/img/9012a2_49c19f44b8dd4b08ab9fd17a1e23a20f_mv2_d_2048_1270_s_2.webp"></div>
                                                                </div>
                                                                <div id="comp-iuk2u8c3dataItem-ive9ptilzoom" class="style-iuogulojimageItemzoom" style="cursor: pointer;"></div>
                                                            </div>
                                                        </a>
                                                        <div id="comp-iuk2u8c3dataItem-ive9ptilpanel" class="style-iuogulojimageItem_panel style-iuogulojimageItempanel">
                                                            <div class="style-iuogulojimageItem_panelWrap">
                                                                <h6 aria-hidden="true" itemprop="name" id="comp-iuk2u8c3dataItem-ive9ptiltitle" class="style-iuogulojimageItemtitle" style="text-align: center;">TUCASAOK en LUN</h6><span id="comp-iuk2u8c3dataItem-ive9ptilDescription" itemprop="description" class="style-iuogulojimageItemdescription" style="text-align: center;"><!-- react-text: 567 -->LUN describe porque es tan importante inspeccionar con nosotras tu propiedad<!-- /react-text --><br><!-- react-text: 569 -->7 de Noviembre 2016<!-- /react-text --></span></div>
                                                        </div>
                                                    </div>
                                                    <div data-image-index="13" data-displayer-width="2738" data-displayer-height="1093" data-displayer-uri="9012a2_1f03919e3c9e4c9fb4b56e60ad0100c6_mv2.png" data-height-diff="78" data-width-diff="0" data-bottom-gap="0" data-image-wrapper-right="8" data-image-wrapper-left="8" data-image-wrapper-top="8" data-image-wrapper-bottom="8" data-margin-to-container="0" itemscope="" itemtype="http://schema.org/ImageObject" class="style-iuogulojimageItem" data-state="notShowPanel desktopView alignCenter unselected clipImage noTransition normal link" id="comp-iuk2u8c3dataItem-iuoegwux" style="position: absolute; height: 247px; width: 237px; left: 247px; top: 771px;">
                                                        <a draggable="false" data-page-item-context="galleryId:comp-iuk2u8c3 galleryCompId:comp-iuk2u8c3" data-gallery-id="comp-iuk2u8c3" href="?lightbox=dataItem-iuoegwux" target="_self" id="comp-iuk2u8c3dataItem-iuoegwuxlink" class="style-iuogulojimageItemlink" style="cursor: pointer; height: 100%; width: 100%; position: absolute; top: 0px; left: 0px; user-select: none; -webkit-user-drag: none; -webkit-user-modify: read-only;">
                                                            <div id="comp-iuk2u8c3dataItem-iuoegwuximageWrapper" class="style-iuogulojimageItemimageWrapper" style="height: 169px; width: 237px; margin: 0px;">
                                                                <div class="style-iuogulojimageItem_imgBorder">
                                                                    <div data-style="position:relative;overflow:hidden" class="style-iuogulojimageItemimage" id="comp-iuk2u8c3dataItem-iuoegwuximage" style="position: relative; width: 221px; height: 153px; overflow: hidden;">
                                                                        <div class="style-iuogulojimageItemimagepreloader" id="comp-iuk2u8c3dataItem-iuoegwuximagepreloader"></div><img id="comp-iuk2u8c3dataItem-iuoegwuximageimage" alt="La Tercera Negocios" data-type="image" itemprop="contentUrl" style="width: 221px; height: 153px; object-fit: cover;" src="<?php echo get_template_directory_uri();?>/assets/img/9012a2_1f03919e3c9e4c9fb4b56e60ad0100c6_mv2.png"></div>
                                                                </div>
                                                                <div id="comp-iuk2u8c3dataItem-iuoegwuxzoom" class="style-iuogulojimageItemzoom" style="cursor: pointer;"></div>
                                                            </div>
                                                        </a>
                                                        <div id="comp-iuk2u8c3dataItem-iuoegwuxpanel" class="style-iuogulojimageItem_panel style-iuogulojimageItempanel">
                                                            <div class="style-iuogulojimageItem_panelWrap">
                                                                <h6 aria-hidden="true" itemprop="name" id="comp-iuk2u8c3dataItem-iuoegwuxtitle" class="style-iuogulojimageItemtitle" style="text-align: center;">La Tercera Negocios</h6><span id="comp-iuk2u8c3dataItem-iuoegwuxDescription" itemprop="description" class="style-iuogulojimageItemdescription" style="text-align: center;"><!-- react-text: 582 -->TUCASAOK&nbsp;en Salón Inmobiliario de Chile <!-- /react-text --><br><!-- react-text: 584 -->Octubre 2016<!-- /react-text --></span></div>
                                                        </div>
                                                    </div>
                                                    <div data-image-index="14" data-displayer-width="1280" data-displayer-height="960" data-displayer-uri="9012a2_710cb9d289594cf3a9b5dd4ddc738cdd_mv2.jpg" data-height-diff="78" data-width-diff="0" data-bottom-gap="0" data-image-wrapper-right="8" data-image-wrapper-left="8" data-image-wrapper-top="8" data-image-wrapper-bottom="8" data-margin-to-container="0" itemscope="" itemtype="http://schema.org/ImageObject" class="style-iuogulojimageItem" data-state="notShowPanel desktopView alignCenter unselected clipImage noTransition normal noLink" id="comp-iuk2u8c3dataItem-iuof1o23" style="position: absolute; height: 247px; width: 237px; left: 494px; top: 771px;">
                                                        <a draggable="false" data-page-item-context="galleryId:comp-iuk2u8c3 galleryCompId:comp-iuk2u8c3" data-gallery-id="comp-iuk2u8c3" href="?lightbox=dataItem-iuof1o23" target="_self" id="comp-iuk2u8c3dataItem-iuof1o23link" class="style-iuogulojimageItemlink" style="cursor: pointer; height: 100%; width: 100%; position: absolute; top: 0px; left: 0px; user-select: none; -webkit-user-drag: none; -webkit-user-modify: read-only;">
                                                            <div id="comp-iuk2u8c3dataItem-iuof1o23imageWrapper" class="style-iuogulojimageItemimageWrapper" style="height: 169px; width: 237px; margin: 0px;">
                                                                <div class="style-iuogulojimageItem_imgBorder">
                                                                    <div data-style="position:relative;overflow:hidden" class="style-iuogulojimageItemimage" id="comp-iuk2u8c3dataItem-iuof1o23image" style="position: relative; width: 221px; height: 153px; overflow: hidden;">
                                                                        <div class="style-iuogulojimageItemimagepreloader" id="comp-iuk2u8c3dataItem-iuof1o23imagepreloader"></div><img id="comp-iuk2u8c3dataItem-iuof1o23imageimage" alt="Mujeres ON" data-type="image" itemprop="contentUrl" style="width: 221px; height: 153px; object-fit: cover;" src="<?php echo get_template_directory_uri();?>/assets/img/9012a2_710cb9d289594cf3a9b5dd4ddc738cdd_mv2.webp"></div>
                                                                </div>
                                                                <div id="comp-iuk2u8c3dataItem-iuof1o23zoom" class="style-iuogulojimageItemzoom" style="cursor: pointer;"></div>
                                                            </div>
                                                        </a>
                                                        <div id="comp-iuk2u8c3dataItem-iuof1o23panel" class="style-iuogulojimageItem_panel style-iuogulojimageItempanel">
                                                            <div class="style-iuogulojimageItem_panelWrap">
                                                                <h6 aria-hidden="true" itemprop="name" id="comp-iuk2u8c3dataItem-iuof1o23title" class="style-iuogulojimageItemtitle" style="text-align: center;">Mujeres ON</h6><span id="comp-iuk2u8c3dataItem-iuof1o23Description" itemprop="description" class="style-iuogulojimageItemdescription" style="text-align: center;"><!-- react-text: 597 -->TUCASAOK apoyando el emprendimiento de mujeres.<!-- /react-text --><br><!-- react-text: 599 -->18 Octubre 2016<!-- /react-text --></span></div>
                                                        </div>
                                                    </div>
                                                    <div data-image-index="15" data-displayer-width="1280" data-displayer-height="960" data-displayer-uri="9012a2_85b0b11084044f7b9f0099de71f8cef1_mv2.jpg" data-height-diff="78" data-width-diff="0" data-bottom-gap="0" data-image-wrapper-right="8" data-image-wrapper-left="8" data-image-wrapper-top="8" data-image-wrapper-bottom="8" data-margin-to-container="0" itemscope="" itemtype="http://schema.org/ImageObject" class="style-iuogulojimageItem" data-state="notShowPanel desktopView alignCenter unselected clipImage noTransition normal noLink" id="comp-iuk2u8c3dataItem-iuof1o231" style="position: absolute; height: 247px; width: 237px; left: 741px; top: 771px;">
                                                        <a draggable="false" data-page-item-context="galleryId:comp-iuk2u8c3 galleryCompId:comp-iuk2u8c3" data-gallery-id="comp-iuk2u8c3" href="?lightbox=dataItem-iuof1o231" target="_self" id="comp-iuk2u8c3dataItem-iuof1o231link" class="style-iuogulojimageItemlink" style="cursor: pointer; height: 100%; width: 100%; position: absolute; top: 0px; left: 0px; user-select: none; -webkit-user-drag: none; -webkit-user-modify: read-only;">
                                                            <div id="comp-iuk2u8c3dataItem-iuof1o231imageWrapper" class="style-iuogulojimageItemimageWrapper" style="height: 169px; width: 237px; margin: 0px;">
                                                                <div class="style-iuogulojimageItem_imgBorder">
                                                                    <div data-style="position:relative;overflow:hidden" class="style-iuogulojimageItemimage" id="comp-iuk2u8c3dataItem-iuof1o231image" style="position: relative; width: 221px; height: 153px; overflow: hidden;">
                                                                        <div class="style-iuogulojimageItemimagepreloader" id="comp-iuk2u8c3dataItem-iuof1o231imagepreloader"></div><img id="comp-iuk2u8c3dataItem-iuof1o231imageimage" alt="FEN" data-type="image" itemprop="contentUrl" style="width: 221px; height: 153px; object-fit: cover;" src="<?php echo get_template_directory_uri();?>/assets/img/9012a2_85b0b11084044f7b9f0099de71f8cef1_mv2.webp"></div>
                                                                </div>
                                                                <div id="comp-iuk2u8c3dataItem-iuof1o231zoom" class="style-iuogulojimageItemzoom" style="cursor: pointer;"></div>
                                                            </div>
                                                        </a>
                                                        <div id="comp-iuk2u8c3dataItem-iuof1o231panel" class="style-iuogulojimageItem_panel style-iuogulojimageItempanel">
                                                            <div class="style-iuogulojimageItem_panelWrap">
                                                                <h6 aria-hidden="true" itemprop="name" id="comp-iuk2u8c3dataItem-iuof1o231title" class="style-iuogulojimageItemtitle" style="text-align: center;">FEN</h6><span id="comp-iuk2u8c3dataItem-iuof1o231Description" itemprop="description" class="style-iuogulojimageItemdescription" style="text-align: center;"><!-- react-text: 612 -->TUCASAOK apoyando a los alumnos de ingeniería comercial de la U. de Chile<!-- /react-text --><br><!-- react-text: 614 -->18 Octubre 2016<!-- /react-text --></span></div>
                                                        </div>
                                                    </div>
                                                    <div data-image-index="16" data-displayer-width="361" data-displayer-height="241" data-displayer-uri="9012a2_4e2b7fc71add4e0695a3847cf3b1e06e_mv2.png" data-height-diff="78" data-width-diff="0" data-bottom-gap="0" data-image-wrapper-right="8" data-image-wrapper-left="8" data-image-wrapper-top="8" data-image-wrapper-bottom="8" data-margin-to-container="0" itemscope="" itemtype="http://schema.org/ImageObject" class="style-iuogulojimageItem" data-state="notShowPanel desktopView alignCenter unselected clipImage noTransition normal noLink" id="comp-iuk2u8c3dataItem-iuof1o22" style="position: absolute; height: 247px; width: 237px; left: 0px; top: 1028px;">
                                                        <a draggable="false" data-page-item-context="galleryId:comp-iuk2u8c3 galleryCompId:comp-iuk2u8c3" data-gallery-id="comp-iuk2u8c3" href="?lightbox=dataItem-iuof1o22" target="_self" id="comp-iuk2u8c3dataItem-iuof1o22link" class="style-iuogulojimageItemlink" style="cursor: pointer; height: 100%; width: 100%; position: absolute; top: 0px; left: 0px; user-select: none; -webkit-user-drag: none; -webkit-user-modify: read-only;">
                                                            <div id="comp-iuk2u8c3dataItem-iuof1o22imageWrapper" class="style-iuogulojimageItemimageWrapper" style="height: 169px; width: 237px; margin: 0px;">
                                                                <div class="style-iuogulojimageItem_imgBorder">
                                                                    <div data-style="position:relative;overflow:hidden" class="style-iuogulojimageItemimage" id="comp-iuk2u8c3dataItem-iuof1o22image" style="position: relative; width: 221px; height: 153px; overflow: hidden;">
                                                                        <div class="style-iuogulojimageItemimagepreloader" id="comp-iuk2u8c3dataItem-iuof1o22imagepreloader"></div><img id="comp-iuk2u8c3dataItem-iuof1o22imageimage" alt="Chilevisión noticias" data-type="image" itemprop="contentUrl" style="width: 221px; height: 153px; object-fit: cover;" src="<?php echo get_template_directory_uri();?>/assets/img/9012a2_4e2b7fc71add4e0695a3847cf3b1e06e_mv2.png"></div>
                                                                </div>
                                                                <div id="comp-iuk2u8c3dataItem-iuof1o22zoom" class="style-iuogulojimageItemzoom" style="cursor: pointer;"></div>
                                                            </div>
                                                        </a>
                                                        <div id="comp-iuk2u8c3dataItem-iuof1o22panel" class="style-iuogulojimageItem_panel style-iuogulojimageItempanel">
                                                            <div class="style-iuogulojimageItem_panelWrap">
                                                                <h6 aria-hidden="true" itemprop="name" id="comp-iuk2u8c3dataItem-iuof1o22title" class="style-iuogulojimageItemtitle" style="text-align: center;">Chilevisión noticias</h6><span id="comp-iuk2u8c3dataItem-iuof1o22Description" itemprop="description" class="style-iuogulojimageItemdescription" style="text-align: center;"><!-- react-text: 627 -->TUCASAOK explica lo importante de inspeccionar la propiedad antes de recibirla.<!-- /react-text --><br><!-- react-text: 629 -->16 Octubre 2016<!-- /react-text --></span></div>
                                                        </div>
                                                    </div>
                                                    <div data-image-index="17" data-displayer-width="640" data-displayer-height="640" data-displayer-uri="9012a2_4c6ad091e4914176af69d532a10a13f5_mv2.jpg" data-height-diff="78" data-width-diff="0" data-bottom-gap="0" data-image-wrapper-right="8" data-image-wrapper-left="8" data-image-wrapper-top="8" data-image-wrapper-bottom="8" data-margin-to-container="0" itemscope="" itemtype="http://schema.org/ImageObject" class="style-iuogulojimageItem" data-state="notShowPanel desktopView alignCenter unselected clipImage noTransition normal noLink" id="comp-iuk2u8c3dataItem-iuoe819n1" style="position: absolute; height: 247px; width: 237px; left: 247px; top: 1028px;">
                                                        <a draggable="false" data-page-item-context="galleryId:comp-iuk2u8c3 galleryCompId:comp-iuk2u8c3" data-gallery-id="comp-iuk2u8c3" href="?lightbox=dataItem-iuoe819n1" target="_self" id="comp-iuk2u8c3dataItem-iuoe819n1link" class="style-iuogulojimageItemlink" style="cursor: pointer; height: 100%; width: 100%; position: absolute; top: 0px; left: 0px; user-select: none; -webkit-user-drag: none; -webkit-user-modify: read-only;">
                                                            <div id="comp-iuk2u8c3dataItem-iuoe819n1imageWrapper" class="style-iuogulojimageItemimageWrapper" style="height: 169px; width: 237px; margin: 0px;">
                                                                <div class="style-iuogulojimageItem_imgBorder">
                                                                    <div data-style="position:relative;overflow:hidden" class="style-iuogulojimageItemimage" id="comp-iuk2u8c3dataItem-iuoe819n1image" style="position: relative; width: 221px; height: 153px; overflow: hidden;">
                                                                        <div class="style-iuogulojimageItemimagepreloader" id="comp-iuk2u8c3dataItem-iuoe819n1imagepreloader"></div><img id="comp-iuk2u8c3dataItem-iuoe819n1imageimage" alt="Charla UDD" data-type="image" itemprop="contentUrl" style="width: 221px; height: 153px; object-fit: cover;" src="<?php echo get_template_directory_uri();?>/assets/img/9012a2_4c6ad091e4914176af69d532a10a13f5_mv2.webp"></div>
                                                                </div>
                                                                <div id="comp-iuk2u8c3dataItem-iuoe819n1zoom" class="style-iuogulojimageItemzoom" style="cursor: pointer;"></div>
                                                            </div>
                                                        </a>
                                                        <div id="comp-iuk2u8c3dataItem-iuoe819n1panel" class="style-iuogulojimageItem_panel style-iuogulojimageItempanel">
                                                            <div class="style-iuogulojimageItem_panelWrap">
                                                                <h6 aria-hidden="true" itemprop="name" id="comp-iuk2u8c3dataItem-iuoe819n1title" class="style-iuogulojimageItemtitle" style="text-align: center;">Charla UDD</h6><span id="comp-iuk2u8c3dataItem-iuoe819n1Description" itemprop="description" class="style-iuogulojimageItemdescription" style="text-align: center;"><!-- react-text: 642 -->TUCASAOK en charla First Tuesday, facultad de ingeniería UDD<!-- /react-text --><br><!-- react-text: 644 -->Septiembre 2016<!-- /react-text --></span></div>
                                                        </div>
                                                    </div>
                                                    <div data-image-index="18" data-displayer-width="340" data-displayer-height="340" data-displayer-uri="9012a2_cc63f39abd8b4d6594fbce51116db49c_mv2.png" data-height-diff="78" data-width-diff="0" data-bottom-gap="0" data-image-wrapper-right="8" data-image-wrapper-left="8" data-image-wrapper-top="8" data-image-wrapper-bottom="8" data-margin-to-container="0" itemscope="" itemtype="http://schema.org/ImageObject" class="style-iuogulojimageItem" data-state="notShowPanel desktopView alignCenter unselected clipImage noTransition normal link" id="comp-iuk2u8c3dataItem-iuoe819o2" style="position: absolute; height: 247px; width: 237px; left: 494px; top: 1028px;">
                                                        <a draggable="false" data-page-item-context="galleryId:comp-iuk2u8c3 galleryCompId:comp-iuk2u8c3" data-gallery-id="comp-iuk2u8c3" href="?lightbox=dataItem-iuoe819o2" target="_self" id="comp-iuk2u8c3dataItem-iuoe819o2link" class="style-iuogulojimageItemlink" style="cursor: pointer; height: 100%; width: 100%; position: absolute; top: 0px; left: 0px; user-select: none; -webkit-user-drag: none; -webkit-user-modify: read-only;">
                                                            <div id="comp-iuk2u8c3dataItem-iuoe819o2imageWrapper" class="style-iuogulojimageItemimageWrapper" style="height: 169px; width: 237px; margin: 0px;">
                                                                <div class="style-iuogulojimageItem_imgBorder">
                                                                    <div data-style="position:relative;overflow:hidden" class="style-iuogulojimageItemimage" id="comp-iuk2u8c3dataItem-iuoe819o2image" style="position: relative; width: 221px; height: 153px; overflow: hidden;">
                                                                        <div class="style-iuogulojimageItemimagepreloader" id="comp-iuk2u8c3dataItem-iuoe819o2imagepreloader"></div><img id="comp-iuk2u8c3dataItem-iuoe819o2imageimage" alt="Noticiero de Chilevisión" data-type="image" itemprop="contentUrl" style="width: 221px; height: 153px; object-fit: cover;" src="<?php echo get_template_directory_uri();?>/assets/img/9012a2_cc63f39abd8b4d6594fbce51116db49c_mv2.png"></div>
                                                                </div>
                                                                <div id="comp-iuk2u8c3dataItem-iuoe819o2zoom" class="style-iuogulojimageItemzoom" style="cursor: pointer;"></div>
                                                            </div>
                                                        </a>
                                                        <div id="comp-iuk2u8c3dataItem-iuoe819o2panel" class="style-iuogulojimageItem_panel style-iuogulojimageItempanel">
                                                            <div class="style-iuogulojimageItem_panelWrap">
                                                                <h6 aria-hidden="true" itemprop="name" id="comp-iuk2u8c3dataItem-iuoe819o2title" class="style-iuogulojimageItemtitle" style="text-align: center;">Noticiero de Chilevisión</h6><span id="comp-iuk2u8c3dataItem-iuoe819o2Description" itemprop="description" class="style-iuogulojimageItemdescription" style="text-align: center;"><!-- react-text: 657 -->TUCASAOK explica posibles causas de derrumbe en Las Condes.<!-- /react-text --><br><!-- react-text: 659 -->Septiembre 2016<!-- /react-text --></span></div>
                                                        </div>
                                                    </div>
                                                    <div data-image-index="19" data-displayer-width="1280" data-displayer-height="960" data-displayer-uri="9012a2_677c81a9d60b4a7faf68e09b8fc9282c_mv2.jpg" data-height-diff="78" data-width-diff="0" data-bottom-gap="0" data-image-wrapper-right="8" data-image-wrapper-left="8" data-image-wrapper-top="8" data-image-wrapper-bottom="8" data-margin-to-container="0" itemscope="" itemtype="http://schema.org/ImageObject" class="style-iuogulojimageItem" data-state="notShowPanel desktopView alignCenter unselected clipImage noTransition normal noLink" id="comp-iuk2u8c3dataItem-iuoe819n" style="position: absolute; height: 247px; width: 237px; left: 741px; top: 1028px;">
                                                        <a draggable="false" data-page-item-context="galleryId:comp-iuk2u8c3 galleryCompId:comp-iuk2u8c3" data-gallery-id="comp-iuk2u8c3" href="?lightbox=dataItem-iuoe819n" target="_self" id="comp-iuk2u8c3dataItem-iuoe819nlink" class="style-iuogulojimageItemlink" style="cursor: pointer; height: 100%; width: 100%; position: absolute; top: 0px; left: 0px; user-select: none; -webkit-user-drag: none; -webkit-user-modify: read-only;">
                                                            <div id="comp-iuk2u8c3dataItem-iuoe819nimageWrapper" class="style-iuogulojimageItemimageWrapper" style="height: 169px; width: 237px; margin: 0px;">
                                                                <div class="style-iuogulojimageItem_imgBorder">
                                                                    <div data-style="position:relative;overflow:hidden" class="style-iuogulojimageItemimage" id="comp-iuk2u8c3dataItem-iuoe819nimage" style="position: relative; width: 221px; height: 153px; overflow: hidden;">
                                                                        <div class="style-iuogulojimageItemimagepreloader" id="comp-iuk2u8c3dataItem-iuoe819nimagepreloader"></div><img id="comp-iuk2u8c3dataItem-iuoe819nimageimage" alt="Programa innovación infinita" data-type="image" itemprop="contentUrl" style="width: 221px; height: 153px; object-fit: cover;" src="<?php echo get_template_directory_uri();?>/assets/img/9012a2_677c81a9d60b4a7faf68e09b8fc9282c_mv2.webp"></div>
                                                                </div>
                                                                <div id="comp-iuk2u8c3dataItem-iuoe819nzoom" class="style-iuogulojimageItemzoom" style="cursor: pointer;"></div>
                                                            </div>
                                                        </a>
                                                        <div id="comp-iuk2u8c3dataItem-iuoe819npanel" class="style-iuogulojimageItem_panel style-iuogulojimageItempanel">
                                                            <div class="style-iuogulojimageItem_panelWrap">
                                                                <h6 aria-hidden="true" itemprop="name" id="comp-iuk2u8c3dataItem-iuoe819ntitle" class="style-iuogulojimageItemtitle" style="text-align: center;">Programa innovación infinita</h6><span id="comp-iuk2u8c3dataItem-iuoe819nDescription" itemprop="description" class="style-iuogulojimageItemdescription" style="text-align: center;"><!-- react-text: 672 -->TUCASAOK en programa innovación infinita con Nicolás Larraín<!-- /react-text --><br><!-- react-text: 674 -->Agosto 2016<!-- /react-text --></span></div>
                                                        </div>
                                                    </div>
                                                    <div data-image-index="20" data-displayer-width="640" data-displayer-height="445" data-displayer-uri="9012a2_6247dcc5894245a9a76bdc4f5177a97b_mv2.jpg" data-height-diff="78" data-width-diff="0" data-bottom-gap="0" data-image-wrapper-right="8" data-image-wrapper-left="8" data-image-wrapper-top="8" data-image-wrapper-bottom="8" data-margin-to-container="0" itemscope="" itemtype="http://schema.org/ImageObject" class="style-iuogulojimageItem" data-state="notShowPanel desktopView alignCenter unselected clipImage noTransition normal link" id="comp-iuk2u8c3dataItem-iuoe819o3" style="position: absolute; height: 247px; width: 237px; left: 0px; top: 1285px;">
                                                        <a draggable="false" data-page-item-context="galleryId:comp-iuk2u8c3 galleryCompId:comp-iuk2u8c3" data-gallery-id="comp-iuk2u8c3" href="?lightbox=dataItem-iuoe819o3" target="_self" id="comp-iuk2u8c3dataItem-iuoe819o3link" class="style-iuogulojimageItemlink" style="cursor: pointer; height: 100%; width: 100%; position: absolute; top: 0px; left: 0px; user-select: none; -webkit-user-drag: none; -webkit-user-modify: read-only;">
                                                            <div id="comp-iuk2u8c3dataItem-iuoe819o3imageWrapper" class="style-iuogulojimageItemimageWrapper" style="height: 169px; width: 237px; margin: 0px;">
                                                                <div class="style-iuogulojimageItem_imgBorder">
                                                                    <div data-style="position:relative;overflow:hidden" class="style-iuogulojimageItemimage" id="comp-iuk2u8c3dataItem-iuoe819o3image" style="position: relative; width: 221px; height: 153px; overflow: hidden;">
                                                                        <div class="style-iuogulojimageItemimagepreloader" id="comp-iuk2u8c3dataItem-iuoe819o3imagepreloader"></div><img id="comp-iuk2u8c3dataItem-iuoe819o3imageimage" alt="Start-Up Chile" data-type="image" itemprop="contentUrl" style="width: 221px; height: 153px; object-fit: cover;" src="<?php echo get_template_directory_uri();?>/assets/img/9012a2_6247dcc5894245a9a76bdc4f5177a97b_mv2.webp"></div>
                                                                </div>
                                                                <div id="comp-iuk2u8c3dataItem-iuoe819o3zoom" class="style-iuogulojimageItemzoom" style="cursor: pointer;"></div>
                                                            </div>
                                                        </a>
                                                        <div id="comp-iuk2u8c3dataItem-iuoe819o3panel" class="style-iuogulojimageItem_panel style-iuogulojimageItempanel">
                                                            <div class="style-iuogulojimageItem_panelWrap">
                                                                <h6 aria-hidden="true" itemprop="name" id="comp-iuk2u8c3dataItem-iuoe819o3title" class="style-iuogulojimageItemtitle" style="text-align: center;">Start-Up Chile</h6><span id="comp-iuk2u8c3dataItem-iuoe819o3Description" itemprop="description" class="style-iuogulojimageItemdescription" style="text-align: center;"><!-- react-text: 687 -->TUCASAOK parte de startup-chile, generación 16<!-- /react-text --><br><!-- react-text: 689 -->Julio 2016<!-- /react-text --></span></div>
                                                        </div>
                                                    </div>
                                                    <div data-image-index="21" data-displayer-width="312" data-displayer-height="360" data-displayer-uri="9012a2_48e6fb39f6e14ac387b0f89c6d7d7dc6_mv2.jpg" data-height-diff="78" data-width-diff="0" data-bottom-gap="0" data-image-wrapper-right="8" data-image-wrapper-left="8" data-image-wrapper-top="8" data-image-wrapper-bottom="8" data-margin-to-container="0" itemscope="" itemtype="http://schema.org/ImageObject" class="style-iuogulojimageItem" data-state="notShowPanel desktopView alignCenter unselected clipImage noTransition normal link" id="comp-iuk2u8c3dataItem-iuoe819o1" style="position: absolute; height: 247px; width: 237px; left: 247px; top: 1285px;">
                                                        <a draggable="false" data-page-item-context="galleryId:comp-iuk2u8c3 galleryCompId:comp-iuk2u8c3" data-gallery-id="comp-iuk2u8c3" href="?lightbox=dataItem-iuoe819o1" target="_self" id="comp-iuk2u8c3dataItem-iuoe819o1link" class="style-iuogulojimageItemlink" style="cursor: pointer; height: 100%; width: 100%; position: absolute; top: 0px; left: 0px; user-select: none; -webkit-user-drag: none; -webkit-user-modify: read-only;">
                                                            <div id="comp-iuk2u8c3dataItem-iuoe819o1imageWrapper" class="style-iuogulojimageItemimageWrapper" style="height: 169px; width: 237px; margin: 0px;">
                                                                <div class="style-iuogulojimageItem_imgBorder">
                                                                    <div data-style="position:relative;overflow:hidden" class="style-iuogulojimageItemimage" id="comp-iuk2u8c3dataItem-iuoe819o1image" style="position: relative; width: 221px; height: 153px; overflow: hidden;">
                                                                        <div class="style-iuogulojimageItemimagepreloader" id="comp-iuk2u8c3dataItem-iuoe819o1imagepreloader"></div><img id="comp-iuk2u8c3dataItem-iuoe819o1imageimage" alt="Entrevista de emprendimiento" data-type="image" itemprop="contentUrl" style="width: 221px; height: 153px; object-fit: cover;" src="<?php echo get_template_directory_uri();?>/assets/img/9012a2_48e6fb39f6e14ac387b0f89c6d7d7dc6_mv2.webp"></div>
                                                                </div>
                                                                <div id="comp-iuk2u8c3dataItem-iuoe819o1zoom" class="style-iuogulojimageItemzoom" style="cursor: pointer;"></div>
                                                            </div>
                                                        </a>
                                                        <div id="comp-iuk2u8c3dataItem-iuoe819o1panel" class="style-iuogulojimageItem_panel style-iuogulojimageItempanel">
                                                            <div class="style-iuogulojimageItem_panelWrap">
                                                                <h6 aria-hidden="true" itemprop="name" id="comp-iuk2u8c3dataItem-iuoe819o1title" class="style-iuogulojimageItemtitle" style="text-align: center;">Entrevista de emprendimiento</h6><span id="comp-iuk2u8c3dataItem-iuoe819o1Description" itemprop="description" class="style-iuogulojimageItemdescription" style="text-align: center;"><!-- react-text: 702 -->Entrevista de parte de asociación de emprendedores de Chile (asech)<!-- /react-text --><br><!-- react-text: 704 -->MAYO 2016<!-- /react-text --></span></div>
                                                        </div>
                                                    </div>
                                                    <div data-image-index="22" data-displayer-width="1016" data-displayer-height="1331" data-displayer-uri="9012a2_d9d74d34feba44b89c1a8115023a7598.jpg" data-height-diff="78" data-width-diff="0" data-bottom-gap="0" data-image-wrapper-right="8" data-image-wrapper-left="8" data-image-wrapper-top="8" data-image-wrapper-bottom="8" data-margin-to-container="0" itemscope="" itemtype="http://schema.org/ImageObject" class="style-iuogulojimageItem" data-state="notShowPanel desktopView alignCenter unselected clipImage noTransition normal link" id="comp-iuk2u8c3dataItem-iuoeb1wm" style="position: absolute; height: 247px; width: 237px; left: 494px; top: 1285px;">
                                                        <a draggable="false" data-page-item-context="galleryId:comp-iuk2u8c3 galleryCompId:comp-iuk2u8c3" data-gallery-id="comp-iuk2u8c3" href="?lightbox=dataItem-iuoeb1wm" target="_self" id="comp-iuk2u8c3dataItem-iuoeb1wmlink" class="style-iuogulojimageItemlink" style="cursor: pointer; height: 100%; width: 100%; position: absolute; top: 0px; left: 0px; user-select: none; -webkit-user-drag: none; -webkit-user-modify: read-only;">
                                                            <div id="comp-iuk2u8c3dataItem-iuoeb1wmimageWrapper" class="style-iuogulojimageItemimageWrapper" style="height: 169px; width: 237px; margin: 0px;">
                                                                <div class="style-iuogulojimageItem_imgBorder">
                                                                    <div data-style="position:relative;overflow:hidden" class="style-iuogulojimageItemimage" id="comp-iuk2u8c3dataItem-iuoeb1wmimage" style="position: relative; width: 221px; height: 153px; overflow: hidden;">
                                                                        <div class="style-iuogulojimageItemimagepreloader" id="comp-iuk2u8c3dataItem-iuoeb1wmimagepreloader"></div><img id="comp-iuk2u8c3dataItem-iuoeb1wmimageimage" alt="TUCASAOK en Publimetro" data-type="image" itemprop="contentUrl" style="width: 221px; height: 153px; object-fit: cover;" src="<?php echo get_template_directory_uri();?>/assets/img/9012a2_d9d74d34feba44b89c1a8115023a7598.webp"></div>
                                                                </div>
                                                                <div id="comp-iuk2u8c3dataItem-iuoeb1wmzoom" class="style-iuogulojimageItemzoom" style="cursor: pointer;"></div>
                                                            </div>
                                                        </a>
                                                        <div id="comp-iuk2u8c3dataItem-iuoeb1wmpanel" class="style-iuogulojimageItem_panel style-iuogulojimageItempanel">
                                                            <div class="style-iuogulojimageItem_panelWrap">
                                                                <h6 aria-hidden="true" itemprop="name" id="comp-iuk2u8c3dataItem-iuoeb1wmtitle" class="style-iuogulojimageItemtitle" style="text-align: center;">TUCASAOK en Publimetro</h6><span id="comp-iuk2u8c3dataItem-iuoeb1wmDescription" itemprop="description" class="style-iuogulojimageItemdescription" style="text-align: center;"><!-- react-text: 717 -->TUCASAOK en revista casas de Publimetro<!-- /react-text --><br><!-- react-text: 719 -->Octubre_2015<!-- /react-text --></span></div>
                                                        </div>
                                                    </div>
                                                    <div data-image-index="23" data-displayer-width="591" data-displayer-height="746" data-displayer-uri="9012a2_4a97f150ac024196bebf89724aaf3c3d_mv2.jpg" data-height-diff="78" data-width-diff="0" data-bottom-gap="0" data-image-wrapper-right="8" data-image-wrapper-left="8" data-image-wrapper-top="8" data-image-wrapper-bottom="8" data-margin-to-container="0" itemscope="" itemtype="http://schema.org/ImageObject" class="style-iuogulojimageItem" data-state="notShowPanel desktopView alignCenter unselected clipImage noTransition normal link" id="comp-iuk2u8c3dataItem-iuoe819o" style="position: absolute; height: 247px; width: 237px; left: 741px; top: 1285px;">
                                                        <a draggable="false" data-page-item-context="galleryId:comp-iuk2u8c3 galleryCompId:comp-iuk2u8c3" data-gallery-id="comp-iuk2u8c3" href="?lightbox=dataItem-iuoe819o" target="_self" id="comp-iuk2u8c3dataItem-iuoe819olink" class="style-iuogulojimageItemlink" style="cursor: pointer; height: 100%; width: 100%; position: absolute; top: 0px; left: 0px; user-select: none; -webkit-user-drag: none; -webkit-user-modify: read-only;">
                                                            <div id="comp-iuk2u8c3dataItem-iuoe819oimageWrapper" class="style-iuogulojimageItemimageWrapper" style="height: 169px; width: 237px; margin: 0px;">
                                                                <div class="style-iuogulojimageItem_imgBorder">
                                                                    <div data-style="position:relative;overflow:hidden" class="style-iuogulojimageItemimage" id="comp-iuk2u8c3dataItem-iuoe819oimage" style="position: relative; width: 221px; height: 153px; overflow: hidden;">
                                                                        <div class="style-iuogulojimageItemimagepreloader" id="comp-iuk2u8c3dataItem-iuoe819oimagepreloader"></div><img id="comp-iuk2u8c3dataItem-iuoe819oimageimage" alt="Reportaje de LUN" data-type="image" itemprop="contentUrl" style="width: 221px; height: 153px; object-fit: cover;" src="<?php echo get_template_directory_uri();?>/assets/img/9012a2_4a97f150ac024196bebf89724aaf3c3d_mv2.webp"></div>
                                                                </div>
                                                                <div id="comp-iuk2u8c3dataItem-iuoe819ozoom" class="style-iuogulojimageItemzoom" style="cursor: pointer;"></div>
                                                            </div>
                                                        </a>
                                                        <div id="comp-iuk2u8c3dataItem-iuoe819opanel" class="style-iuogulojimageItem_panel style-iuogulojimageItempanel">
                                                            <div class="style-iuogulojimageItem_panelWrap">
                                                                <h6 aria-hidden="true" itemprop="name" id="comp-iuk2u8c3dataItem-iuoe819otitle" class="style-iuogulojimageItemtitle" style="text-align: center;">Reportaje de LUN</h6><span id="comp-iuk2u8c3dataItem-iuoe819oDescription" itemprop="description" class="style-iuogulojimageItemdescription" style="text-align: center;"><!-- react-text: 732 -->Reportaje a TUCASAOK en las últimas noticias<!-- /react-text --><br><!-- react-text: 734 -->Agosto 2015<!-- /react-text --></span></div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div tabindex="0" id="comp-iuk2u8c3showMore" class="style-iuogulojshowMore">Más información</div>
                                            </div>
                                            <div class="v1" id="comp-iuojdflt" style="width: 415px; left: 0px; position: absolute; top: 707px; height: 220px;">
                                                <div tabindex="0" id="comp-iuojdfltvideoFrame" class="v1videoFrame">
                                                    <iframe height="100%" width="100%" allowfullscreen="" frameborder="0" title="External YouTube" aria-label="External YouTube" src="//www.youtube.com/embed/XfKsVEXcBkE?wmode=transparent&amp;autoplay=0&amp;theme=dark&amp;controls=1&amp;autohide=0&amp;loop=0&amp;showinfo=0&amp;rel=0&amp;playlist=false&amp;enablejsapi=0"></iframe>
                                                </div>
                                                <div id="comp-iuojdfltpreview" class="v1preview" style="display: none;"></div>
                                            </div>
                                            <div class="txtNew" id="comp-iuojdflu" style="left: 426px; width: 353px; position: absolute; top: 707px;">
                                                <p class="font_8" style="font-size:13px;"><span style="font-size:13px;"><span style="font-family:wfont_9012a2_6d1c84e5242d4b0e8c7bd107ccec04ac,wf_6d1c84e5242d4b0e8c7bd107c,orig_helvetica_bold;"><span style="color:#000000;"><span style="letter-spacing:0.05em;"><span style="font-weight:bold;">TUCASA</span></span>
                                                    </span><span style="color:#FF6600;"><span style="letter-spacing:0.05em;"><span style="font-weight:bold;">OK</span></span>
                                                    </span>
                                                    </span><span style="color:#000000;"><span style="letter-spacing:0.05em;"><span style="font-family:wfont_9012a2_cf30a994f829458daa5b46a9a39e7eac,wf_cf30a994f829458daa5b46a9a,orig_cooperhewittbook;"> inspecciona propiedades nuevas, con el fin de que la recibas&nbsp;en perfectas condiciones y hagas valer las garantías asociadas a tu compra.</span></span>
                                                    </span>
                                                    </span>
                                                </p>

                                                <p class="font_8" style="font-size:13px;"><span style="font-size:13px;"><span style="color:#000000;"><span style="letter-spacing:0.05em;"><span style="font-family:wfont_9012a2_cf30a994f829458daa5b46a9a39e7eac,wf_cf30a994f829458daa5b46a9a,orig_cooperhewittbook;">Resguardando la seguridad de tu familia y el valor de<span style="font-weight:bold;"> TU INVERSIÓN.</span></span>
                                                    </span>
                                                    </span>
                                                    </span>
                                                </p>

                                                <p class="font_8" style="font-size:13px;">&nbsp;</p>

                                                <p class="font_8" style="font-size:13px;"><span style="font-size:13px;"><span style="color:#000000;"><span style="letter-spacing:0.05em;"><span style="font-family:wfont_9012a2_cf30a994f829458daa5b46a9a39e7eac,wf_cf30a994f829458daa5b46a9a,orig_cooperhewittbook;">Nuestros profesionales utilizan tecnología de última generación para revisar hasta el último rincón de tu propiedad, detectando fugas de gas, filtraciones de agua, fallas eléctricas y&nbsp;puentes térmicos. Verificamos los metros cuadrados vendidos y el estado de las terminaciones.</span></span>
                                                    </span>
                                                    </span>
                                                </p>

                                                <p class="font_8" style="font-size:13px;"><span style="font-size:13px;"><span style="color:#000000;"><span style="letter-spacing:0.05em;"><span style="font-family:wfont_9012a2_cf30a994f829458daa5b46a9a39e7eac,wf_cf30a994f829458daa5b46a9a,orig_cooperhewittbook;">Descansa y deja la inspección en nuestras manos.</span></span>
                                                    </span>
                                                    </span>
                                                </p>
                                            </div>
                                            <div class="txtNew" id="comp-iuokppsu" style="left: 316px; width: 350px; position: absolute; top: 3065px;">
                                                <h2 class="font_2" style="text-align:center;"><span style="letter-spacing:0.1em;">NOTICIAS Y TESTIMONIOS</span></h2>
                                            </div>
                                            <div class="style-ja795yey" id="comp-ix9afwcd" style="min-height: 446px; min-width: 980px; left: 0px; width: 980px; position: absolute; top: 4688px; height: 446px;">
                                                
                                                <div id="comp-ix9afwcdoverlay" class="style-ja795yeyoverlay"></div>
                                            </div>
                                            <section class="strc1" id="comp-ja78axmi" style="width: 980px; position: absolute; top: 463px; height: 211px; left: 0px;">
                                                <div data-enable-video="true" class="strc1balata" id="comp-ja78axmibalata" style="position: absolute; top: 0px; width: calc((100vw - 17px) - 0px); height: 100%; pointer-events: auto; bottom: 0px; left: calc(490px - (((100vw - 17px) - 0px) / 2)); right: calc(490px + (((100vw - 17px) - 0px) / 2)); overflow: hidden; clip: rect(0px 1349px 211px 0px);">
                                                    <div class="bgColor" id="comp-ja78axmibalatabgcolor" style="position: absolute; width: 100%; height: 100%;">
                                                        <div id="comp-ja78axmibalatabgcoloroverlay" class="bgColoroverlay" style="width: 100%; height: 100%; position: absolute; background-color: transparent;"></div>
                                                    </div>
                                                </div>
                                                <div id="comp-ja78axmiinlineContent" class="strc1inlineContent">
                                                    <div class="strc1" id="comp-ja78axr8" style="position: absolute; left: calc((((((100vw - 17px) - 0px) * 0.5) + ((980px - ((100vw - 17px) - 0px)) / 2)) + 0px) + -490px); width: 980px; top: 0px; height: 211px;">
                                                        <div data-enable-video="true" class="strc1balata" id="comp-ja78axr8balata" style="position: absolute; top: 0px; width: calc((((100vw - 17px) - 0px) * 1) + 1px); height: 100%; pointer-events: auto; left: calc((((((100vw - 17px) - 0px) * 1) - 980px) * -0.5) - 1px); bottom: 0px; overflow: hidden; clip: rect(0px 1350px 211px 0px);">
                                                            <div class="bgColor" id="comp-ja78axr8balatabgcolor" style="position: absolute; width: 100%; height: 100%;">
                                                                <div id="comp-ja78axr8balatabgcoloroverlay" class="bgColoroverlay" style="width: 100%; height: 100%; position: absolute; background-color: rgb(97, 97, 97);"></div>
                                                            </div>
                                                            <div data-effect="none" data-fitting="tile" data-align="center" class="bgMedia" id="comp-ja78axr8balatamedia" style="position: absolute; pointer-events: auto; top: 0px; width: 1350px; left: 0px; height: 211px;">
                                                                <div class="bgImage" id="comp-ja78axr8balatamediacontent" style="width: 100%;">
                                                                    <div data-type="bgimage" id="comp-ja78axr8balatamediacontentimage" class="bgImageimage" style="position: absolute; width: 100%; height: 211px; background-size: auto; background-repeat: repeat; background-position: center center; background-image: url(&quot;<?php echo get_template_directory_uri();?>/assets/img/38567b0b5eb55f8eba96f60fa6356630.png&quot;);" data-image-css="{&quot;height&quot;:211,&quot;backgroundSize&quot;:&quot;auto&quot;,&quot;backgroundRepeat&quot;:&quot;repeat&quot;,&quot;backgroundPosition&quot;:&quot;center center&quot;}"></div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div id="comp-ja78axr8inlineContent" class="strc1inlineContent" style="position: absolute; width: 980px; top: 0px; bottom: 0px; left: calc((100% - 980px) * 0.5);">
                                                            <div data-exact-height="114" data-content-padding-horizontal="0" data-content-padding-vertical="0" title="" class="wp2" id="comp-ja78axrb" style="left: 489px; position: absolute; top: 62px; width: 167px; height: 114px;">
                                                                <div id="comp-ja78axrblink" class="wp2link" style="width: 167px; height: 114px;">
                                                                    <div data-style="" class="wp2img" id="comp-ja78axrbimg" style="position: relative; width: 167px; height: 114px;">
                                                                        <div class="wp2imgpreloader" id="comp-ja78axrbimgpreloader"></div><img id="comp-ja78axrbimgimage" alt="" data-type="image" style="width: 167px; height: 114px; object-fit: cover;" src="<?php echo get_template_directory_uri();?>/assets/img/9012a2_18014d15c22842bd85d44f27158fefc4_mv2.png"></div>
                                                                </div>
                                                            </div>
                                                            <div data-exact-height="114" data-content-padding-horizontal="0" data-content-padding-vertical="0" title="" class="wp2" id="comp-ja78axre" style="left: 322px; position: absolute; top: 63px; width: 167px; height: 114px;">
                                                                <div id="comp-ja78axrelink" class="wp2link" style="width: 167px; height: 114px;">
                                                                    <div data-style="" class="wp2img" id="comp-ja78axreimg" style="position: relative; width: 167px; height: 114px;">
                                                                        <div class="wp2imgpreloader" id="comp-ja78axreimgpreloader"></div><img id="comp-ja78axreimgimage" alt="" data-type="image" style="width: 167px; height: 114px; object-fit: cover;" src="<?php echo get_template_directory_uri();?>/assets/img/9012a2_5518b76acf4b4b9f88089bec4c7b5921_mv2.png"></div>
                                                                </div>
                                                            </div>
                                                            <div data-exact-height="114" data-content-padding-horizontal="0" data-content-padding-vertical="0" title="" class="wp2" id="comp-ja78axrg" style="left: 155px; position: absolute; top: 63px; width: 167px; height: 114px;">
                                                                <div id="comp-ja78axrglink" class="wp2link" style="width: 167px; height: 114px;">
                                                                    <div data-style="" class="wp2img" id="comp-ja78axrgimg" style="position: relative; width: 167px; height: 114px;">
                                                                        <div class="wp2imgpreloader" id="comp-ja78axrgimgpreloader"></div><img id="comp-ja78axrgimgimage" alt="" data-type="image" style="width: 167px; height: 114px; object-fit: cover;" src="<?php echo get_template_directory_uri();?>/assets/img/9012a2_dd75db8a9ae344c99a1d8baf82fba190_mv2.png"></div>
                                                                </div>
                                                            </div>
                                                            <div data-exact-height="114" data-content-padding-horizontal="0" data-content-padding-vertical="0" title="" class="wp2" id="comp-ja78axri" style="left: 0px; position: absolute; top: 63px; width: 167px; height: 114px;">
                                                                <div id="comp-ja78axrilink" class="wp2link" style="width: 167px; height: 114px;">
                                                                    <div data-style="" class="wp2img" id="comp-ja78axriimg" style="position: relative; width: 167px; height: 114px;">
                                                                        <div class="wp2imgpreloader" id="comp-ja78axriimgpreloader"></div><img id="comp-ja78axriimgimage" alt="" data-type="image" style="width: 167px; height: 114px; object-fit: cover;" src="<?php echo get_template_directory_uri();?>/assets/img/9012a2_4748ea0b291f4e37bce13ec0a3d12930_mv2.png"></div>
                                                                </div>
                                                            </div>
                                                            <div data-exact-height="114" data-content-padding-horizontal="0" data-content-padding-vertical="0" title="" class="wp2" id="comp-ja78axrx" style="left: 646px; position: absolute; top: 62px; width: 167px; height: 114px;">
                                                                <div id="comp-ja78axrxlink" class="wp2link" style="width: 167px; height: 114px;">
                                                                    <div data-style="" class="wp2img" id="comp-ja78axrximg" style="position: relative; width: 167px; height: 114px;">
                                                                        <div class="wp2imgpreloader" id="comp-ja78axrximgpreloader"></div><img id="comp-ja78axrximgimage" alt="" data-type="image" style="width: 167px; height: 114px; object-fit: cover;" src="<?php echo get_template_directory_uri();?>/assets/img/9012a2_8f04ad7a9c074e578724f9840d8001d3_mv2.png"></div>
                                                                </div>
                                                            </div>
                                                            <div data-exact-height="114" data-content-padding-horizontal="0" data-content-padding-vertical="0" title="" class="wp2" id="comp-ja78axrz" style="left: 813px; position: absolute; top: 62px; width: 167px; height: 114px;">
                                                                <div id="comp-ja78axrzlink" class="wp2link" style="width: 167px; height: 114px;">
                                                                    <div data-style="" class="wp2img" id="comp-ja78axrzimg" style="position: relative; width: 167px; height: 114px;">
                                                                        <div class="wp2imgpreloader" id="comp-ja78axrzimgpreloader"></div><img id="comp-ja78axrzimgimage" alt="" data-type="image" style="width: 167px; height: 114px; object-fit: cover;" src="<?php echo get_template_directory_uri();?>/assets/img/9012a2_2092244680614f2bbf5e6eb5a3dd2b34_mv2.png"></div>
                                                                </div>
                                                            </div>
                                                            <div class="txtNew" id="comp-ja78axs2" style="left: 26px; width: 304px; position: absolute; top: 22px;">
                                                                <h2 class="font_2"><span style="color:#FFFFFF;">¿Cómo Funciona?</span></h2></div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </section>
                                            <section class="strc1" id="comp-ja78cwjf" style="width: 980px; position: absolute; top: 952px; height: 186px; left: 0px;">
                                                <div data-enable-video="true" class="strc1balata" id="comp-ja78cwjfbalata" style="position: absolute; top: 0px; width: calc((100vw - 17px) - 0px); height: 100%; pointer-events: auto; bottom: 0px; left: calc(490px - (((100vw - 17px) - 0px) / 2)); right: calc(490px + (((100vw - 17px) - 0px) / 2)); overflow: hidden; clip: rect(0px 1349px 186px 0px);">
                                                    <div class="bgColor" id="comp-ja78cwjfbalatabgcolor" style="position: absolute; width: 100%; height: 100%;">
                                                        <div id="comp-ja78cwjfbalatabgcoloroverlay" class="bgColoroverlay" style="width: 100%; height: 100%; position: absolute; background-color: transparent;"></div>
                                                    </div>
                                                </div>
                                                <div id="comp-ja78cwjfinlineContent" class="strc1inlineContent">
                                                    <div class="mc1" id="comp-ja78cwo8" style="position: absolute; left: calc((((((100vw - 17px) - 0px) * 0.5) + ((980px - ((100vw - 17px) - 0px)) / 2)) + 0px) + -490px); width: 980px; top: 0px; height: 186px;">
                                                        <div id="comp-ja78cwo8container" class="mc1container">
                                                            <div data-enable-video="true" class="mc1balata" id="comp-ja78cwo8balata" style="position: absolute; top: 0px; width: calc((((100vw - 17px) - 0px) * 1) + 1px); height: 100%; pointer-events: auto; left: calc((((((100vw - 17px) - 0px) * 1) - 980px) * -0.5) - 1px); bottom: 0px; overflow: hidden; clip: rect(0px 1350px 186px 0px);">
                                                                <div class="bgColor" id="comp-ja78cwo8balatabgcolor" style="position: absolute; width: 100%; height: 100%;">
                                                                    <div id="comp-ja78cwo8balatabgcoloroverlay" class="bgColoroverlay" style="width: 100%; height: 100%; position: absolute; background-color: rgb(250, 250, 250);"></div>
                                                                </div>
                                                            </div>
                                                            <div id="comp-ja78cwo8inlineContentParent" class="mc1inlineContentParent" style="position: absolute; width: calc((((100vw - 17px) - 0px) * 1) + 1px); left: calc((((((100vw - 17px) - 0px) * 1) - 980px) * -0.5) - 1px); top: 0px; bottom: 0px;">
                                                                <div id="comp-ja78cwo8inlineContent" class="mc1inlineContent" style="position: absolute; width: 980px; top: 0px; bottom: 0px; left: calc((100% - 980px) * 0.5);">
                                                                    <div class="txtNew" id="comp-ja78eihw" style="left: 26px; width: 304px; position: absolute; top: 17px;">
                                                                        <h2 class="font_2" style="font-size:18px;"><span style="font-size:18px;"><span style="font-family:spinnaker,sans-serif;"><span style="font-weight:bold;"><span style="color:#000000;">¿Qué inspecciona TUCASA</span><span style="color:#FF6600;">OK</span><span style="color:#000000;">?</span></span></span></span></h2></div>
                                                                    <div data-exact-height="79" data-content-padding-horizontal="0" data-content-padding-vertical="0" title="termografia.PNG" class="wp2" id="comp-iur6ppp4" style="left: 699px; position: absolute; top: 53px; width: 80px; height: 79px;">
                                                                        <div id="comp-iur6ppp4link" class="wp2link" style="width: 80px; height: 79px;">
                                                                            <div data-style="" class="wp2img" id="comp-iur6ppp4img" style="position: relative; width: 80px; height: 79px;">
                                                                                <div class="wp2imgpreloader" id="comp-iur6ppp4imgpreloader"></div><img id="comp-iur6ppp4imgimage" alt="" data-type="image" style="width: 80px; height: 79px; object-fit: cover;" src="<?php echo get_template_directory_uri();?>/assets/img/9012a2_310741e7e6fb4973ba152e21da2a1e8c_mv2.png"></div>
                                                                        </div>
                                                                    </div>
                                                                    <div data-exact-height="79" data-content-padding-horizontal="0" data-content-padding-vertical="0" title="" class="wp2" id="comp-iuicqrms" style="left: 853px; position: absolute; top: 59px; width: 80px; height: 79px;">
                                                                        <div id="comp-iuicqrmslink" class="wp2link" style="width: 80px; height: 79px;">
                                                                            <div data-style="" class="wp2img" id="comp-iuicqrmsimg" style="position: relative; width: 80px; height: 79px;">
                                                                                <div class="wp2imgpreloader" id="comp-iuicqrmsimgpreloader"></div><img id="comp-iuicqrmsimgimage" alt="" data-type="image" style="width: 80px; height: 79px; object-fit: cover;" src="<?php echo get_template_directory_uri();?>/assets/img/9012a2_24efdd5eed5a4e61ad69196f5abf6da9_mv2.png"></div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="txtNew" id="comp-iuid4ngu" style="left: 829px; width: 140px; position: absolute; top: 156px;">
                                                                        <h2 class="font_2" style="text-align:center; font-size:12px;"><span style="font-size:12px;"><span style="color:#000000;"><span style="font-family:wfont_9012a2_eab04d8add6b4f4eb1026227d1089542,wf_eab04d8add6b4f4eb1026227d,orig_montserratlight;">TERMINACIONES</span></span></span></h2>
                                                                    </div>
                                                                    <div data-exact-height="80" data-content-padding-horizontal="0" data-content-padding-vertical="0" title="" class="wp2" id="comp-iuicqrmu1" style="left: 199px; position: absolute; top: 64px; width: 115px; height: 80px;">
                                                                        <div id="comp-iuicqrmu1link" class="wp2link" style="width: 115px; height: 80px;">
                                                                            <div data-style="" class="wp2img" id="comp-iuicqrmu1img" style="position: relative; width: 115px; height: 80px;">
                                                                                <div class="wp2imgpreloader" id="comp-iuicqrmu1imgpreloader"></div><img id="comp-iuicqrmu1imgimage" alt="" data-type="image" style="width: 115px; height: 80px; object-fit: cover;" src="<?php echo get_template_directory_uri();?>/assets/img/9012a2_b4d96d1080174b1eb4c243cbb438d789_mv2.png"></div>
                                                                        </div>
                                                                    </div>
                                                                    <div data-exact-height="66" data-content-padding-horizontal="0" data-content-padding-vertical="0" title="" class="wp2" id="comp-iuicqrmv" style="left: 556px; position: absolute; top: 70px; width: 77px; height: 66px;">
                                                                        <div id="comp-iuicqrmvlink" class="wp2link" style="width: 77px; height: 66px;">
                                                                            <div data-style="" class="wp2img" id="comp-iuicqrmvimg" style="position: relative; width: 77px; height: 66px;">
                                                                                <div class="wp2imgpreloader" id="comp-iuicqrmvimgpreloader"></div><img id="comp-iuicqrmvimgimage" alt="" data-type="image" style="width: 77px; height: 66px; object-fit: cover;" src="<?php echo get_template_directory_uri();?>/assets/img/9012a2_69ae765560e147468498d4ad31f35a68_mv2.png"></div>
                                                                        </div>
                                                                    </div>
                                                                    <div data-exact-height="80" data-content-padding-horizontal="0" data-content-padding-vertical="0" title="" class="wp2" id="comp-iuicqrmv1" style="left: 391px; position: absolute; top: 59px; width: 85px; height: 80px;">
                                                                        <div id="comp-iuicqrmv1link" class="wp2link" style="width: 85px; height: 80px;">
                                                                            <div data-style="" class="wp2img" id="comp-iuicqrmv1img" style="position: relative; width: 85px; height: 80px;">
                                                                                <div class="wp2imgpreloader" id="comp-iuicqrmv1imgpreloader"></div><img id="comp-iuicqrmv1imgimage" alt="" data-type="image" style="width: 85px; height: 80px; object-fit: cover;" src="<?php echo get_template_directory_uri();?>/assets/img/9012a2_7f90064d59a04195b9fb0c0de979e595_mv2.png"></div>
                                                                        </div>
                                                                    </div>
                                                                    <div data-exact-height="90" data-content-padding-horizontal="0" data-content-padding-vertical="0" title="" class="wp2" id="comp-iuicqrmu" style="left: 38px; position: absolute; top: 60px; width: 85px; height: 90px;">
                                                                        <div id="comp-iuicqrmulink" class="wp2link" style="width: 85px; height: 90px;">
                                                                            <div data-style="" class="wp2img" id="comp-iuicqrmuimg" style="position: relative; width: 85px; height: 90px;">
                                                                                <div class="wp2imgpreloader" id="comp-iuicqrmuimgpreloader"></div><img id="comp-iuicqrmuimgimage" alt="" data-type="image" style="width: 85px; height: 90px; object-fit: cover;" src="<?php echo get_template_directory_uri();?>/assets/img/9012a2_c48bc1a56a3a4bc58b99ec2499c2f7e6_mv2.png"></div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="txtNew" id="comp-iuid47fn" style="left: 0px; width: 162px; position: absolute; top: 157px;">
                                                                        <h2 class="font_2" style="text-align:center; font-size:12px;"><span style="font-size:12px;"><span style="color:#000000;"><span style="font-family:wfont_9012a2_eab04d8add6b4f4eb1026227d1089542,wf_eab04d8add6b4f4eb1026227d,orig_montserratlight;">DIMENSIONES</span></span></span></h2>
                                                                    </div>
                                                                    <div class="txtNew" id="comp-iuid4czy" style="left: 489px; width: 210px; position: absolute; top: 156px;">
                                                                        <h2 class="font_2" style="font-size:12px; text-align:center;"><span style="font-size:12px;"><span style="color:#000000;"><span style="font-family:wfont_9012a2_eab04d8add6b4f4eb1026227d1089542,wf_eab04d8add6b4f4eb1026227d,orig_montserratlight;">FILTRACIONES DE AGUA</span></span></span></h2>
                                                                    </div>
                                                                    <div class="txtNew" id="comp-ja792fc8" style="left: 657px; width: 180px; position: absolute; top: 156px;">
                                                                        <h2 class="font_2" style="font-size:12px; text-align:center;"><span style="font-size:12px;"><span style="color:#000000;"><span style="font-family:wfont_9012a2_eab04d8add6b4f4eb1026227d1089542,wf_eab04d8add6b4f4eb1026227d,orig_montserratlight;">TERMOGRAFÍA</span></span></span></h2></div>
                                                                    <div class="txtNew" id="comp-iuid2k9q" style="left: 346px; width: 190px; position: absolute; top: 156px;">
                                                                        <h2 class="font_2" style="font-size:12px; text-align:center;"><span style="font-size:12px;"><span style="color:#000000;"><span style="font-family:wfont_9012a2_eab04d8add6b4f4eb1026227d1089542,wf_eab04d8add6b4f4eb1026227d,orig_montserratlight;">ELECTRICIDÁD</span></span></span></h2></div>
                                                                    <div class="txtNew" id="comp-iuid4itp" style="left: 162px; width: 210px; position: absolute; top: 157px;">
                                                                        <h2 class="font_2" style="font-size:12px; text-align:center;"><span style="font-size:12px;"><span style="color:#000000;"><span style="font-family:wfont_9012a2_eab04d8add6b4f4eb1026227d1089542,wf_eab04d8add6b4f4eb1026227d,orig_montserratlight;">FUGAS DE GAS</span></span></span></h2></div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </section>
                                            <div id="comp-j532hjof" data-align="center" data-disabled="false" data-margin="0" data-should-use-flex="true" class="style-j532j2y6" data-state="desktop shouldUseFlex center" style="left: 790px; position: absolute; top: 862px; height: 27px; min-height: 19px; width: 190px;" data-prev-text="MANUAL DE TOLERANCIA" data-prev-min-width="186" data-prev-width="190"><a href="https://docs.wixstatic.com/ugd/9012a2_8b7538eab6164243b45f318c501a9ea7.pdf" target="_blank" data-type="document" role="button" id="comp-j532hjoflink" class="g-transparent-a style-j532j2y6link"><span id="comp-j532hjoflabel" class="style-j532j2y6label" style="">MANUAL DE TOLERANCIA</span></a></div>
                                            <div id="comp-j532kfkx" data-align="center" data-disabled="false" data-margin="0" data-should-use-flex="true" class="style-j532j2y6" data-state="desktop shouldUseFlex center" style="left: 790px; position: absolute; top: 893px; height: 27px; min-height: 19px; width: 190px;" data-prev-text="INFORME MODELO" data-prev-min-width="140" data-prev-width="190"><a href="https://docs.wixstatic.com/ugd/9012a2_5a8e26776fac45b8804d7ac5aa06b2ec.pdf" target="_blank" data-type="document" role="button" id="comp-j532kfkxlink" class="g-transparent-a style-j532j2y6link"><span id="comp-j532kfkxlabel" class="style-j532j2y6label" style="">INFORME MODELO</span></a></div>
                                            <div id="comp-j532gs4v" data-align="center" data-disabled="false" data-margin="0" data-should-use-flex="true" class="style-j532j2y6" data-state="desktop shouldUseFlex center" style="left: 790px; position: absolute; top: 828px; height: 27px; min-height: 19px; width: 190px;" data-prev-text="TUS DERECHOS" data-prev-min-width="118" data-prev-width="190"><a href="/blog/tag/Derechos" target="_blank" data-content="/blog/tag/Derechos" data-type="external" role="button" id="comp-j532gs4vlink" class="g-transparent-a style-j532j2y6link"><span id="comp-j532gs4vlabel" class="style-j532j2y6label" style="">TUS DERECHOS</span></a></div>
                                            <div id="comp-j532cqgq" data-align="center" data-disabled="false" data-margin="0" data-should-use-flex="true" class="style-iuq3miwd" data-state="desktop shouldUseFlex center" style="left: 790px; position: absolute; top: 782px; height: 28px; min-height: 19px; width: 190px;" data-prev-text="+569 4472 0173" data-prev-min-width="114" data-prev-width="190"><a href="tel:+56944720173" data-content="+56944720173" data-type="phone" role="button" id="comp-j532cqgqlink" class="g-transparent-a style-iuq3miwdlink"><span id="comp-j532cqgqlabel" class="style-iuq3miwdlabel" style="">+569 4472 0173</span></a></div>
                                            <div id="comp-j532bi0o" data-align="center" data-disabled="false" data-margin="0" data-should-use-flex="true" class="style-iuq3miwd" data-state="desktop shouldUseFlex center" style="left: 790px; position: absolute; top: 746px; height: 27px; min-height: 19px; width: 190px;" data-prev-text="CONTACTO / COTIZA" data-prev-min-width="154" data-prev-width="190"><a href="" target="_self" data-keep-roots="true" data-anchor="c18fq" role="button" id="comp-j532bi0olink" class="g-transparent-a style-iuq3miwdlink"><span id="comp-j532bi0olabel" class="style-iuq3miwdlabel" style="">CONTACTO / COTIZA</span></a></div>
                                            <div id="comp-ja78wdzp" data-align="center" data-disabled="false" data-margin="0" data-should-use-flex="true" class="style-iuq3miwd" data-state="desktop shouldUseFlex center" style="left: 790px; position: absolute; top: 711px; height: 27px; min-height: 19px; width: 190px;" data-prev-text="RESERVA TÚ HORA" data-prev-min-width="142" data-prev-width="190"><a href="/book-online" target="_self" role="button" id="comp-ja78wdzplink" class="g-transparent-a style-iuq3miwdlink"><span id="comp-ja78wdzplabel" class="style-iuq3miwdlabel" style="">RESERVA TÚ HORA</span></a></div>
                                            
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- react-empty: 860 -->
            <!-- react-empty: 861 -->
            <div class="siteAspectsContainer">
                <div></div>
                <div></div>
                <!-- react-empty: 865 -->
            </div>
            <!-- react-empty: 866 -->
            <!-- react-empty: 867 -->
            <!-- react-empty: 868 -->
        </div>
    </div>

    <!-- No Footer -->

</body>
<?php get_footer(); ?>