var is_mobile_agent=/Android|webOS|iPhone|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent);
var is_wix_mobile=(typeof Wix!="undefined"&&Wix.Utils.getDeviceType()=="mobile");
var is_mobile=is_mobile_agent||is_wix_mobile;
var main_scope;
function AppController(n,h,f){main_scope=n;
n.free_user=Inffuse.user.plan()=="free";
n.upgrade_required=(typeof Inffuse.project=="undefined");
n.items;
n.settings;
n.flags={};
n.pages=[];
n.current_page=0;
n.page_count;
n.max_rows;
n.uid;
n.template=null;
n.is_mobile=is_mobile;
var g=1*(new Date());
var b="This is a sample testimonial. All sample testimonials will disappear once the first real testimonial is added. Double click here to open the settings panel and get started!";
var i=[{author_image:"/img/faceui/f1.jpg",text:b,date:g,author_name:"Amie Williams",source:"custom"},{author_image:"/img/faceui/m1.jpg",text:b,date:g,author_name:"Dave Johnson",source:"custom"},{author_image:"/img/faceui/m2.jpg",text:b,date:g,author_name:"John Carrey",source:"custom"},{author_image:"/img/faceui/f2.jpg",text:b,date:g,author_name:"Natalie Roddick",source:"custom"}];
var o={collect:{},design:"simple2",cols:1,rows:2,bg1:"color-1",bg2:"color-8",color1:"color-5",color2:"color-5"};
var d=0;
n.$on("$includeContentRequested",function(){d++
});
n.$on("$includeContentLoaded",function(){d--;
if(d){return
}setTimeout(n.onContentChanged)
});
n.init=function(){n.settings=Inffuse.project?Inffuse.project.get("settings",o):o;
n.flags=Inffuse.project?Inffuse.project.get("flags",{}):{};
n.items=Inffuse.project?angular.copy(Inffuse.project.get("items")):null;
if(!n.items||!n.items.length){n.items=i
}if(n.flags.max_items){MAX_ITEMS=n.flags.max_items
}if(n.free_user){n.settings.slider={};
n.settings.collect={};
if(n.items.length>MAX_ITEMS){n.items=n.items.slice(0,MAX_ITEMS)
}}n.items.forEach(function(z,y){if(z.author_image){if(z.author_image=="http://www.inffuse.com/static/img/profile.png"){z.author_image="https://inffuse-testimonials.appspot.com/img/profile.png"
}else{z.author_image=z.author_image.replace("http://graph.facebook.com/","https://graph.facebook.com/")
}}if(z.source=="yelp"&&z.link){z.link=z.link.replace("http://www.yelp.com/biz/https:","https:")
}});
var p=JSON.parse(n.settings.rotate||false);
n.settings.slider=n.settings.slider||{autoplay:p,pager:false,pause:n.settings.rotationDuration,transition:"horizontal"};
n.settings.bg1=n.settings.bg1||o.bg1;
n.settings.bg2=n.settings.bg2||o.bg2;
n.settings.color1=n.settings.color1||o.color1;
n.settings.color2=n.settings.color2||o.color2;
n.settings.direction=n.settings.direction||"ltr";
if(typeof n.settings.bg1=="string"&&n.settings.bg1.indexOf("rgb(")==0&&typeof n.settings.bg1opacity!="undefined"){n.settings.bg1={color:n.settings.bg1,opacity:n.settings.bg1opacity}
}if(typeof n.settings.bg2=="string"&&n.settings.bg2.indexOf("rgb(")==0&&typeof n.settings.bg2opacity!="undefined"){n.settings.bg2={color:n.settings.bg2,opacity:n.settings.bg2opacity}
}n.settings.bg1=renderColor(n.settings.bg1);
n.settings.bg2=renderColor(n.settings.bg2);
n.settings.color1=renderColor(n.settings.color1);
n.settings.color2=renderColor(n.settings.color2);
if(n.settings.collect&&n.settings.collect.button){n.settings.collect.button.bg=renderColor(n.settings.collect.button.bg,1);
n.settings.collect.button.color=renderColor(n.settings.collect.button.color,1);
n.settings.collect.button.pos=n.settings.collect.button.pos||"bottom"
}if(Inffuse.platform=="wix"&&typeof Wix!="undefined"){function q(y){if(typeof y=="object"){return pair2color(y)
}if(y.substr(0,6)!="color-"){return y
}return Wix.Styles.getColorByreference(y).value
}Wix.Styles.getStyleParams(function(y){n.settings.bg1=q(n.settings.bg1);
n.settings.bg2=q(n.settings.bg2);
n.settings.color1=q(n.settings.color1);
n.settings.color2=q(n.settings.color2);
if(n.settings.collect&&n.settings.collect.button){n.settings.collect.button.bg=q(n.settings.collect.button.bg);
n.settings.collect.button.color=q(n.settings.collect.button.color)
}})
}switch(n.settings.order){case"random":shuffle(n.items);
break;
case"date-desc":case"date-asc":n.items=n.items.sort(function(z,y){var A=(n.settings.order=="date-desc")?-1:1;
if(z.date<y.date){return -1*A
}if(z.date>y.date){return 1*A
}return 0
});
break
}if(n.settings.direction=="rtl"){n.items.reverse()
}var u=n.settings.rows;
var t=n.settings.cols;
if(n.is_mobile){var r=(u==0||n.sliderEnabled())?n.items.length:u*t;
n.max_rows=Math.min(r,2);
n.show_more_button=r>n.max_rows;
n.settings.cols=1;
n.settings.slider.pager=false;
n.settings.slider.arrows=false;
n.settings.slider.autoplay=false;
var x=n.items.map(function(y){return[y]
});
n.pages=[x]
}else{var s=Math.ceil(n.items.length/t);
var x=[];
for(var w=0;
w<s;
w++){x.push(n.items.slice(t*w,t*(w+1)))
}n.pages=[];
if(u==0){n.pages=[x]
}else{n.page_count=1;
if(n.sliderEnabled()){n.page_count=Math.ceil(s/u)
}for(var v=0;
v<n.page_count;
v++){n.pages.push(x.slice(u*v,u*(v+1)))
}}if(n.pages.length==1){n.settings.slider.autoplay=false
}}n.collect_button=n.settings.collect&&n.settings.collect.enabled;
n.template=n.settings.design;
if(!n.$$phase){n.$apply()
}};
n.setTemplatePreview=function(p){n.template=p||n.settings.design;
if(!n.$$phase){n.$apply()
}};
n.openForm=function(){Inffuse.utils.openPopup({url:document.location.href.replace("widget.html","form.html"),width:650,height:650,onClose:function(){}});
InffuseAnalytics.init("bd6bc073d2f5e0cd5086a1f1bf12585e",n.uid);
InffuseAnalytics.track("Collect Testimonials")
};
n.sliderEnabled=function(){if(!n.settings){return false
}if(!n.settings.slider){return false
}return n.settings.slider.pager||n.settings.slider.arrows||n.settings.slider.autoplay
};
function m(p){p.css("height","");
var q=0;
p.each(function(r,s){q=Math.max(q,$(s).height())
});
p.css("height",q)
}function j(){var p=!n.flags.dont_align&&!n.is_mobile&&(n.settings.cols>1||n.sliderEnabled());
if(p){var q=$(".align-height");
m(q);
n.onContentSizeChanged()
}if(n.settings.slider.arrows){var s;
if(n.settings.rows!=1){s=$(".bx-viewport")
}else{s=$(".current-slide .arrows-align:first");
if(!s.length){s=$(".bx-viewport")
}}if(s.length){var r=$(".arrows");
r.css("top",s.offset().top);
r.height(s.outerHeight(false))
}}}function l(){var p=0;
$(".testimonials-page").each(function(q,s){var r=$(s).outerHeight();
p=Math.max(p,r)
});
$(".testimonials").height(p);
Inffuse.project.updateHeight()
}function a(){var q=$(".current-slide .testimonials-item:first");
if(!q.length){q=$(".testimonials-item:first")
}var r=q.width();
var p="";
if(r>=300&&r<400){p="medium"
}else{if(r<300){p="small"
}}$("body").removeClass("size-small");
$("body").removeClass("size-medium");
$("body").removeClass("size-big");
$("body").addClass("size-"+p)
}function e(){a();
j();
l()
}n.onContentSizeChanged=function(){l()
};
n.onContentChanged=function(){a();
j();
l()
};
n.opacity=function(p,q){if(!p){return p
}if(p[0]=="#"){p=p.substr(1);
shorthand=(p.length==3);
if(shorthand){colors=[p[0]+p[0],p[1]+p[1],p[2]+p[2]]
}else{colors=[p.substr(0,2),p.substr(2,2),p.substr(4,2)]
}colors=colors.map(function(r){return parseInt(r,16)
});
colors.push(q);
p="rgba("+colors.join(",")+")"
}else{if(p.indexOf("rgb")==0){p=p.replace("rgb(","");
p=p.replace("rgba(","");
p=p.replace(")","");
p=p.replace(" ","");
colors=p.split(",").slice(0,3);
colors.push(q);
p="rgba("+colors.join(",")+")"
}else{if(p.indexOf("hsl")==0){p=p.replace("hsl(","");
p=p.replace("hsla(","");
p=p.replace(")","");
p=p.replace(" ","");
colors=p.split(",").slice(0,3);
colors.push(q);
p="hsla("+colors.join(",")+")"
}}}return p
};
n.toggleShowMore=function(){n.$root._expanded=!n.$root._expanded;
setTimeout(function(){n.onContentSizeChanged()
})
};
n.logTiming=function(p){};
var c={};
function k(p,q){if(c[p]){clearTimeout(c[p])
}c[p]=setTimeout(function(){delete c[p];
p()
},q)
}$(window).resize(function(){k(e,200)
})
}angular.module("App",["ngAnimate"]).controller("AppController",["$scope","$http","$interval",AppController]).config(["$interpolateProvider",function(a){a.startSymbol("[[");
a.endSymbol("]]")
}]).run(["$templateCache","$http",function(a,c){if(Inffuse.viewMode()=="editor"){var b=["simple2","bubbledown","simplequote","simplequotenp","bubbleup2","circlelarge","circlelargenb","triangleup","bubbleup3","bubbleupnp","largequote","bubbleright"];
b.forEach(function(d){c.get("templates/"+d+".html",{cache:a})
})
}}]).filter("safe",["$sce",function(a){return function(b){return a.trustAsHtml(b)
}
}]).directive("bxSlider",[function(){return{restrict:"A",require:"bxSlider",priority:0,controller:function(){},link:function(c,b,a,e){var d;
e.update=function(){if(d){d.destroySlider()
}var f=b.find("[bx-slider-item]").length;
if(f<=1){$(".container").css("visibility","visible");
$(".testimonials-page").removeClass("hidden");
return
}var h=c.$eval(a.sliderOptions)||{};
var j=c.$eval(a.direction)||"ltr";
var i=h.arrow_type||"angle";
var g={auto:h.autoplay,autoHover:true,pause:h.pause*1000,pager:!!h.pager,mode:h.transition||"horizontal",controls:h.arrows,nextSelector:"#arrow-right",prevSelector:"#arrow-left",nextText:h.arrows?'<span class="fa fa-'+i+'-right"></span>':"",prevText:h.arrows?'<span class="fa fa-'+i+'-left"></span>':"",autoDirection:j=="rtl"?"prev":"next",startSlide:j=="rtl"?f-1:0,onSliderLoad:function(k){$(".container").css("visibility","visible");
$(".testimonials-page:not(.bx-clone):eq("+k+")").addClass("current-slide");
$(".testimonials-page:not(.bx-clone):eq("+k+")").removeClass("hidden");
c.onContentSizeChanged();
c.$apply()
},onSlideBefore:function(m,l,k){m.removeClass("hidden");
$(".current-slide").removeClass("current-slide");
m.addClass("current-slide")
},onSlideAfter:function(m,l,k){$(".testimonials-page:not(.bx-clone):eq("+l+")").addClass("hidden")
}};
d=b.bxSlider(g)
}
}}
}]).directive("bxSliderItem",[function(){return{require:"^bxSlider",link:function(c,d,b,a){c.loaded=function(e){if(!e){return
}setTimeout(function(){c.onContentChanged();
setTimeout(function(){a.update()
})
})
}
}}
}]);
var app="testimonials";
var server="//inffuse-platform.appspot.com";
var inffuseSDK=new InffuseSDK_01(app);
inffuseSDK.server=server;
inffuseSDK.init(function(b){window.Inffuse=b;
$("#app-loading").remove();
function a(){InffuseAnalytics.init(b.user.id());
if(b.user.isNew()){InffuseAnalytics.track("User created")
}angular.element(document).ready(function(){angular.bootstrap(document,["App"]);
main_scope.init();
b.on("data-changed",main_scope.init);
b.on("project-deleted",InffuseAnalytics.trackHandler("App deleted"));
b.on("template-preview",function(c){main_scope.setTemplatePreview(c.id);
main_scope.$apply()
})
})
}a();
(function(e,f,k,j,h,d,c){e.GoogleAnalyticsObject=h;
e[h]=e[h]||function(){(e[h].q=e[h].q||[]).push(arguments)
},e[h].l=1*new Date();
d=f.createElement(k),c=f.getElementsByTagName(k)[0];
d.async=1;
d.src=j;
c.parentNode.insertBefore(d,c)
})(window,document,"script","//www.google-analytics.com/analytics.js","ga");
ga("create","UA-39145762-1","auto");
ga("set","dimension1",b.user.id());
ga("set","dimension2",b.site.id());
ga("set","dimension3",b.project?b.project.id():"None");
ga("set","dimension4",b.user.plan());
ga("send","pageview")
});
function shuffle(d){var c=d.length,b,a;
while(0!==c){a=Math.floor(Math.random()*c);
c-=1;
b=d[c];
d[c]=d[a];
d[a]=b
}return d
}function hex2rgba(d,c){var a=/^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(d);
if(!a){a=/^#?([a-f\d])([a-f\d])([a-f\d])$/i.exec(d)
}if(!a){return null
}a.shift();
var b=a.map(function(e){return parseInt((e.length==1?e+e:e),16)
});
if(c==1||typeof c=="undefined"){return"rgb("+b.join(",")+")"
}b.push(c);
return"rgba("+b.join(",")+")"
}function renderColor(a,e){if(typeof a=="string"){if(a.substr(0,6)=="color-"){a=Wix.Styles.getColorByreference(a).value
}return a
}var b=e||a.opacity;
a=a.color;
if(typeof a=="undefined"){return
}if(typeof b=="undefined"){b=1
}if(a.substr(0,6)=="color-"){a=Wix.Styles.getColorByreference(a).value
}if(a[0]=="#"){return hex2rgba(a,b)
}var c=a.substr(0,a.indexOf("("));
var d=a.substr(c.length+1,a.length-c.length-2).split(",");
if(d.length==3){d.push(b)
}else{d[3]=b
}return"rgba("+d.join(",")+")"
}function onContentSizeChanged(){if(typeof main_scope!="undefined"){main_scope.onContentSizeChanged()
}};