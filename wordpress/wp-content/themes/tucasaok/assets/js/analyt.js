var InffuseAnalytics=new function(){var a=this;
this.initialized=false;
this.rand_factor=null;
this.init=function(c){if(a.initialized){return
}a.rand_factor=(1*c.substr(c.length-10,10).split("").map(function(d){return d.charCodeAt(0)
}).reduce(function(e,d){return e+d
},"")%1000)/10;
a.initMixpanel(c);
if(typeof Inffuse!="undefined"){var b=Inffuse.user.plan()=="free"?2:4;
if(a.randFactor(b)){a.initJaco(c)
}}a.initialized=true
};
this.randFactor=function(b){return a.rand_factor<b
};
this.initMixpanel=function(c){(function(s,v){if(!v.__SV){var u=window;
try{var t,f,p,o=u.location,r=o.hash;
t=function(g,e){return(f=g.match(RegExp(e+"=([^&]*)")))?f[1]:null
};
r&&t(r,"state")&&(p=JSON.parse(decodeURIComponent(t(r,"state"))),"mpeditor"===p.action&&(u.sessionStorage.setItem("_mpcehash",r),history.replaceState(p.desiredHash||"",s.title,o.pathname+o.search)))
}catch(d){}var n,q;
window.mixpanel=v;
v._i=[];
v.init=function(g,k,h){function i(e,l){var m=l.split(".");
2==m.length&&(e=e[m[0]],l=m[1]);
e[l]=function(){e.push([l].concat(Array.prototype.slice.call(arguments,0)))
}
}var j=v;
"undefined"!==typeof h?j=v[h]=[]:h="mixpanel";
j.people=j.people||[];
j.toString=function(e){var l="mixpanel";
"mixpanel"!==h&&(l+="."+h);
e||(l+=" (stub)");
return l
};
j.people.toString=function(){return j.toString(1)+".people (stub)"
};
n="disable time_event track track_pageview track_links track_forms register register_once alias unregister identify name_tag set_config reset people.set people.set_once people.increment people.append people.union people.track_charge people.clear_charges people.delete_user".split(" ");
for(q=0;
q<n.length;
q++){i(j,n[q])
}v._i.push([g,k,h])
};
v.__SV=1.2;
u=s.createElement("script");
u.type="text/javascript";
u.async=!0;
u.src="undefined"!==typeof MIXPANEL_CUSTOM_LIB_URL?MIXPANEL_CUSTOM_LIB_URL:"file:"===s.location.protocol&&"//cdn.mxpnl.com/libs/mixpanel-2-latest.min.js".match(/^\/\//)?"https://cdn.mxpnl.com/libs/mixpanel-2-latest.min.js":"//cdn.mxpnl.com/libs/mixpanel-2-latest.min.js";
t=s.getElementsByTagName("script")[0];
t.parentNode.insertBefore(u,t)
}})(document,window.mixpanel||[]);
var b="bd6bc073d2f5e0cd5086a1f1bf12585e";
mixpanel.init(b);
mixpanel.identify(c)
};
this.initJaco=function(b){(function(j,f){function d(l,i){l[i]=function(){l.push([i].concat(Array.prototype.slice.call(arguments,0)))
}
}function g(){var m=f.location.hostname.match(/[a-z0-9][a-z0-9\-]+\.[a-z\.]{2,6}$/i),o=m?m[0]:null,l="; domain=."+o+"; path=/";
f.referrer&&f.referrer.indexOf(o)===-1?f.cookie="jaco_referer="+f.referrer+l:f.cookie="jaco_referer="+h+l
}var k="JacoRecorder",h="none";
(function(v,p,q,w){if(!q.__VERSION){v[k]=q;
var n=["init","identify","startRecording","stopRecording","removeUserTracking","setUserInfo"];
for(var m=0;
m<n.length;
m++){d(q,n[m])
}g(),q.__VERSION=2.1,q.__INIT_TIME=1*new Date;
var s=p.createElement("script");
s.async=!0,s.setAttribute("crossorigin","anonymous"),s.src=w;
var i=p.getElementsByTagName("head")[0];
i.appendChild(s)
}})(j,f,j[k]||[],"https://recorder-assets.getjaco.com/recorder_v2.js")
}).call(window,window,document),window.JacoRecorder.push(["init","c2e5b5a8-901d-4506-acd5-7e212e7ba47b",{}]);
window.JacoRecorder.identify(b,function c(d){});
window.JacoRecorder.push(["session.setAttribute",{attributeName:"Platform",attributeValue:Inffuse.platform}]);
window.JacoRecorder.push(["session.setAttribute",{attributeName:"Plan",attributeValue:Inffuse.user.plan()}])
};
this.track=function(c,b,d){if(typeof b=="undefined"){b={}
}if(typeof Inffuse!="undefined"){b._Platform=Inffuse.platform
}b.Version="newUI";
if(typeof mixpanel!="undefined"&&mixpanel){mixpanel.track(c,b,d)
}if(typeof _cio!="undefined"&&_cio){_cio.track(c,b)
}};
this.trackHandler=function(c,b){return(function(e,d){return function(){a.track(e,d)
}
})(c,b)
};
this.tag=function(c,e,d){var b=c;
if(e||d){b=[e,d].join(" ")
}mixpanel.name_tag(b);
mixpanel.people.set({"$email":c,"$name":b})
};
this.register=function(b){mixpanel.register(b)
}
};