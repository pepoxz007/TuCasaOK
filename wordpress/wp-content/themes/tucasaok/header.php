<!DOCTYPE html>
<html lang="en" class="">

<head>
    <meta http-equiv="X-UA-Compatible" content="IE=Edge">

    <meta charset="utf-8">

    <title>inspección de propiedades</title>


    <link rel="shortcut icon" href="<?php echo get_template_directory_uri();?>/assets/img/9012a2_d22b2dd3921b42f78ba8933cb5b48d41.jpg" type="image/jpeg">
    <link rel="apple-touch-icon" href="<?php echo get_template_directory_uri();?>/assets/img/9012a2_d22b2dd3921b42f78ba8933cb5b48d41.jpg" type="image/jpeg">
    

    <script type="text/javascript">
        var santaBase = 'https://static.parastorage.com/services/santa/1.2682.22';
        var clientSideRender = true;
    </script>




    <link rel="preconnect" href="https://static.wixstatic.com/" crossorigin="">
    <link rel="preconnect" href="//fonts.googleapis.com" crossorigin="">

    <meta http-equiv="X-Wix-Renderer-Server" content="app-jvm-12-200.42.wixprod.net">
    <meta http-equiv="X-Wix-Meta-Site-Id" content="cbba464f-73d7-4ff4-b85c-9e3373293aef">
    <meta http-equiv="X-Wix-Application-Instance-Id" content="1ebbaad7-e0fb-4281-8a99-e1a7bbdf0dff">
    <meta http-equiv="X-Wix-Published-Version" content="352">

    <meta http-equiv="etag" content="c427dac5b9c23e948618100caa0443e4">

    <meta property="og:type" content="website">

    <meta property="og:site_name" content="inspección de propiedades">
    <meta property="fb:admins" content="pauli.salas">
    <meta name="SKYPE_TOOLBAR" content="SKYPE_TOOLBAR_PARSER_COMPATIBLE">

    <meta id="wixMobileViewport" name="viewport" content="width=980, user-scalable=yes">

    <script>
        // BEAT MESSAGE
        try {
            window.wixBiSession = {
                initialTimestamp: Date.now(),
                viewerSessionId: 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
                    var r = Math.random() * 16 | 0,
                        v = c == 'x' ? r : (r & 0x3 | 0x8);
                    return v.toString(16);
                })
            };
            (new Image()).src = 'https://frog.wix.com/bt?src=29&evid=3&pn=1&et=1&v=1.2682.22&msid=cbba464f-73d7-4ff4-b85c-9e3373293aef&vsi=' + wixBiSession.viewerSessionId +
                '&url=' + encodeURIComponent(location.href.replace(/^http(s)?:\/\/(www\.)?/, '')) +
                '&isp=1&st=2&ts=0&iss=0&c=' + wixBiSession.initialTimestamp;
        } catch (e) {}
        // BEAT MESSAGE END
    </script>

    <meta name="fragment" content="!">

    <!-- DATA -->
    <script type="text/javascript">
        var adData = {};
        var mobileAdData = {};
        var usersDomain = "https://users.wix.com/wix-users";
    </script>
    
    
    
    
    
    
    
    
    <style type="text/css">
        a,
        abbr,
        acronym,
        address,
        applet,
        b,
        big,
        blockquote,
        body,
        button,
        caption,
        center,
        cite,
        code,
        dd,
        del,
        dfn,
        div,
        dl,
        dt,
        em,
        fieldset,
        font,
        footer,
        form,
        h1,
        h2,
        h3,
        h4,
        h5,
        h6,
        header,
        html,
        i,
        iframe,
        img,
        ins,
        kbd,
        label,
        legend,
        li,
        nav,
        object,
        ol,
        p,
        pre,
        q,
        s,
        samp,
        section,
        small,
        span,
        strike,
        strong,
        sub,
        sup,
        table,
        tbody,
        td,
        tfoot,
        th,
        thead,
        title,
        tr,
        tt,
        u,
        ul,
        var {
            margin: 0;
            padding: 0;
            border: 0;
            outline: 0;
            vertical-align: baseline;
            background: transparent
        }
        
        body {
            font-size: 10px;
            font-family: Arial, Helvetica, sans-serif
        }
        
        input,
        select,
        textarea {
            font-family: Helvetica, Arial, sans-serif;
            box-sizing: border-box
        }
        
        ol,
        ul {
            list-style: none
        }
        
        blockquote,
        q {
            quotes: none
        }
        
        ins {
            text-decoration: none
        }
        
        del {
            text-decoration: line-through
        }
        
        table {
            border-collapse: collapse;
            border-spacing: 0
        }
        
        a {
            cursor: pointer;
            text-decoration: none
        }
        
        body,
        html {
            height: 100%
        }
        
        body {
            overflow-x: auto;
            overflow-y: scroll
        }
        
        .testStyles {
            overflow-y: hidden
        }
        
        .reset-button {
            background: none;
            border: 0;
            outline: 0;
            color: inherit;
            font: inherit;
            line-height: normal;
            overflow: visible;
            padding: 0;
            -webkit-appearance: none;
            -webkit-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none
        }
        
        :focus {
            outline: none
        }
        
        .wixSiteProperties {
            -webkit-font-smoothing: antialiased;
            -moz-osx-font-smoothing: grayscale;
            overflow: hidden
        }
        
        .SITE_ROOT {
            min-height: 100%;
            position: relative;
            margin: 0 auto
        }
        
        .POPUPS_ROOT {
            left: 0;
            top: 0;
            width: 100%;
            height: 100%;
            overflow-x: auto;
            overflow-y: scroll;
            position: fixed;
            z-index: 99999
        }
        
        .POPUPS_ROOT.mobile {
            z-index: 1005
        }
        
        .POPUPS_WRAPPER {
            position: relative;
            overflow: hidden
        }
        
        .POPUPS_WRAPPER>div {
            margin: 0 auto
        }
        
        .auto-generated-link {
            color: inherit
        }
        
        .warmup .hidden-on-warmup {
            visibility: hidden
        }
        
        html.device-phone body {
            overflow-y: auto
        }
        
        html.device-mobile-optimized.device-android {
            margin-bottom: 1px
        }
        
        html.device-mobile-optimized.blockSiteScrolling>body {
            position: fixed;
            width: 100%
        }
        
        html.device-mobile-optimized.media-zoom-mode>body {
            touch-action: manipulation
        }
        
        html.device-mobile-optimized.media-zoom-mode>body>#SITE_CONTAINER {
            height: 100%;
            overflow: hidden
        }
        
        html.device-mobile-optimized.media-zoom-mode>body>#SITE_CONTAINER>.noop {
            height: 100%
        }
        
        html.device-mobile-optimized.media-zoom-mode>body>#SITE_CONTAINER>.noop>.siteAspectsContainer {
            height: 100%;
            z-index: 1005
        }
        
        .siteAspectsContainer {
            position: absolute;
            top: 0;
            margin: 0 auto;
            left: 0;
            right: 0
        }
        
        body.device-mobile-optimized {
            overflow-x: hidden;
            overflow-y: scroll
        }
        
        body.device-mobile-optimized.qa-mode {
            overflow-y: auto
        }
        
        body.device-mobile-optimized #SITE_CONTAINER {
            width: 320px;
            overflow-x: visible;
            margin: 0 auto;
            position: relative
        }
        
        body.device-mobile-optimized>* {
            max-width: 100%!important
        }
        
        body.device-mobile-optimized .SITE_ROOT {
            overflow-x: hidden;
            overflow-y: hidden
        }
        
        body.device-mobile-non-optimized #SITE_CONTAINER>:not(.mobile-non-optimized-overflow) .SITE_ROOT {
            overflow-x: hidden;
            overflow-y: auto
        }
        
        body.device-mobile-non-optimized.fullScreenMode {
            background-color: #5f6360
        }
        
        body.device-mobile-non-optimized.fullScreenMode #MOBILE_ACTIONS_MENU,
        body.device-mobile-non-optimized.fullScreenMode #SITE_BACKGROUND,
        body.device-mobile-non-optimized.fullScreenMode .SITE_ROOT {
            visibility: hidden
        }
        
        body.fullScreenMode {
            overflow-x: hidden!important;
            overflow-y: hidden!important
        }
        
        body.fullScreenMode.device-mobile-optimized #TINY_MENU {
            opacity: 0;
            pointer-events: none
        }
        
        body.fullScreenMode-scrollable.device-mobile-optimized {
            overflow-x: hidden!important;
            overflow-y: auto!important
        }
        
        body.fullScreenMode-scrollable.device-mobile-optimized #masterPage,
        body.fullScreenMode-scrollable.device-mobile-optimized .SITE_ROOT {
            overflow-x: hidden!important;
            overflow-y: hidden!important
        }
        
        body.fullScreenMode-scrollable.device-mobile-optimized #masterPage,
        body.fullScreenMode-scrollable.device-mobile-optimized #SITE_BACKGROUND {
            height: auto!important
        }
        
        .fullScreenOverlay {
            z-index: 1005;
            position: fixed;
            left: 0;
            top: -60px;
            right: 0;
            bottom: 0;
            display: -webkit-box;
            display: -webkit-flex;
            display: flex;
            -webkit-box-pack: center;
            -webkit-justify-content: center;
            justify-content: center;
            overflow-y: hidden
        }
        
        .fullScreenOverlay>.fullScreenOverlayContent {
            margin: 0 auto;
            position: absolute;
            right: 0;
            top: 60px;
            left: 0;
            bottom: 0;
            overflow: hidden;
            -webkit-transform: translateZ(0);
            transform: translateZ(0)
        }
        
        .mobile-actions-menu-wrapper {
            z-index: 1000
        }
        
        body[contenteditable] {
            overflow-x: auto;
            overflow-y: auto
        }
        
        .bold {
            font-weight: 700
        }
        
        .italic {
            font-style: italic
        }
        
        .underline {
            text-decoration: underline
        }
        
        .lineThrough {
            text-decoration: line-through
        }
        
        .singleLineText {
            white-space: nowrap;
            text-overflow: ellipsis
        }
        
        .alignLeft {
            text-align: left
        }
        
        .alignCenter {
            text-align: center
        }
        
        .alignRight {
            text-align: right
        }
        
        .alignJustify {
            text-align: justify
        }
        
        ol.font_100,
        ul.font_100 {
            color: #080808;
            font-family: "Arial, Helvetica, sans-serif", serif;
            font-size: 10px;
            font-style: normal;
            font-variant: normal;
            font-weight: 400;
            margin: 0;
            text-decoration: none;
            line-height: normal;
            letter-spacing: normal
        }
        
        ol.font_100 li,
        ul.font_100 li {
            margin-bottom: 12px
        }
        
        letter {
            position: relative
        }
        
        letter,
        word {
            display: inline-block
        }
        
        word {
            white-space: nowrap
        }
        
        letter.space,
        word.space {
            display: inline
        }
        
        ol.wix-list-text-align,
        ul.wix-list-text-align {
            list-style-position: inside
        }
        
        ol.wix-list-text-align h1,
        ol.wix-list-text-align h2,
        ol.wix-list-text-align h3,
        ol.wix-list-text-align h4,
        ol.wix-list-text-align h5,
        ol.wix-list-text-align h6,
        ol.wix-list-text-align p,
        ul.wix-list-text-align h1,
        ul.wix-list-text-align h2,
        ul.wix-list-text-align h3,
        ul.wix-list-text-align h4,
        ul.wix-list-text-align h5,
        ul.wix-list-text-align h6,
        ul.wix-list-text-align p {
            display: inline
        }
        
        .wixapps-less-spacers-align.ltr {
            text-align: left
        }
        
        .wixapps-less-spacers-align.center {
            text-align: center
        }
        
        .wixapps-less-spacers-align.rtl {
            text-align: right
        }
        
        .wixapps-less-spacers-align>a,
        .wixapps-less-spacers-align>div {
            display: inline-block!important
        }
        
        .flex_display {
            display: -webkit-box;
            display: -webkit-flex;
            display: flex
        }
        
        .flex_vbox {
            box-sizing: border-box;
            padding-top: .01em;
            padding-bottom: .01em
        }
        
        a.wixAppsLink img {
            cursor: pointer
        }
        
        .singleLine {
            white-space: nowrap;
            display: block;
            overflow: hidden;
            text-overflow: ellipsis;
            word-wrap: normal
        }
        
        [data-z-counter] {
            z-index: 0
        }
        
        [data-z-counter="0"] {
            z-index: auto
        }
        
        .circle-preloader {
            -webkit-animation: semi-rotate 1s 1ms linear infinite;
            animation: semi-rotate 1s 1ms linear infinite;
            height: 30px;
            left: 50%;
            margin-left: -15px;
            margin-top: -15px;
            overflow: hidden;
            position: absolute;
            top: 50%;
            -webkit-transform-origin: 100% 50%;
            transform-origin: 100% 50%;
            width: 15px
        }
        
        .circle-preloader:before {
            content: "";
            top: 0;
            left: 0;
            right: -100%;
            bottom: 0;
            border: 3px solid currentColor;
            border-color: currentColor transparent transparent currentColor;
            border-radius: 50%;
            position: absolute;
            -webkit-transform: rotate(-45deg);
            transform: rotate(-45deg);
            -webkit-animation: inner-rotate .5s 1ms linear infinite alternate;
            animation: inner-rotate .5s 1ms linear infinite alternate;
            color: #7fccf7
        }
        
        .circle-preloader:after {
            content: "";
            top: 0;
            left: 0;
            right: -100%;
            bottom: 0;
            border: 3px solid currentColor;
            border-color: currentColor transparent transparent currentColor;
            border-radius: 50%;
            position: absolute;
            -webkit-transform: rotate(-45deg);
            transform: rotate(-45deg);
            -webkit-animation: inner-rotate .5s 1ms linear infinite alternate;
            animation: inner-rotate .5s 1ms linear infinite alternate;
            color: #3899ec;
            opacity: 0
        }
        
        .circle-preloader.white:before {
            color: #f0f0f0
        }
        
        .circle-preloader.white:after {
            color: #dcdcdc
        }
        
        @-webkit-keyframes inner-rotate {
            to {
                opacity: 1;
                -webkit-transform: rotate(115deg);
                transform: rotate(115deg)
            }
        }
        
        @keyframes inner-rotate {
            to {
                opacity: 1;
                -webkit-transform: rotate(115deg);
                transform: rotate(115deg)
            }
        }
        
        @-webkit-keyframes semi-rotate {
            0% {
                -webkit-animation-timing-function: ease-out;
                animation-timing-function: ease-out;
                -webkit-transform: rotate(180deg);
                transform: rotate(180deg)
            }
            45% {
                -webkit-transform: rotate(198deg);
                transform: rotate(198deg)
            }
            55% {
                -webkit-transform: rotate(234deg);
                transform: rotate(234deg)
            }
            to {
                -webkit-transform: rotate(540deg);
                transform: rotate(540deg)
            }
        }
        
        @keyframes semi-rotate {
            0% {
                -webkit-animation-timing-function: ease-out;
                animation-timing-function: ease-out;
                -webkit-transform: rotate(180deg);
                transform: rotate(180deg)
            }
            45% {
                -webkit-transform: rotate(198deg);
                transform: rotate(198deg)
            }
            55% {
                -webkit-transform: rotate(234deg);
                transform: rotate(234deg)
            }
            to {
                -webkit-transform: rotate(540deg);
                transform: rotate(540deg)
            }
        }
        
        .hidden-comp-ghost-mode {
            opacity: .5
        }
        
        .collapsed-comp-mode:after {
            position: absolute;
            top: 0;
            bottom: 0;
            left: 0;
            right: 0;
            content: "";
            background: -webkit-repeating-linear-gradient(135deg, transparent, transparent 49%, #2b5672 0, #2b5672 0, transparent 51%);
            background: repeating-linear-gradient(-45deg, transparent, transparent 49%, #2b5672 0, #2b5672 0, transparent 51%);
            background-size: 6px 6px;
            background-repeat: repeat
        }
        
        .g-transparent-a:link,
        .g-transparent-a:visited {
            border-color: transparent
        }
        
        .transitioning-comp * {
            transition: inherit!important;
            -webkit-transition: inherit!important
        }
        
        .selectionSharerContainer {
            position: absolute;
            background-color: #fff;
            box-shadow: 0 4px 10px 0 rgba(57, 86, 113, .24);
            width: 142px;
            height: 45px;
            border-radius: 100px;
            text-align: center
        }
        
        .selectionSharerContainer:after {
            content: "";
            position: absolute;
            bottom: -10px;
            left: 42%;
            border-width: 10px 10px 0;
            border-style: solid;
            border-color: #fff transparent;
            display: block;
            width: 0
        }
        
        .selectionSharerContainer .selectionSharerOption {
            display: inline-block;
            cursor: pointer;
            vertical-align: top;
            padding: 13px 11px 11px 13px;
            margin: 1px;
            z-index: -1
        }
        
        .selectionSharerContainer .selectionSharerVerticalSeparator {
            margin-top: 9px;
            margin-bottom: 18px;
            background-color: #eaf7ff;
            height: 26px;
            width: 1px;
            display: inline-block
        }
        
        .visual-focus-on .focus-ring:not(.has-custom-focus):focus {
            box-shadow: inset 0 0 0 1px hsla(0, 0%, 100%, .9), 0 0 1px 2px #3899ec!important
        }
    </style>
    <link rel="stylesheet" href="<?php echo get_template_directory_uri();?>/assets/css/languages.css" id="font_langauges">
    <link rel="stylesheet" href="<?php echo get_template_directory_uri();?>/assets/css/style.css" >
    <link rel="stylesheet" href="<?php echo get_template_directory_uri();?>/assets/css/reset.css" >
    <meta property="og:title" content="inspección de propiedades">
    <meta property="og:url" content="">
    <meta name="robots" content="index">
    <?php wp_head(); ?>
</head>